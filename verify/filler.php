<?php

namespace Deveducation;

use Dompdf\Dompdf;
use Dompdf\Options;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\ErrorCorrectionLevel;

class Filler {
	private $template;
	private $placeholders = [];

	public function __construct() {
	}

	public function setPlaceholders( $placeholders ): void {
		$this->placeholders = $placeholders;
	}

	public function setTemplate( $template, $qr = 'default' ): void {
		$qrcode = new QrCode( $qr == 'default' ? $this->getLink() : $qr );

		$qrcode->setErrorCorrectionLevel( ErrorCorrectionLevel::LOW() );

		$this->template = str_replace(
			array_merge( array_keys( $this->placeholders ), [ '%qr%', '%assets%' ] ),
			array_merge(
				array_values( $this->placeholders ),
				[
					$qrcode->writeDataUri(),
					$this->getLink( true )
				]
			),
			file_get_contents( $template )
		);
	}

	public function generate( $filename = 'certificate.pdf' ): void {
		$options = new Options();
		$options->set( 'isRemoteEnabled', true );

		$dompdf = new Dompdf( $options );

		$dompdf->loadHtml( $this->template );
		$dompdf->setPaper( 'A4', 'landscape' );
		$dompdf->render();

		$dompdf->stream( $filename, [ 'Attachment' => false ] );
	}

	public function getLink( $assets = false ): string {
		$url = ( $_SERVER['HTTPS'] === 'on' ? "https" : "http" ) . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		return $assets ? substr( $url, 0, strpos( $url, '?' ) ) . 'assets/' : $url;
	}
}