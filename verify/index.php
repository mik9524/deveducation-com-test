<?php

if ( ! isset( $_GET['token'] ) ) {
	header( 'Location: /' );
}

require 'vendor/autoload.php';
require 'filler.php';

$pdo = new PDO(
	'mysql:host=mariadb-certs;dbname=wordpress;',
	'wordpress',
	'bJ13hAyfHfJkmVawHjeURmVa'
);

$prepared = $pdo->prepare(
	'SELECT c.*, h.honor FROM sertificate s 
                        JOIN data c ON s.token = ? AND c.id = s.id_sertificate 
                        JOIN honors h ON h.id_sertificate = c.id'
);

$prepared->execute( [ $_GET['token'] ] );

if ( $data = $prepared->fetch() ) {
	$certificate = new Deveducation\Filler();

	$certificate->setPlaceholders(
		[
			'%honor%'  => strpos( strtolower( $data['course'] ), 'project' ) !== false ? 'pm' : $data['honor'],
			'%number%' => $data['series'] . ' ' . $data['number'],
			'%owner%'  => str_replace( ' ', '<br>', $data['full_name_student'] ),
			'%course%' => $data['course'],
			'%year%'   => strtolower( $data['city'] ) === 'online' ? date( 'Y' ) : $data['city'] . ', ' . date( 'Y' ),
			'%date%'   => str_replace( '/', '.', $data['date_start'] . ' - ' . $data['date_end'] )
		]
	);
	$certificate->setTemplate( 'templates/devedu.html' );
	$certificate->generate();
} else {
	header( 'Location: /' );
}