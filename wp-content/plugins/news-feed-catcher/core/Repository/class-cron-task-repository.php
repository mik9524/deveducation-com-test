<?php
/**
 * Class Cron_Task_Repository
 *
 */

class Cron_Task_Repository {

	private $wpdb;
	private $table_name;
	const TABLE_NAME = 'news_channel';

	/**
	 * Cron_Task_Repository constructor.
	 *
	 * @param $wpdb
	 */
	public function __construct( $wpdb ) {
		$this->wpdb       = $wpdb;
		$this->table_name = $this->wpdb->prefix . self::TABLE_NAME;
	}


	/**
	 * @param $arrayData
	 *
	 * @return mixed
	 */
	public function save( $arrayData ) {

		return $this->wpdb->insert( $this->table_name, $arrayData );
	}

	/**
	 * @param $task_id
	 * @param $arrayData
	 *
	 * @return mixed
	 */
	public function update( $task_id, $arrayData ) {
		$where = [ 'id' => $task_id ];

		return $this->wpdb->update( $this->table_name, $arrayData, $where );
	}

	/**
	 * @param $task_id
	 *
	 * @return mixed
	 */
	public function delete( $task_id ) {
		$where = [ 'id' => $task_id ];

		return $this->wpdb->delete( $this->table_name, $where );
	}

	/**
	 * @param $name
	 */
	public function find( $name ) {
	}

	/**
	 * @return mixed
	 */
	public function getAllTasks() {
		$results = $this->wpdb->get_results( "SELECT * FROM $this->table_name", ARRAY_A );

		return $results;
	}

	/**
	 * @return mixed
	 */
	public function getActiveTasks( $hourly = true ) {
		if ( $hourly ) {
			$results = $this->wpdb->get_results( "SELECT * FROM $this->table_name WHERE task_period = 1 AND status=1", ARRAY_A ); // hourly
		} else {
			$results = $this->wpdb->get_results( "SELECT * FROM $this->table_name WHERE task_period = 24 AND status=1", ARRAY_A ); // daily
		}

		return $results;
	}
}