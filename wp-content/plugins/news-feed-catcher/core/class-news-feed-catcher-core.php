<?php

/**
 * The core functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/core
 */

/**
 * The core functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the core stylesheet and JavaScript.
 *
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/core
 * @author     Your Name <email@example.com>
 */
class News_Feed_Catcher_Core {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 *
	 */
	public function run_task_hourly() {
		global $wpdb;

		$task_repository = new Cron_Task_Repository( $wpdb );
		$tasks           = $task_repository->getActiveTasks();

		foreach ( $tasks as $task ) {
			$this->upload_news( $task );
		}
	}

	/**
	 *
	 */
	public function run_task_daily() {
		global $wpdb;

		$task_repository = new Cron_Task_Repository( $wpdb );
		$tasks           = $task_repository->getActiveTasks( false );

		foreach ( $tasks as $task ) {
			$this->upload_news( $task );
		}
	}

	/**
	 * @param $task
	 *
	 * @return bool
	 */
	private function upload_news( $task ) {
		$request = wp_remote_get( $task['url'], [ 'timeout' => 20 ] );

		if ( is_wp_error( $request ) ) {
			return false;
		}

		$body = wp_remote_retrieve_body( $request );
		$news = json_decode( ( $body ), true );
		if ( ! empty( $news ) ) {
			foreach ( $news as $post ) {
				$this->savePost( $post, $task );
			}
		}

		return true;
	}

	private function savePost( $post, $task_settings ) {
		global $wpdb;

		$description = $post['Description'];
		$exploded    = explode( '/', $post['Link'] );
		$post_name   = end( $exploded );
		$author      = $task_settings['author'];
		$tags        = json_decode( $task_settings['author_tags'], true );

		foreach ( $post['Tags'] as $tag ) { // trying to find in tags
			if ( isset( $tags[ $tag['Name'] ] ) ) {
				$author = $tags[ $tag['Name'] ];

				break;
			}
		}

		if ( isset( $post['Heading'] ) ) {
			$prepared = $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title IN (%s, %s) OR post_name = %s", $post['Title'], $post['Heading'], $post_name );
		} else {
			$prepared = $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s OR post_name = %s", $post['Title'], $post_name );
		}

		if ( empty( $wpdb->query( $prepared ) ) ) {
			$new_post = array(
				'post_title'   => $post['Heading'] ?? $post['Title'],
				'post_content' => $this->reuploadImages( $post['Encoded'], $post['Heading'] ?? $post['Title'], json_decode( $task_settings['image_settings'] ) ),
				'post_date'    => date( "Y-m-d H:i:s", $post['PublishDate'] ),
				'post_status'  => $task_settings['pub_status'],
				'post_author'  => $author,
				'post_name'    => $post_name,
				'post_type'    => $task_settings['post_type']
			);

			$post_id = wp_insert_post( $new_post );

			if ( function_exists( 'pll_set_post_language' ) ) {
				pll_set_post_language( $post_id, $task_settings['lang'] );
			}

			if ( ! empty( $task_settings['term_id'] ) ) {
				wp_set_post_terms( $post_id, [ $task_settings['term_id'] ], $task_settings['post_category'] );
			}

			if ( ! is_wp_error( $post_id ) ) {
				/* Yoast SEO integration */
				$this->saveYoastTags( $post_id, [
					'title'       => $post['Title'],
					'description' => $description
				] );
				$this->saveCustomPermalink( $post_id, [
					'url'           => $post_name,
					'category_slug' => ! empty( get_category( $task_settings['term_id'] ) ) ? get_category( $task_settings['term_id'] )->slug : '',
				] );
				$this->savePostTags( $post_id, $post['Tags'] );

				$this->savePostThumbnail( $post_id, $post['Image'], json_decode( $task_settings['image_settings'] ) );

				if ( ! empty( $task_settings['template'] ) ) {
					$this->savePostTemplate( $post_id, $task_settings['template'] );
				}

				do_action( 'news_feed_catcher_on_added', $post_id, $post );

				$this->logger( '[' . $task_settings['task_name'] . '] ' . date( "Y-m-d H:i:s", $post['PublishDate'] ) . ' - success - <a href=' . get_permalink( $post_id ) . ' target="_blank">' . trim($post['Heading'] ?? $post['Title']) . '</a>' );
			} else {
				$this->logger( '[' . $task_settings['task_name'] . '] ' . date( "Y-m-d H:i:s", $post['PublishDate'] ) . ' - failed - <a href="' . $post['Link'] . '"> target="_blank"' . trim($post['Heading'] ?? $post['Title']) . '</a>' );
			}
		}
	}


	private function saveYoastTags( $post_id, $arrayData ) {
		/* Yoast SEO integration */
		include_once( 'wp-admin/includes/plugin.php' );
		if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) || is_plugin_active( 'wordpress-seo-premium/wp-seo-premium.php' ) ) {
			update_post_meta( $post_id, '_yoast_wpseo_title', $arrayData['title'] . ' %%sep%% %%sitename%%' );
			update_post_meta( $post_id, '_yoast_wpseo_metadesc', $arrayData['descrition'] );
		}
	}

	private function saveCustomPermalink( $post_id, $arrayData ) {
		/* Yoast SEO integration */
		include_once( 'wp-admin/includes/plugin.php' );
		if ( is_plugin_active( 'custom-permalinks/custom-permalinks.php' ) && $arrayData['category_slug'] ) {
			update_post_meta( $post_id, 'custom_permalink', $arrayData['category_slug'] . '/' . $arrayData['url'] . '/' );
		}
	}

	public function savePostTemplate( $post_id, $post_template ) {
		update_post_meta( $post_id, '_wp_page_template', $post_template );
	}

	private function savePostTags( $post_id, $array_tags ) {
		if ( ! empty( $array_tags ) ) {
			$array_tag_names = [];
			foreach ( $array_tags as $tag ) {
				if ( 'brexit' == strtolower( $tag['Name'] ) ) {
					$array_tag_names[] = $tag['Name'];
				}
			}
			if ( ! empty( $array_tag_names ) ) {
				wp_set_post_tags( $post_id, implode( ',', $array_tag_names ), true );
			}
		}
	}

	private function savePostThumbnail( $post_id, $image_url, $settings ) {
		if ( $attach_id = $this->uploadImageToMedia( $image_url, $settings->width, $settings->height, $settings->quality ) ) {
			set_post_thumbnail( $post_id, $attach_id );
		}
	}

	/**
	 * Reupload images and replacing the links
	 *
	 * @param string $encoded Encoded content
	 *
	 * @return string
	 * @since    2.0.0
	 */
	private function reuploadImages( $encoded, $alt, $settings ) {
		preg_match_all( '/<img[\sA-z"\\\\=-]+src="([A-z:\/.0-9-]+)" alt="" \/>/s', $encoded, $images );

		foreach ( $images[1] as $image => $link ) {
			list( $width, $height ) = getimagesize( $link );

			$attachment_id = $this->uploadImageToMedia( $link, $settings->width, $settings->height, $settings->quality );
			$tag           = '<img src="' . wp_get_attachment_url( $attachment_id ) . '" alt="' . $alt . '" width="' . $width . '" height="' . $height . '">';
			$encoded       = str_replace( $images[0][ $image ], $tag, $encoded );
		}

		$out = strip_tags( $encoded, '<table><p><ol><ul><li><h1><h2><h3><h4><h5><h6><img><a>' ); // clearing the content
		$out = preg_replace( '/<([^>\s]+)[^>]*>(?:\s*(?:<br \/>|&nbsp;|&thinsp;|&ensp;|&emsp;|&#8201;|&#8194;|&#8195;)\s*)*<\/\1>/s', '', $out ); // clearing the empty tags

		return preg_replace( '/(<[^>]+) style=".*?"/i', '$1', $out ); // clearing the style attribute
	}

	private function uploadImageToMedia( $url, $width = null, $height = null, $quality = 100 ) {
		$upload_dir       = wp_upload_dir();
		$exploded         = explode( '/', $url );
		$unique_file_name = wp_unique_filename( $upload_dir['path'], end( $exploded ) );
		$filename         = basename( $unique_file_name );

		if ( wp_mkdir_p( $upload_dir['path'] ) ) {
			$file = $upload_dir['path'] . '/' . $filename . '.jpg';
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename . '.jpg';
		}

		if ( class_exists( 'Imagick' ) && $width && $height ) {
			$imagick = new Imagick();

			try {
				$imagick->readImageBlob( file_get_contents( $url ) );

				if ( $imagick->getImageWidth() > 1920 || $this->getFileSize( $url ) >= 1.5 * 1000 * 1000 ) { // 1.5 MB
					$imagick->scaleImage( $width, $height, true );
				}

				$imagick->setImageCompressionQuality( $quality );
				$imagick->stripImage();
				$imagick->writeImage( $file );
				$imagick->destroy();
			} catch ( ImagickException $e ) {
				file_put_contents( $file, file_get_contents( $url ) );
			}

		} else {
			file_put_contents( $file, file_get_contents( $url ) );
		}

		$wp_filetype = wp_check_filetype( $filename . '.jpg', null );

		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title'     => sanitize_file_name( $filename ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		$attach_id = wp_insert_attachment( $attachment, $file, 0 );

		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

		wp_update_attachment_metadata( $attach_id, $attach_data );

		return $attach_id;
	}

	private function logger( $message ) {
		file_put_contents( __DIR__ . '/../log.txt', $message . PHP_EOL, FILE_APPEND );
	}

	private function getFileSize( $url ) {
		$ch = curl_init( $url );

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HEADER, true );
		curl_setopt( $ch, CURLOPT_NOBODY, true );

		curl_exec( $ch );

		$size = curl_getinfo( $ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD );

		curl_close( $ch );

		return $size;
	}
}
