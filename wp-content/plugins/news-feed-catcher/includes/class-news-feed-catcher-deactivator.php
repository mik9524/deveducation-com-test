<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/includes
 * @author     Your Name <email@example.com>
 */
class News_Feed_Catcher_Deactivator {

	const TABLE_NAME = 'news_channel';
	const CRON_TASK_HOOK_HOURLY = 'news_catcher_task_hourly';
	const CRON_TASK_HOOK_DAILY = 'news_catcher_task_daily';

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		self::stop_cron_tasks();
	}

	/**
	 * Stop all active tasks
	 */
	public static function stop_cron_tasks() {
		wp_clear_scheduled_hook( self::CRON_TASK_HOOK_HOURLY );
		wp_clear_scheduled_hook( self::CRON_TASK_HOOK_DAILY );
	}
}
