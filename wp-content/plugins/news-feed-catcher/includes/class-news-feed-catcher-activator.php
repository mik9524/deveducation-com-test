<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/includes
 * @author     Your Name <email@example.com>
 */
class News_Feed_Catcher_Activator
{

    /**
     *
     */
    const TABLE_NAME = 'news_channel';
    const CRON_TASK_HOOK_HOURLY = 'news_catcher_task_hourly';
    const CRON_TASK_HOOK_DAILY = 'news_catcher_task_daily';


    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        self::create_channel_table();
        self::enable_cron();
    }

    /**
     * Create table with all the future tasks
     */
    public static function create_channel_table()
    {
        global $wpdb;

        $table_name      = $wpdb->prefix . self::TABLE_NAME;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
			  id mediumint(9) NOT NULL AUTO_INCREMENT,
			  task_name tinytext NOT NULL,
			  url varchar(255) DEFAULT '' NOT NULL,
			  pub_status varchar(55) DEFAULT 'draft' NOT NULL,
			  status tinyint NOT NULL,
			  term_id smallint NOT NULL,
			  author smallint NOT NULL,
			  template varchar(255) DEFAULT '' NOT NULL,
			  task_period tinyint NOT NULL DEFAULT 1,
			  post_type tinytext NOT NULL,
			  post_category tinytext NOT NULL,
			  lang varchar(16) DEFAULT '' NOT NULL,
			  image_settings tinytext NOT NULL,
			  author_tags tinytext NOT NULL,
			  PRIMARY KEY  (id)
			) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    public static function enable_cron()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::TABLE_NAME;

        if ($wpdb->get_var("SELECT COUNT(*) FROM $table_name") > 0) { // have previous tasks
            self::add_cron_tasks();
        }
    }

    public static function add_cron_tasks()
    {
        if ( ! wp_next_scheduled(self::CRON_TASK_HOOK_HOURLY)) {
            wp_schedule_event(time(), 'hourly', self::CRON_TASK_HOOK_HOURLY);
        }

        if ( ! wp_next_scheduled(self::CRON_TASK_HOOK_DAILY)) {
            wp_schedule_event(time(), 'daily', self::CRON_TASK_HOOK_DAILY);
        }
    }
}