<div class="wrap">
    <h1 class="options-title">News Aggregator Catcher <span id="show_logs">view logs</span></h1>
    <!--    <h2>Task settings for news aggregator catching.</h2>-->
	<?php
	function get_settings_value( $array_tasks, $id, $value ) {
		foreach ( $array_tasks as $task ) {
			if ( isset($task['id']) && $task['id'] == $id ) {
				return $task[ $value ];
			}
		}
	}

	?>
	<?php if ( ! empty( $errors ) ) : ?>
        <div class="notice notice-error inline">
            <p><?php echo $errors; ?></p>
        </div>
	<?php endif; ?>
	<?php if ( ! empty( $success ) ) : ?>
        <div class="notice notice-success inline">
            <p><?php echo $success; ?></p>
        </div>
	<?php endif; ?>
    <div class="log">
        <h3>Logs</h3>
		<?php if ( file_exists( __DIR__ . '/../log.txt' ) ): ?>
			<?= str_replace( PHP_EOL, '<br>', file_get_contents( __DIR__ . '/../log.txt' ) ); ?>
		<?php else: ?>
            Логов нет
		<?php endif; ?>
    </div>
    <select name="task_name" id="task_name"
            onchange="location = '/wp-admin/admin.php?page=news-feed-catcher&task-id=' + this.options[this.selectedIndex].value;">
		<?php foreach ( $array_tasks as $identifier => $task ) : ?>
            <option <?php echo ( $active_task_id == $task['id'] ) ? 'selected="selected"' : ''; ?>
                    value="<?php echo $task['id']; ?>"><?php echo $task['task_name'] ?></option>
		<?php endforeach; ?>
    </select>
	<?php if ( isset( $removed ) && $removed ): ?>
        <script>setTimeout(() => location.href = '/wp-admin/admin.php?page=news-feed-catcher', 2000)</script>
	<?php endif; ?>
	<?php if ( $active_task_id && ( isset( $removed ) && ! $removed ) ) : ?>
        <a href="/wp-admin/admin.php?page=news-feed-catcher&task-id=<?= $active_task_id; ?>&action=remove"
           class="button button-cancel task-remove">Delete this task</a>
	<?php endif; ?>
    <form method="post" class="form-tabs">
        <div class="nf-tabs">
            <div class="nf-tab active">Feed Settings</div>
            <div class="nf-tab">Post Settings</div>
            <div class="nf-tab">Image Settings</div>
            <input type="submit" name="submit" id="submit" class="option-button button button-primary"
                   value="Save changes">
        </div>
        <div class="nf-tab-content active">
            <table class="form-table">
                <tr valign="center">
                    <th scope="row"><label for="task_name_field">Task Name</label></th>
                    <td><input type="text" id="task_name_field" name="task_name_field" class="regular-text"
                               value="<?php echo $active_task_id ? get_settings_value( $array_tasks, $active_task_id, 'task_name' ) : ''; ?>"
                               required/>
                    </td>
                </tr>
                <tr valign="center">
                    <th scope="row"><label for="url_field">Feed URL</label></th>
                    <td><input type="text" id="url_field" name="url_field" class="regular-text"
                               value="<?php echo $active_task_id ? get_settings_value( $array_tasks, $active_task_id, 'url' ) : ''; ?>"
                               required/>
                    </td>
                </tr>
                <tr valign="center">
                    <th scope="row"><label for="status_field">Task status</label></th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span><input type="radio"</span></legend>
                            <label title='g:i a'>
                                <input type="radio" name="status_field"
                                       value="1" <?php echo ( $active_task_id && 1 == get_settings_value( $array_tasks, $active_task_id, 'status' ) ) ? 'checked' : ''; ?> />
                                <span>Enabled</span>
                            </label><br>
                            <label title='g:i a'>
                                <input type="radio" name="status_field"
                                       value="0" <?php echo ( $active_task_id ) ? ( ( 0 == get_settings_value( $array_tasks, $active_task_id, 'status' ) ) ? 'checked' : '' ) : 'checked'; ?> />
                                <span>Disabled</span>
                            </label>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="center">
                    <th scope="row"><label for="task_period">Task period</label></th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span><input type="radio"></span></legend>
                            <label title='g:i a'>
                                <input type="radio" name="task_period"
                                       value="1" <?php echo ( $active_task_id && 1 == get_settings_value( $array_tasks, $active_task_id, 'task_period' ) ) ? 'checked' : ''; ?> />
                                <span>Every hour</span>
                            </label><br>
                            <label title='g:i a'>
                                <input type="radio" name="task_period"
                                       value="24" <?php echo ( $active_task_id ) ? ( ( 24 == get_settings_value( $array_tasks, $active_task_id, 'task_period' ) ) ? 'checked' : '' ) : 'checked'; ?> />
                                <span>Once per day</span>
                            </label>
                        </fieldset>
                    </td>
                </tr>
            </table>
			<?php if ( $active_task_id ) : ?>
                <input type="hidden" name="task_id"
                       value="<?php echo get_settings_value( $array_tasks, $active_task_id, 'id' ); ?>">
			<?php endif; ?>
        </div>
        <div class="nf-tab-content">
            <table class="form-table">
                <tr valign="center">
                    <th scope="row"><label for="post_type">Post Type</label></th>
                    <td>
                        <select name="post_type" id="post_type">
                            <option selected disabled>Please choose post type</option>
							<?php foreach ( $registered_types as $post_type => $post_data ) : ?>
                                <option value="<?php echo $post_type; ?>" <?php echo $active_task_id ? ( get_settings_value( $array_tasks, $active_task_id, 'post_type' ) == $post_type ? ' selected' : '' ) : ''; ?>>
									<?php echo $post_data->label; ?>
                                </option>
							<?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr valign="center" id="block_taxonomy" style="display: none;">
                    <th scope="row"><label for="post_taxonomy">Post Taxonomy</label></th>
                    <td>
                        <select name="post_taxonomy" id="post_taxonomy"
                                data-preview="<?php echo get_settings_value( $array_tasks, $active_task_id, 'post_category' ) ?? ''; ?>">
                            <option value="default" selected>Default</option>
                        </select>
                        <select name="post_term" id="post_term"
                                data-preview="<?php echo get_settings_value( $array_tasks, $active_task_id, 'term_id' ) ?? ''; ?>"
                                disabled>
                            <option value="default" selected>Default</option>
                        </select>
                    </td>
                </tr>
                <tr valign="center" id="block_templates" style="display:none;">
                    <th scope="row"><label for="post_template">Post Template</label></th>
                    <td>
                        <select name="post_template" id="post_template"
                                data-preview="<?php echo get_settings_value( $array_tasks, $active_task_id, 'template' ) ?? ''; ?>">
                            <option value="default" selected>Default</option>
                        </select>
                    </td>
                </tr>
				<?php if ( function_exists( 'pll_default_language' ) ): ?>
                    <tr valign="center">
                        <th scope="row"><label for="language_field">Select Language</label></th>
                        <td>
							<?php

							$langInfo = pll_languages_list();
							?>
                            <select name="language_field" id="language_field">
                                <option value="">Select language</option>
								<?php foreach ( $langInfo as $lang ) : ?>
                                    <option value="<?php echo $lang; ?>"<?php echo $active_task_id ? ( get_settings_value( $array_tasks, $active_task_id, 'lang' ) == $lang ? ' selected' : '' ) : ''; ?>>
										<?php echo $lang; ?>
                                    </option>
								<?php endforeach; ?>
                            </select>
                            <script>

                            </script>
                        </td>
                    </tr>
				<?php endif; ?>
                <tr valign="center">
                    <th scope="row"><label for="author_field">Choose an author</label></th>
                    <td>
                        <select name="author_field" id="author_field">
							<?php foreach ( $post_authors as $author_object ) : ?>
                                <option value="<?php echo $author_object->data->ID; ?>" <?php echo ( $active_task_id && get_settings_value( $array_tasks, $active_task_id, 'author' ) == $author_object->data->ID ) ? 'selected="selected"' : ''; ?>>
									<?php echo $author_object->data->user_login; ?>
                                </option>
							<?php endforeach; ?>
                        </select>
                    </td>
                </tr>
				<?php if ( $active_task_id ): ?>
					<?php $author_tags = json_decode( get_settings_value( $array_tasks, $active_task_id, 'author_tags' ) ); ?>
				<?php endif; ?>
                <tr id="authors-tag" valign="center">
                    <th scope="row"><label for="author_field">Author from tags</label></th>
                    <td>
						<?php if ( ( $active_task_id && ! $author_tags ) || ! $active_task_id ): ?>
                            <div class="author-tag">
                                <input type="text" class="regular-text" name="author_tag[]">
                                <select name="author_tag_id[]" id="author_field">
									<?php foreach ( $post_authors as $author_object ) : ?>
                                        <option value="<?php echo $author_object->data->ID; ?>">
											<?php echo $author_object->data->user_login; ?>
                                        </option>
									<?php endforeach; ?>
                                </select>
                                <span>remove</span>
                            </div>
						<?php else: ?>
							<?php foreach ( $author_tags as $tag => $author ): ?>
                                <div class="author-tag">
                                    <input type="text" class="regular-text" value="<?php echo $tag; ?>"
                                           name="author_tag[]">
                                    <select name="author_tag_id[]" id="author_field">
										<?php foreach ( $post_authors as $author_object ) : ?>
                                            <option value="<?php echo $author_object->data->ID; ?>" <?php echo $author_object->data->ID == $author ? 'selected="selected"' : ''; ?>>
												<?php echo $author_object->data->user_login; ?>
                                            </option>
										<?php endforeach; ?>
                                    </select>
                                    <span>remove</span>
                                </div>
							<?php endforeach; ?>
						<?php endif; ?>
                        <input type="button" class="add-tag button button-secondary" value="Add">
                    </td>
                </tr>
                <tr valign="center">
                    <th scope="row"><label for="pub_status_field">Publish Status</label></th>
                    <td>
                        <select name="pub_status_field" id="pub_status_field">
							<?php foreach ( $post_publish_statuses as $status_name => $template_value ) : ?>
                                <option value="<?php echo $status_name; ?>" <?php echo ( $active_task_id && get_settings_value( $array_tasks, $active_task_id, 'pub_status' ) == $status_name ) ? 'selected="selected"' : ''; ?>>
									<?php echo $template_value; ?>
                                </option>
							<?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="nf-tab-content">
			<?php if ( $active_task_id ): ?>
				<?php $image_settings = json_decode( get_settings_value( $array_tasks, $active_task_id, 'image_settings' ) ); ?>
			<?php endif; ?>
            <table class="form-table">
                <tr valign="center">
                    <th scope="row"><label for="image_width">Width</label></th>
                    <td><input type="text" id="image_width" name="image_width"
                               value="<?php echo $image_settings->width ?? 1024; ?>"></td>
                </tr>
                <tr valign="center">
                    <th scope="row"><label for="image_height">Height</label></th>
                    <td><input type="text" id="image_height" name="image_height"
                               value="<?php echo $image_settings->height ?? 768; ?>"></td>
                </tr>
                <tr valign="center">
                    <th scope="row"><label for="image_quaility">Quality</label></th>
                    <td><input type="number" id="image_quality" name="image_quality" max="100" min="50"
                               value="<?php echo $image_settings->quality ?? 100; ?>"></td>
                </tr>
            </table>
        </div>
    </form>
</div>