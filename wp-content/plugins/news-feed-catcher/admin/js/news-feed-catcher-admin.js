(function ($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */
    $(window).load(function () {
        let preview = false;

        $('#show_logs').click(function () {
            $('.log').slideToggle();
        });

        $('.add-tag').click(function () {
            let element = $('.author-tag').last().clone();
            element.find('input, select').val('');

            $('.author-tag').last().after(element);
        });

        $('#authors-tag').on('click', 'span', function() {
           $(this).parent().remove();
        });

        $('.nf-tab').click(function () {
            $('.nf-tab.active').removeClass('active');
            $(this).addClass('active');
            $('.nf-tab-content.active').removeClass('active');
            $('.nf-tab-content').eq($(this).index()).addClass('active');
        });

        $('#post_type').on('change', function () {
            $.post(ajaxurl, {action: 'get_type_info', type: $(this).val()}).done(function (e) {
                if (Object.keys(e.data.taxonomies).length) {
                    $('#post_taxonomy, #post_template').html('<option value="default">Default</option>'); // clear
                    $('#block_taxonomy').fadeIn();

                    $.each(e.data.taxonomies, function (i, taxonomy) {
                        $('#post_taxonomy').append('<option value="' + i + '">' + taxonomy.label + '</option>');
                    });

                    if (preview) {
                        $('select[data-preview]').each(function () {
                            $(this).val($(this).data('preview'));
                        });

                        $('#post_taxonomy').trigger('change');
                    }
                } else {
                    $('#block_taxonomy').fadeOut();
                }

                if (Object.keys(e.data.templates).length) {
                    $.each(e.data.templates, function (i, template) {
                        $('#post_template').append('<option value="' + template + '">' + i + '</option>');
                    });

                    $('#block_templates').fadeIn();
                } else {
                    $('#block_templates').fadeOut();
                }
            });
        });

        $('#post_taxonomy').on('change', function () {
            if ($(this).val() == 'default') {
                $('#post_term').prop('disabled', true); // clear

                return;
            }

            $.post(ajaxurl, {action: 'get_type_info', taxonomy: $(this).val()}).done(function (e) {
                if (e.data.terms) {
                    $('#post_term').html('').prop('disabled', false); // clear

                    $.each(e.data.terms, function (i, term) {
                        $('#post_term').append('<option value="' + term.term_id + '">' + term.name + '</option>');
                    });

                    if (preview) {
                        $('#post_term').val($('#post_term').data('preview'));
                    }
                }
            });
        });

        if ($('#post_type').val() != null) {
            $('#post_type').trigger('change');
            preview = true;
        }

        $('.task-remove').click(function (e) {
            if (!confirm('Are you sure? This task will be deleted.')) {
                e.preventDefault();
            }
        });
    });

})(jQuery);
