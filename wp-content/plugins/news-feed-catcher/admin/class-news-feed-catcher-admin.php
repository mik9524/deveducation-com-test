<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    News_Feed_Catcher
 * @subpackage News_Feed_Catcher/admin
 * @author     Your Name <email@example.com>
 */
class News_Feed_Catcher_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of this plugin.
     * @param string $version The version of this plugin.
     *
     * @since    1.0.0
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {
        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in News_Feed_Catcher_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The News_Feed_Catcher_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style(
            $this->plugin_name,
            plugin_dir_url(__FILE__) . 'css/news-feed-catcher-admin.css',
            array(),
            $this->version,
            'all'
        );
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in News_Feed_Catcher_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The News_Feed_Catcher_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script(
            $this->plugin_name,
            plugin_dir_url(__FILE__) . 'js/news-feed-catcher-admin.js',
            array('jquery'),
            $this->version,
            false
        );
    }

    public function admin_menu()
    {
        $menu = add_menu_page(
            'News feed catcher',
            'News Feed Settings',
            'manage_options',
            'news-feed-catcher',
            [$this, 'news_feed_catcher_options_page'],
            'dashicons-rss',
            2
        );

        add_action('load-' . $menu, function () {
            wp_enqueue_style($this->plugin_name, plugins_url('css/news-feed-catcher-admin.css', __FILE__));
        });
    }

    public function get_type_info()
    {
        if (isset($_POST['type'])) {
            $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
            $taxonomies = get_taxonomies(['object_type' => [$type]], 'objects');
            $templates = get_page_templates(null, $type);

            wp_send_json_success(['taxonomies' => $taxonomies, 'templates' => $templates]);
        } elseif (isset($_POST['taxonomy'])) {
            $taxonomy = filter_input(INPUT_POST, 'taxonomy', FILTER_SANITIZE_STRING);

            wp_send_json_success(['terms' => get_terms($taxonomy, ['hide_empty' => false])]);
        }

        wp_die();
    }

    public function news_feed_catcher_options_page()
    {
        $registered_types = get_post_types(['public' => true, '_builtin' => false], 'objects');

        $registered_types['post'] = new stdClass();
        $registered_types['post']->label = 'Posts';

        /* check user's permissions */
        if (!current_user_can('manage_options')) {
            wp_die('Unauthorized user');
        }

        $active_task_id = $_GET['task-id'] ?? '';
        $errors = '';
        $success = '';

        if (!empty($_POST)) {
            if (!$this->validateFormData(
                [
                    'task_name_field',
                    'url_field',
                    'post_type',
                    'task_period',
                    'author_field',
                    'pub_status_field'
                ],
                $_POST
            )) {
                $errors = 'Please fill all the fields';
            } else {
                if ($this->saveFormData($_POST)) {
                    $success = 'Your data is successfully saved';
                };
            }
        } else {
            if (!empty($_GET['action']) && $_GET['action'] == 'remove') {
                $this->deleteFormData(intval($_GET['task-id']));

                $success = 'Your task is successfully removed.';
                $removed = true;
            }
        }

        $array_tasks = $this->getTaskSettings();
        $post_authors = get_users();
        $post_publish_statuses = get_post_statuses();

        array_unshift($array_tasks, ['task_name' => 'Create new task']);
        ?>

        <?php

        require_once('admin-frontend.php');
    }

    public function getTaskSettings()
    {
        global $wpdb;
        $task_repository = new Cron_Task_Repository($wpdb);

        return $task_repository->getAllTasks();
    }

    public function validateFormData($arrayKeys, $arrayData)
    {
        foreach ($arrayKeys as $key) {
            if (empty($arrayData[$key])) {
                return false;
            }
        }

        return true;
    }

    public function saveFormData($arrayData)
    {
        global $wpdb;

        $author_tags = array();

        foreach ($arrayData['author_tag'] as $key => $tag) {
            $author_tags[$tag] = $arrayData['author_tag_id'][$key];
        }

        $task_repository = new Cron_Task_Repository($wpdb);
        $image_settings = json_encode(
            array(
                'width' => $arrayData['image_width'],
                'height' => $arrayData['image_height'],
                'quality' => $arrayData['image_quality']
            )
        );
        $formattedData = [
            'task_name' => $arrayData['task_name_field'],
            'task_period' => $arrayData['task_period'],
            'post_type' => $arrayData['post_type'],
            'post_category' => $arrayData['post_taxonomy'] ?? '',
            'url' => $arrayData['url_field'],
            'status' => $arrayData['status_field'],
            'term_id' => $arrayData['post_term'] ?? 0,
            'author' => $arrayData['author_field'],
            'author_tags' => json_encode($author_tags, JSON_UNESCAPED_UNICODE),
            'lang' => $arrayData['language_field'] ?? '',
            'pub_status' => $arrayData['pub_status_field'],
            'template' => $arrayData['post_template'],
            'image_settings' => $image_settings
        ];

        if (isset($arrayData['task_id'])) {
            return $task_repository->update($arrayData['task_id'], $formattedData);
        }

        if (!wp_next_scheduled(News_Feed_Catcher::CRON_TASK_HOOK_HOURLY)) {
            wp_schedule_event(time(), 'hourly', News_Feed_Catcher::CRON_TASK_HOOK_HOURLY);
        }

        if (!wp_next_scheduled(News_Feed_Catcher::CRON_TASK_HOOK_DAILY)) {
            wp_schedule_event(time(), 'daily', News_Feed_Catcher::CRON_TASK_HOOK_DAILY);
        }

        return $task_repository->save($formattedData);
    }

    public function deleteFormData($task_id)
    {
        global $wpdb;
        $task_repository = new Cron_Task_Repository($wpdb);

        return $task_repository->delete($task_id);
    }
}
