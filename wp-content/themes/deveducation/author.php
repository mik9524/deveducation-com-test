<?php get_header(); ?>
<!-- PAGE -->
<?php 
	$user_info = get_userdata($author);
	$user_instagram = get_user_meta( $author, 'instagram', true);
	$phones = get_acf_options('phones', false, true);
	$user_phone = explode('/', $phones[0]);	
	if ( get_current_blog_id() == 1 && pll_current_language() == 'uk' ) {
		$user_phone = $user_phone[1] ?? '';
	} else {
		$user_phone = $user_phone[0] ?? '';
	}
?>
<main>
	<div class="container">
		<?php print_breadcrumbs(); ?>
		<div class="author-block">
			<div class="author-short">
				<div class="author-image">
					<img src="<?php echo get_user_meta( $author, 'user_photo', true); ?>"
						 alt="Author image" width="150" height="150">
				</div>
				<div class="author-position"><?php ett( 'Автор' ); ?></div>
				<div class="author-name"><h1><?php echo get_user_name( $author ); ?></h1></div>
				<div class="author-status"><?php echo get_user_meta( $author, 'wikipedia' )[0]; ?></div>
				<div class="author-socials">
					<?php if ($user_instagram) { ?>
						<a href="<?php echo $user_instagram; ?>" target="_blank" title="<?php echo $user_instagram; ?>" rel="nofollow">
							<svg width="24" height="25" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12 .251c-6.627 0-12 5.373-12 12 0 6.628 5.373 12 12 12s12-5.372 12-12c0-6.627-5.373-12-12-12zM9.362 5.89c.682-.031.9-.039 2.637-.039 1.738 0 1.956.008 2.638.039.682.031 1.147.14 1.555.297a3.15 3.15 0 011.134.738c.355.356.574.713.738 1.134.157.407.265.872.297 1.553.031.683.04.901.04 2.64 0 1.737-.009 1.955-.04 2.638-.032.68-.14 1.146-.297 1.553a3.155 3.155 0 01-.738 1.134 3.13 3.13 0 01-1.134.738c-.407.159-.872.267-1.554.298-.682.03-.9.038-2.638.038s-1.956-.007-2.639-.038c-.681-.031-1.146-.14-1.553-.298a3.133 3.133 0 01-1.134-.738 3.136 3.136 0 01-.738-1.134c-.158-.407-.266-.872-.297-1.553-.031-.683-.039-.9-.039-2.639 0-1.738.008-1.956.039-2.639.03-.68.138-1.146.297-1.553.164-.42.383-.778.739-1.134a3.134 3.134 0 011.133-.738c.407-.158.872-.266 1.554-.297z" fill="#4C4B54"/><path fill-rule="evenodd" clip-rule="evenodd" d="M11.427 7.005H12c1.708 0 1.911.006 2.586.036.624.029.962.133 1.188.22.299.117.512.255.736.48.224.223.362.437.479.735.087.226.192.564.22 1.188.03.675.037.878.037 2.586 0 1.708-.006 1.91-.037 2.585-.029.624-.133.963-.22 1.188a1.979 1.979 0 01-.48.735 1.98 1.98 0 01-.735.48c-.225.087-.564.191-1.188.22-.675.03-.878.037-2.586.037-1.71 0-1.912-.007-2.587-.037-.624-.03-.962-.133-1.188-.221a1.982 1.982 0 01-.736-.479 1.984 1.984 0 01-.479-.735c-.087-.226-.192-.564-.22-1.188-.031-.675-.037-.878-.037-2.587s.006-1.91.037-2.585c.028-.624.133-.963.22-1.188.116-.3.255-.512.479-.736.224-.224.437-.363.736-.48.226-.087.564-.191 1.188-.22.59-.027.82-.035 2.013-.036v.002zm3.99 1.062a.768.768 0 100 1.537.768.768 0 000-1.537zm-6.703 4.185a3.287 3.287 0 116.573 0 3.287 3.287 0 01-6.573 0z" fill="#4C4B54"/><path d="M12 10.118a2.133 2.133 0 110 4.267 2.133 2.133 0 010-4.267z" fill="#4C4B54"/></svg>
						</a>
					<?php } ?>
					<?php if ($user_info->user_email) { ?>
						<a href="mailto:<?php echo $user_info->user_email; ?>" target="_blank" title="<?php echo $user_info->user_email; ?>" rel="nofollow">
							<svg width="24" height="25" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0 12.251c0-6.627 5.373-12 12-12s12 5.373 12 12c0 6.628-5.373 12-12 12s-12-5.372-12-12zm12.153.46l6.228-4.46H5.606l6.229 4.46a.282.282 0 00.318 0zM19 15.735l-1.149-.823-3.715-2.66L19 8.769v6.966zm-11.76-.678L5.571 16.25H18.38l-1.87-1.339-3.044-2.18-.8.573-.512.367a.282.282 0 01-.319 0l-.511-.367-.818-.585-3.265 2.337zM5 15.7V8.777l4.834 3.46-3.936 2.819L5 15.7z" fill="#4C4B54"/></svg>
						</a>
					<?php } ?>
					<?php if ($user_phone) { ?>
						<a href="tel:<?php echo preg_replace('/\D/','', $user_phone); ?>" target="_blank" title="<?php echo $user_phone; ?>" rel="nofollow">
							<svg width="25" height="25" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0)" fill="#4C4B54"><path d="M18.545 22.754c-.11.107-.988.226-3.306-1.17-2.074-1.25-4.521-3.242-6.891-5.611-2.37-2.37-4.362-4.816-5.612-6.891-1.396-2.32-1.277-3.197-1.17-3.305L4.345 3c.37.15 1.198.688 2.19 1.68.994.993 1.529 1.822 1.68 2.19L6.924 8.165c-1.384 1.386.38 4.409 2.603 6.632 2.224 2.225 5.248 3.987 6.632 2.604l1.292-1.292c.369.15 1.198.686 2.19 1.68.993.993 1.529 1.82 1.68 2.19l-2.775 2.776zm2.157-6.028c-1.118-1.12-3.142-2.85-4.121-1.872l-1.483 1.484c-.246.239-2.178-.27-4.511-2.604-2.334-2.333-2.85-4.266-2.603-4.511L9.468 7.74c.978-.978-.753-3.002-1.872-4.121-.7-.7-1.431-1.297-2.057-1.68-.346-.213-1.398-.858-2.064-.192L.507 4.716c-.891.89-.573 2.62.945 5.14 1.31 2.176 3.383 4.725 5.836 7.178 2.453 2.453 5.002 4.525 7.178 5.836 1.59.958 2.865 1.439 3.813 1.439.555 0 .998-.164 1.326-.493l2.969-2.97c.664-.663.02-1.716-.192-2.063-.383-.626-.98-1.357-1.68-2.057zM21.477 2.844A8.595 8.595 0 0015.357.31h-.002a.75.75 0 000 1.5h.002c1.913 0 3.71.743 5.06 2.094a7.111 7.111 0 012.093 5.062.75.75 0 001.5 0 8.6 8.6 0 00-2.533-6.123z"/><path d="M14.365 4.777h.002c1.385 0 2.685.538 3.662 1.515a5.148 5.148 0 011.515 3.664.75.75 0 001.5 0A6.632 6.632 0 0019.09 5.23a6.637 6.637 0 00-4.723-1.954h-.002a.75.75 0 000 1.5zM13.377 7.743h.002c.855 0 1.66.333 2.264.937a3.18 3.18 0 01.937 2.266.75.75 0 001.5 0 4.665 4.665 0 00-1.376-3.326 4.665 4.665 0 00-3.324-1.376h-.002a.75.75 0 00-.001 1.499z"/></g><defs><clipPath id="clip0"><path fill="#fff" transform="translate(.014 .31)" d="M0 0h23.997v23.997H0z"/></clipPath></defs></svg>					
						</a>
					<?php } ?>
				</div>
			</div>
			<div class="author-desc"><?php echo get_user_meta( $author, 'description' )[0]; ?></div>
		</div>
	</div>
	<?php

	$posts_per_page = 8;
	$paged =  get_query_var('paged') ? get_query_var('paged')-1 : 0;

	$offset = $posts_per_page * $paged;
	
	$events = get_posts( array(
		'post_type'   => array( 'events', 'blog' ),
		'numberposts' => 8,
		'offset'      => $offset,
		'author'      => $author,
		'post_status' => 'publish',
	) ); ?>
	<div class="container">
		<div class="author-posts">
			<?php foreach ( $events as $event ): ?>
				<div class="article">
					<?php $link = get_permalink( $event->ID ); ?>
					<?php $origin = get_post_meta( $event->ID, 'origin', true ); ?>
					<?php $size = array( 0, '200' ); ?>
					<?php $thumbnail = $origin['image'] ?? get_the_post_thumbnail_url( $event->ID, $size ); ?>
					<a href="<?php echo $link; ?>">
						<img src="<?php echo $thumbnail; ?>" alt="<?php echo $event->post_title; ?>"
							 width="300" height="200">
					</a>
					<div class="post-desc">
						<div class="author-post-title">
							<a href="<?php echo $link; ?>"><?php echo $event->post_title; ?></a>
						</div>
						<div class="post-author">
							<a href="/author/<?php echo get_user_meta( $origin['author'] ?? $event->post_author, 'nickname', true ); ?>/"><?php echo get_user_name( $origin['author'] ?? $event->post_author ); ?></a>
							<time datetime="<?php echo $event->post_date; ?>"><?php echo date( 'd.m.Y', strtotime( $event->post_date ) ); ?></time>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php get_template_part( 'parts/general/pagination' ); ?>
	</div>
	<section class="have-questions want-to">
		<div class="container">
			<h3><?php ett( 'Хочешь стать разработчиком?' ); ?></h3>
			<span><?php ett( 'Получи грант на обучение и освой профессию мечты.' ); ?></span>
			<button class="button button-action" data-popup="sign-course"><?php ett( 'Оставить заявку' ); ?></button>
		</div>
	</section>
</main>
<!-- PAGE -->
<?php get_footer(); ?>

