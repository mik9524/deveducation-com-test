<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="preload" href="<?php echo ASSETS_URI . '/fonts/raleway/common.woff2'; ?>" as="font" type="font/woff2" crossorigin="">
	<?php wp_head(); ?>
	<noscript>
		<link rel="stylesheet" href="<?php echo ASSETS_URI . '/css/no-js.css'; ?>">
	</noscript>
	<?php get_template_part( 'parts/general/json-schema' ); ?>
</head>
<?php $offline_class = is_offline_school() ? 'offline-school' : ''; ?>
<?php $disabled_class = is_disabled_site( get_site_url() ) ? ' disabled-site' : ''; ?>
<body <?php body_class($offline_class . $disabled_class ); ?>>
<?php wp_body_open(); ?>
<?php
if ( is_disabled_site( get_site_url() ) || ( get_current_blog_id() == 1 && ! is_site_subdomain() ) ) {
	get_template_part( 'parts/general/overhead');
} 

if ( is_site_subdomain() ) {
	get_template_part( 'parts/general/header-sub', null, $args );
} else {
	get_template_part( 'parts/general/header', null, $args );
}