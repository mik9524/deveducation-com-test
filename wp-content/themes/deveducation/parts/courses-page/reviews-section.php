<section class="reviews-section">
    <div class="container">
        <h2><?php ett( 'Отзывы студентов' ); ?></h2>
        <div class="splide reviews-items">
            <div class="splide__track">
                <ul class="splide__list">
					<?php foreach ( get_reviews( 'reviews' ) as $review ): ?>
                        <li class="splide__slide review">
                            <div class="review-image"
                                 data-link="<?php echo get_post_meta( $review->ID, 'video', true ); ?>">
                                <img src="<?php echo get_the_post_thumbnail_url( $review, 'medium' ); ?>" width="350" height="400" alt="<?php echo $review->post_title; ?>">
                            </div>
                            <div class="review-name"><?php echo $review->post_title; ?></div>
                        </li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>