<div class="courses-section">
    <div class="container">
        <div class="nearest-courses">
            <h1><?php echo get_the_title(); ?></h1>
            <div class="nearest-courses-wrapper" id="courses-tabs">
				<?php
				$message = tt( 'Набор на ближайшие курсы завершён' );
				$i       = 0;
				$k       = 0;
				?>

                <ul class="nearest-courses-list-city">
                    <li data-site-id="<?php echo get_current_blog_id(); ?>"
                        class="city-item active">
                        <span class="city-item-name"
                              data-city-link="<?php echo get_site_url(); ?>/courses/"><?php ett( 'Все курсы' ); ?></span>
                    </li>
					<?php foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ): $i ++; ?>
						<?php switch_to_blog( $site->blog_id ); ?>
                        <li data-site-id="<?php echo get_current_blog_id(); ?>"
                            class="city-item">
                          <span class="city-item-name"
                                data-city-link="<?php echo get_site_lang_url( $site->blog_id ); ?>courses/"><?php ett( $site->blogname, true ); ?></span>
                        </li>
						<?php restore_current_blog(); ?>
					<?php endforeach; ?>
                </ul>
                <div class="nearest-courses-list-courses">
                    <div class="splide-list-courses">
                        <div class="splide__track">
                            <div class="course-items active splide__list all"
                                 data-courses-id="<?php echo get_current_blog_id(); ?>">
								<?php foreach (
									get_posts( array(
										'post_type' => 'courses',
										'numberposts' => - 1
									) ) as $course
								): ?>
									<?php $alternative = get_post_meta( $course->ID, 'courses_alternative_date', true ); ?>
                                    <div class="course-item splide__slide">
                                            <a href="<?php echo get_home_url(); ?>/courses/<?php echo $course->post_name; ?>/"
                                                data-site-id="<?php echo get_current_blog_id(); ?>">
                                                <div class="course-top">
                                                    <svg role="img" class="lines">
                                                        <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#lines"></use>
                                                    </svg>
                                                    <svg role="img" class="code">
                                                        <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#code"></use>
                                                    </svg>
                                                    <div class="course-title"><?php echo $course->post_title; ?></div>
                                                </div>
                                            </a>
                                            <div class="course-bottom">
                                                <div class="course-desc"><?php echo $course->post_content; ?></div>
                                                <a href="<?php echo get_home_url(); ?>/courses/<?php echo $course->post_name; ?>/"><span class="course-date"><?php ett( 'Подробнее' ); ?></span></a>
                                            </div>
                                       
                                    </div>
								<?php endforeach; ?>
                            </div>
							<?php foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ): $k ++; ?>
								<?php switch_to_blog( $site->blog_id ); ?>
								<?php $nearest = get_nearest_courses(); ?>
                                <div class="course-items"
                                     data-courses-id="<?php echo get_current_blog_id(); ?>">
									<?php if ( count( $nearest ) ): ?>
										<?php foreach ( $nearest as $course ): ?>
											<?php $alternative = get_post_meta( $course->ID, 'courses_alternative_date', true ); ?>
                                            <div class="course-item splide__slide">
                                                <a href="<?php echo get_home_url(); ?>/courses/<?php echo $course->post_name; ?>/"
                                                   data-site-id="<?php echo get_current_blog_id(); ?>">
                                                    <div class="course-top">
                                                        <svg role="img" class="lines">
                                                            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#lines"></use>
                                                        </svg>
                                                        <svg role="img" class="code">
                                                            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#code"></use>
                                                        </svg>
                                                        <div class="course-title"><?php echo $course->post_title; ?></div>
                                                    </div>
                                                    <div class="course-bottom">
                                                        <span class="course-start"><?php ett( 'Старт занятий', true ) ?>:</span>
														<?php if ( ! empty( $alternative ) ): ?>
                                                            <span class="course-date"><?php echo $alternative; ?></span>
														<?php else: ?>
                                                            <span class="course-date"><?php echo convert_acf_date( get_post_meta( $course->ID, 'courses_date', true ) ); ?></span>
														<?php endif; ?>
                                                    </div>
                                                </a>
                                            </div>
										<?php endforeach; ?>
									<?php else: ?>
                                        <div class="course-empty splide__slide"
                                             data-courses-id="<?php echo get_current_blog_id(); ?>">
                                            <p class="course-text"><?php echo $message; ?></p>
                                            <svg role="img" class="lines">
                                                <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#lines"></use>
                                            </svg>
                                            <svg role="img" class="braces">
                                                <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#braces"></use>
                                            </svg>
                                            <svg role="img" class="end-tag">
                                                <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#end-tag"></use>
                                            </svg>
                                            <svg role="img" class="debag">
                                                <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#debag"></use>
                                            </svg>
                                            <div class="stiven-image">
                                                <img src="<?= get_template_directory_uri() . '/assets/img/stiven.svg' ?>"
                                                     alt="stiven leopard" width="260" height="485">
                                            </div>
                                        </div>
									<?php endif; ?>
                                </div>
								<?php restore_current_blog(); ?>
							<?php endforeach; ?>
                        </div>
                    </div>

                </div>
                <div class="nearest-courses-link">
                    <a href="#"><?php ett( 'Все курсы в городе' ) ?> <span class="city"></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
