<section class="grant-chance-section">
    <div class="container">
        <div class="grant-chance">
            <h2><?php ett( 'Заполни форму и получи грант на обучение в ближайшем потоке курса!' ); ?></h2>

            <form data-ajax-action="sign_course" class="grant-chance-form">
                <div class="grant-chance-fields">
					<?php wp_nonce_field( - 1, 'grant' ); ?>
					<?php wp_nonce_field( - 1, 'grant_chance' ); ?>
                    <input class="grant-chance-field" type="text" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>"
                           required>
                    <input class="phone-mask grant-chance-field" name="phone" type="text"
                           placeholder="<?php ett( 'Телефон' ); ?>"
                           required>
                    <input class="grant-chance-field" type="email" name="email" placeholder="Email" required>
                    <div class="custom-select grant-chance-field">
                        <?php 
                        $args = is_offline_school() ? '' : array( 'site__not_in' => array( get_main_site_id() ) );
                        $all_sites = get_sites($args);
                        $site_name = 'Выберите город';
                        $site_id = '';
                        foreach ( $all_sites as $site ) {
                            if ( get_current_blog_id() == $site->blog_id ) {
                                $site_name = $site->blogname;
                                $site_id = $site->blog_id;
                                break;
                            }
                        }
                        ?>
                        <div class="select city"><?php ett($site_name); ?></div>
                        <div class="choose-city">
							<?php foreach ( $all_sites as $site ): ?>
                                <?php if ( is_disabled_site($site->domain) ) continue; ?>
                                <div class="choose-city-list"
                                     data-value="<?php echo $site->blog_id; ?>"><?php ett( $site->blogname ); ?></div>
							<?php endforeach; ?>
                        </div>
                        <input class="choose-city-input" value="<?php echo $site_id; ?>" type="text" name="city_id" hidden required>
                    </div>
                    <div class="custom-select grant-chance-field">
                        <div class="select course"><?php ett( 'Выберите курс' ); ?></div>
                        <div class="courses-list"></div>
                        <input class="choose-course-input" value="" type="text" name="course" hidden required>
                    </div>
                    <button class="grant-chance-button"><?php ett( 'Получить грант' ); ?></button>
                    <p class="terms-and-cond">
                    <span class="check_tnc">
                        <input type="checkbox" name="agree_personal_data" id="agree_personal_data" required>
                        <label for="agree_personal_data"></label>
                    </span>
                        <span><?php ett( 'Заполняя форму регистрации, я принимаю' ); ?>
                        <a href="<?= get_permalink( get_page_by_path( 'politika-konfidentsialnosti' ) ); ?>"> <?php ett( 'условия обработки персональных данных' ) ?></a>
                    </span>
                    </p>
                </div>
            </form>

            <div class="form-result"><?php ett( 'Успешно отправлено' ); ?></div>
        </div>
    </div>
</section>