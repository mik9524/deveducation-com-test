<?php get_header_ajax(); ?>
	<!-- BLOG -->
	<main>
		<div class="container">
			<div class="article">
				<?php print_breadcrumbs(); ?>
				<?php get_template_part( 'parts/general/article-share-buttons' ); ?>
				<?php $origin = get_post_meta( get_the_ID(), 'origin', true ); ?>
				<div class="article-author">
					<a href="/author/<?php echo get_user_meta( ( $origin['author'] ?? get_the_author_meta( 'ID' ) ), 'nickname', true ); ?>/"><?php echo get_user_name( $origin['author'] ?? get_the_author_meta( 'ID' ) ); ?></a>
					<time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php the_date(); ?></time>
				</div>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
		<section class="join-us">
			<h2><?php ett( 'Присоединяйся к DevEducation — стань востребованным специалистом и построй карьеру в IT!' ); ?></h2>
			<button data-popup="join-us" class="form-button"><?php ett( 'Присоединиться' ); ?></button>
		</section>
	</main>
	<!-- BLOG -->
	<!-- POPUP -->
	<div class="popup-overlay" id="join-us">
		<div class="popup">
			<div class="popup-close"></div>
			<div class="popup-title"><?php ett( 'Присоединиться к DevEducation' ); ?></div>
			<div class="popup-content">
				<form data-ajax-action="join_us">
					<?php wp_nonce_field( - 1, 'join' ); ?>
					<input type="text" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
					<input class="phone-mask" name="phone" type="text" placeholder="<?php ett( 'Телефон' ); ?>"
						   required>
					<button class="button button-action form-button"><?php ett( 'Отправить' ); ?></button>
				</form>
			</div>
		</div>
	</div>
	<!-- POPUP -->
<?php get_footer_ajax(); ?>