<?php get_header_ajax(); ?>
    <!-- EVENT -->
    <main>
        <div class="container">
            <div class="article">
				<?php print_breadcrumbs(); ?>
				<?php get_template_part( 'parts/general/article-share-buttons' ); ?>
				<?php $origin = get_post_meta( get_the_ID(), 'origin', true ); ?>
                <div class="article-author">
                    <a href="/author/<?php echo get_user_meta( ( $origin['author'] ?? get_the_author_meta( 'ID' ) ), 'nickname', true ); ?>/"><?php echo get_user_name( $origin['author'] ?? get_the_author_meta( 'ID' ) ); ?></a>
                    <time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php the_date(); ?></time>
                </div>
                <h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
            </div>
        </div>
    </main>
    <!-- EVENT -->
<?php get_footer_ajax(); ?>