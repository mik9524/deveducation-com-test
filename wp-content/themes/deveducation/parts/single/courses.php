<?php get_header( null, array( 'transparent' => true ) ); ?>
    <!-- COURSE -->
<?php $course = get_acf_group( get_the_ID(), 'courses' ); ?>
    <main>
        <div class="course">
            <section class="top-course"
                     data-bg="<?php echo wp_get_attachment_image_url( $course->image_bg, 'full' ); ?>">
                <div class="container">
					<?php print_breadcrumbs(); ?>
                    <h3><?php echo $course->subtitle; ?></h3>
                    <h1><?php echo $course->title; ?></h1>
                    <div class="course-description"><?php echo $course->desc; ?></div>
					<?php if ( is_site_subdomain() ): ?>
                        <div class="course-city">
							<?php ett( 'Город' ); ?>: <strong><?php ett( get_bloginfo() ); ?></strong>
                        </div>
                        <div class="course-date">
							<?php ett( 'Начало занятий' ); ?>:
							<?php if ( empty( $course->alternative_date ) ): ?>
                                <strong><?php echo convert_acf_date( $course->date ); ?></strong>
                                <?php if ( $course->vacancy != '') : ?>
                                    <div>
                                        <div class="vacancy">
                                            <span class="vacancy-title"><?php ett('Осталось мест'); ?></span>
                                            <span class="vacancy-count"><?php echo $course->vacancy; ?></span>
                                        </div>
                                    </div>
                                <?php endif; ?>
							<?php else: ?>
                                <strong><?php echo $course->alternative_date; ?></strong>
							<?php endif; ?>
                        </div>
					<?php else: ?>
						<?php $post = get_post(); ?>
						<?php $nearest = get_nearest_courses( true ); ?>
                        <div class="upcoming-courses">
                            <div class="block-title"><?php ett( 'Ближайшие курсы' ); ?></div>
							<?php if ( ! isset( $nearest->{$post->post_name} ) ): ?>
                                <span><?php ett( 'В ближайшее время курсов нет' ); ?></span>
							<?php else: ?>
                                <div class="courses">
									<?php foreach ( $nearest->{$post->post_name}->cities as $city ): ?>
                                        <div class="course">
                                            <a href="<?php echo $city->link; ?>">
                                                <div class="course-title"><?php ett( $city->name ); ?></div>
                                                <div class="time"><?php echo $city->date; ?></div>
                                                <?php if ( $city->vacancy ) : ?>
                                                <div>
                                                    <div class="vacancy<?php echo ! is_site_subdomain() ? '-small' : ''; ?> ">
                                                        <span class="vacancy-title"><?php ett('Осталось мест'); ?></span>
                                                        <span class="vacancy-count"><?php echo $city->vacancy; ?></span>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                            </a>
                                        </div>
									<?php endforeach; ?>
                                </div>
							<?php endif; ?>
                        </div>
					<?php endif; ?>
					<?php if ( is_site_subdomain() ): ?>
                        <button data-popup="sign-course"<?php //echo ! $course->{'reg-open'} ? ' disabled' : ''; ?>><?php ett( 'Записаться' ); ?></button>
					<?php else: ?>
                        <button data-popup="sign-course"<?php //echo ! isset( $nearest->{$post->post_name} ) ? ' disabled' : ''; ?>><?php ett( 'Записаться' ); ?></button>
					<?php endif; ?>
                    <div class="features-list">
						<?php foreach ( $course->advantages as $feature ): ?>
                            <div class="course-feature">
                                <div class="feature-title"><?php echo $feature->title; ?></div>
                                <div class="feature-desc"><?php echo $feature->description; ?></div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </section>
            <section class="profession-for render-deferred">
                <div class="container">
                    <h2><?php ett( 'Эта профессия для тех, кто' ); ?>:</h2>
                    <div class="profession-for-list">
						<?php foreach ( $course->{'list-from-profession'} as $item ): ?>
                            <div class="profession-for-item">
                                <div class="profession-for-image">
									<?php $image = wp_get_attachment_image_url( $item->image ); ?>
									<?php $sizes = get_image_size( $image ); ?>
                                    <img src="<?php echo $image; ?>"
                                         alt="<?php echo $item->desc; ?>" width="<?php echo $sizes['width']; ?>"
                                         height="<?php echo $sizes['height']; ?>">
                                </div>
                                <div class="profession-for-desc"><?php echo $item->desc; ?></div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </section>
            <section class="how-to-start render-deferred">
                <div class="container">
                    <h2><?php ett( 'Как начать карьеру в it?' ); ?></h2>
                    <div class="timeline-steps">
						<?php foreach ( $course->steps as $number => $step ): ?>
                            <div class="timeline-step">
                                <div class="timeline-step-number"><?php echo $number + 1; ?></div>
                                <div class="timeline-step-desc"><?php echo $step->title; ?></div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </section>
            <?php if ( !empty($course->{'list-from-program'}) ) {
                ?>
            <div class="course-program render-deferred">
                <div class="container">
                    <div class="info-block">
                        <div class="info-block-text">
							<?php echo $course->{'call-to-action_name'}; ?>
                        </div>
                        <div class="info-block-button">
                            <button data-popup="sign-course"
                                    class="button button-action" <?php //echo ! $course->{'reg-open'} ? ' disabled' : ''; ?>><?php echo $course->{'call-to-action_text-for-btn'}; ?></button>
                        </div>
                    </div>
                    <h2><?php ett( 'Программа курса' ); ?></h2>
                    <div class="faq">
                        <?php
                        $total_programs   = isset( $course->{'list-from-program'}) ? count( $course->{'list-from-program'}) : 0;
                        $total_programs_2 = isset( $course->{'list-from-program-ext'}) ? count( $course->{'list-from-program-ext'}) : 0;
                        ?>
                        <div class="<?php echo $total_programs_2 > 1 ? 'half' : 'full'; ?>">
                            <?php if ( $course->{'list-title'} ) : ?>
                            <h3><?php echo $course->{'list-title'}; ?></h3>
                            <?php endif; ?>
                            <?php for ($i = 0; $i < $total_programs; $i++ ) : ?>
                                <div class="question">
                                    <div class="question-title"><?php echo ($i+1).'. ', $course->{'list-from-program'}[$i]->title; ?></div>
                                    <div class="question-answer"><?php echo $course->{'list-from-program'}[$i]->desc; ?></div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <?php if ( $total_programs_2 > 1 ) : ?>
                        <div class="half">
                            <?php if ( $course->{'list-title2'} ) : ?>
                            <h3><?php echo $course->{'list-title2'}; ?></h3>
                            <?php endif; ?>
                            <?php for ($i = 0; $i < $total_programs_2; $i++ ) : ?>
                                <div class="question">
                                    <div class="question-title"><?php echo ($i+1).'. ', $course->{'list-from-program-ext'}[$i]->title; ?></div>
                                    <div class="question-answer"><?php echo $course->{'list-from-program-ext'}[$i]->desc; ?></div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>            
            <?php } ?>
            <?php if ( !empty($course->{'summary_fio'}) ) {
                ?>
            <section class="course-resume render-deferred">
                <div class="container">
                    <h2><?php ett( 'Как будет выглядеть ваше резюме после обучения?' ); ?></h2>
                    <div class="resume-card">
                        <div class="resume-top">
                            <div class="resume-top-image">
                                <img src="<?php echo ASSETS_URI . '/img/avatar.png'; ?>" width="100" height="100"
                                     alt="Avatar">
                            </div>
                            <div class="resume-top-title">
                                <div class="resume-top-name"><?php echo $course->{'summary_fio'}; ?></div>
                                <div class="resume-top-position"><?php echo $course->{'summary_position'}; ?></div>
                            </div>
                        </div>
                        <div class="resume-skills-list">
						<?php foreach ( $course->{'summary_group-skills'} as $skills ): ?>
                            <?php 
                            $total_skils = count( $skills->skills);
                            $half = isset($skills->width_half) && $skills->width_half ? $total_skils-1 : intval( $total_skils / 2 );
                            ?>
                            <div class="resume-skills <?php echo $half == $total_skils-1 ? 'half' : '' ?>">
                                <div class="resume-skills-title"><?php echo $skills->title; ?></div>
                                <ul>
									<?php for ( $i = 0; $i <= $half; $i ++ ): ?>
                                        <li><?php echo $skills->skills[ $i ]->name; ?></li>
									<?php endfor; ?>
                                </ul>
                                <?php if ( $half != $total_skils-1 ) : ?>
                                <ul>
									<?php for ( $i = $half + 1; $i < $total_skils; $i ++ ): ?>
                                        <li><?php echo $skills->skills[ $i ]->name; ?></li>
									<?php endfor; ?>
                                </ul>
                                <?php endif; ?>
                            </div>
						<?php endforeach; ?>
                        </div>
                    </div>
                    <div class="employment">
                        <div class="employment-info">
                            <div class="employment-title"><?php echo $course->{'employment_title'}; ?></div>
                            <div class="employment-desc"><?php echo $course->{'employment_desc'}; ?></div>
                        </div>
                        <div class="employment-features">
                            <div class="employment-features-title"><?php echo $course->{'in-deveducation-you-get_title'}; ?></div>
							<?php foreach ( $course->{'in-deveducation-you-get_list'} as $item ): ?>
                                <div class="employment-feature">
                                    <div class="employment-feature-image">
										<?php $employment = wp_get_attachment_image_url( $item->image, 'full' ); ?>
										<?php $sizes = get_image_size( $employment ); ?>
                                        <img src="<?php echo $employment; ?>"
                                             width="<?php echo $sizes['width']; ?>"
                                             height="<?php echo $sizes['height']; ?>"
                                             alt="Employment">
                                    </div>
                                    <div class="employment-feature-text"><?php echo $item->text; ?></div>
                                </div>
							<?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </section>
            <?php } ?>
			<?php if ( is_site_subdomain() ): ?>
                <div class="salaries render-deferred">
                    <div class="container">
                        <div class="info-block">
                            <div class="info-block-text">
								<?php echo $course->{'call-to-action-training_name'}; ?>
                            </div>
                            <div class="info-block-button">
                                <button data-popup="sign-course"
                                        class="button button-action" <?php //echo ! $course->{'reg-open'} ? ' disabled' : ''; ?>><?php echo $course->{'call-to-action_text-for-btn'}; ?></button>
                            </div>
                        </div>
                        <?php if ( !empty($course->graphic) ): ?>
                        <div class="salary-block">
							<?php $salaries = array(); ?>
							<?php foreach ( $course->graphic as $item ): ?>
								<?php $salaries[ intval( $item->year ) ] = intval( str_replace( ' ', '', $item->price ) ); ?>
							<?php endforeach; ?>
                            <div class="salary-text">
                                <h2><?php echo $course->{'title-salaries'}; ?></h2>
								<?php echo $course->{'desc-salaries'}; ?>
                            </div>
                            <div class="salary-chart">
                                <script>let salaries = <?php echo json_encode( $salaries, JSON_UNESCAPED_UNICODE ); ?>;</script>
                                <canvas id="salary-graph"></canvas>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
			<?php endif; ?>
			<?php if ( is_site_subdomain() && !empty( $course->teachers ) ): ?>
                <section class="course-teachers render-deferred">
                    <div class="container">
                        <h2><?php ett( 'Преподаватель' ); ?></h2>
                        <div class="splide teachers-list">
                            <div class="splide__track">
                                <ul class="splide__list">
									<?php foreach ( $course->teachers as $teacher ): ?>
                                        <li class="splide__slide course-teacher">
                                            <div class="teacher-name"><?php echo $teacher->name; ?></div>
                                            <div class="teacher-desc"><?php echo $teacher->description; ?></div>
                                            <div class="teacher-img">
												<?php $image = wp_get_attachment_image_url( $teacher->image, 'full' ); ?>
												<?php $sizes = get_image_size( $image ); ?>
                                                <img src="<?php echo $image; ?>"
                                                     width="<?php echo $sizes['width']; ?>"
                                                     height="<?php echo $sizes['height']; ?>"
                                                     alt="<?php echo $teacher->name; ?>">
                                            </div>
                                        </li>
									<?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
			<?php endif; ?>
            <section class="course-reviews render-deferred">
                <div class="container">
                    <h2><?php ett( 'Что о нас говорят выпускники?' ); ?></h2>
                    <div class="splide reviews-list">
                        <div class="splide__track">
                            <ul class="splide__list">
								<?php foreach ( get_reviews( 'reviews' ) as $review ): ?>
                                    <li class="splide__slide review">
										<?php $size = array( 0, 200 ); ?>
										<?php $thumbnail = get_the_post_thumbnail_url( $review, $size ); ?>
										<?php $sizes = get_image_size( $thumbnail ); ?>
                                        <div class="review-image"
                                             data-link="<?php echo get_post_meta( $review->ID, 'video', true ); ?>">
                                            <img src="<?php echo $thumbnail; ?>"
                                                 width="<?php echo $sizes['width']; ?>"
                                                 height="<?php echo $sizes['height']; ?>"
                                                 alt="<?php echo $review->post_title; ?>"></div>
                                        <div class="review-name"><?php echo $review->post_title; ?></div>
                                    </li>
								<?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section class="course-faq render-deferred">
                <div class="container">
                    <h2><?php ett( 'Самые частые вопросы' ); ?></h2>
                    <div class="faq">
						<?php foreach ( $course->questions as $number => $item ): ?>
                            <div class="question">
                                <div class="question-title"><?php echo $number + 1; ?>
                                    . <?php echo $item->question; ?></div>
                                <div class="question-answer"><?php echo $item->answer; ?></div>
                            </div>
						<?php endforeach; ?>
                    </div>
                    <div class="faq-bottom">
						<?php ett( 'Заполните форму и узнайте больше о том, как построить карьеру в IT с DevEducation!' ); ?>
                        <button data-popup="sign-course"
                                class="button button-action"<?php //echo ! $course->{'reg-open'} ? ' disabled' : ''; ?>><?php ett( 'Получить профессию' ); ?></button>
                    </div>
                </div>
            </section>
            <?php if ( ! is_offline_school() ) : ?>
            <div class="plug render-deferred">
                <div class="plug-title"><?php echo get_post_meta( get_the_ID(), 'courses_plug_title', true ); ?></div>
                <div class="plug-text">
                <?php $text = get_post_meta( get_the_ID(), 'courses_plug_text', true );
                    echo filter_content_titles($text);
                ?>
                </div>
                <div class="plug-title"><?php echo get_post_meta( get_the_ID(), 'courses_plug_title_additional', true ); ?></div>
                <div class="plug-text">
                    <?php $text = get_post_meta( get_the_ID(), 'courses_plug_text_additional', true );
                    echo filter_content_titles($text);
                ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </main>
    <!-- COURSE -->
<?php get_footer(); ?>