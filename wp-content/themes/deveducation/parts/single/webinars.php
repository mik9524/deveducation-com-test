<?php get_header(); ?>
    <!-- GLOSSARY -->
    <main>
        <div class="container">
			<?php echo print_breadcrumbs(); ?>
            <div class="webinar">
                <div class="webinar-info">
					<?php $thumbnail = get_post_meta( get_the_ID(), 'youtube_thumbnail', true ); ?>
                    <div class="webinar-preview" data-link="<?php echo strip_tags( get_the_excerpt() ); ?>">
                        <img src="<?php echo $thumbnail; ?>"
                             alt="<?php the_title(); ?>" width="750" height="422">
                    </div>
                    <h1><?php the_title(); ?></h1>
                    <div class="webinar-category">
						<?php echo wp_get_object_terms( get_the_ID(), 'webinars_cat' )[0]->name; ?>
                    </div>
                    <div class="webinar-content"><?php the_content(); ?></div>
                </div>
				<?php $recommends = array( 'post_type' => 'webinars', 'numberposts' => 8, 'orderby' => 'rand' ); ?>
                <div class="recommendations">
                    <div class="recommendations-title"><?php ett( 'Рекомендованные' ); ?></div>
                    <div class="recommendations-list">
						<?php foreach ( get_posts( $recommends ) as $webinar ): ?>
                            <div class="webinar-recommend render-deferred">
                                <a href="<?php echo get_permalink( $webinar->ID ); ?>">
                                    <div class="webinar-recommend-image">
                                        <img src="<?php echo get_post_meta( $webinar->ID, 'youtube_thumbnail', true ); ?>"
                                             width="285" height="160" alt="<?php echo $webinar->post_title; ?>">
                                    </div>
                                </a>
                                <div class="webinar-recommend-content">
                                    <div class="webinar-recommend-title"><?php echo $webinar->post_title; ?></div>
                                    <div class="webinar-recommend-desc"><?php echo limit_text( $webinar->post_content ); ?></div>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
		<?php get_template_part( 'parts/general/glossary-join-form' ); ?>
    </main>
    <!-- GLOSSARY -->
<?php get_footer(); ?>