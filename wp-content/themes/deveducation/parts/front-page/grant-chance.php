<section class="grant-chance render-deferred">
	<div class="container">
		<div class="grant-chance-box">
			<h2><?php ett( 'Заполни форму и узнай, как получить грант на обучение' ); ?></h2>
			<div class="grant-chance-box__form">
				<form data-ajax-action="grant_chance">
					<div>
						<div class="grant-chance-flex-box">
							<?php wp_nonce_field( - 1, 'grant_chance' ); ?>
							<input type="text" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
							<input type="email" name="email" placeholder="Email" required>
						</div>
						<div class="grant-chance-flex-box">
							<input class="phone-mask" name="phone" type="text" placeholder="<?php ett( 'Телефон' ); ?>"
								   required>
							<div class="custom-select">
								<?php 
									$args = is_offline_school() ? '' : array( 'site__not_in' => array( get_main_site_id() ) );
									$all_sites = get_sites( $args );
									$site_name = 'Выберите город';
									$site_id = '';
									foreach ( $all_sites as $site ) {
										if ( get_current_blog_id() == $site->blog_id ) {
											$site_name = $site->blogname;
											$site_id = $site->blog_id;
											break;
										}
									}
								?>
                       			<div class="select"><?php ett($site_name); ?></div>
								<div class="choose-city">
									<?php foreach ( $all_sites as $site ):  ?>
										<?php 
											if ( is_disabled_site($site->domain) ) continue; ?>
										<div class="choose-city-list"
											 data-value="<?php echo $site->blog_id; ?>" <?php echo get_current_blog_id() == $site->blog_id ? 'data-selected="1"' : ''; ?>><?php ett( $site->blogname ); ?></div>
									<?php endforeach; ?>
								</div>
								<input class="choose-city-input" value="<?php echo $site_id; ?>" type="text" name="city_id" hidden required>
							</div>
						</div>
						<div class="grant-chance-flex-box">
							<p class="terms-and-cond">
								<span class="check_tnc">
									<input type="checkbox" name="agree_personal_data" id="agree_personal_data" required><label
											for="agree_personal_data"></label>
								</span>
								<span><?php ett( 'Заполняя форму регистрации, я принимаю' ); ?> <a
											href="<?= get_permalink( get_page_by_path( 'politika-konfidentsialnosti' ) ); ?>"><?php ett( 'условия обработки персональных данных' ) ?></a></span>
							</p>
							<button class="grant-chance-button"><?php ett( 'Получить грант' ); ?></button>
						</div>
					</div>
				</form>
				<div class="form-result"><?php ett( 'Успешно отправлено' ); ?></div>
			</div>
		</div>
	</div>
</section>