<div class="bottom-screen render-deferred">
    <div class="container">
        <div class="bottom-screen__title">
            <span><?php ett( 'Не уверен какая профессия подходит именно тебе?' ); ?></span>
            <br> <span><?php ett( 'Мы поможем с выбором' ) ?>.</span></div>
        <button type="button" class="button-home-page"
                data-popup="sign-course"><?php ett( 'Оставить заявку' ); ?></button>

        <svg role="img" class="bulb first-bulb">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#bulb"></use>
        </svg>
        <svg role="img" class="chart">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#chart"></use>
        </svg>
        <svg role="img" class="coin">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#coin"></use>
        </svg>
        <svg role="img" class="diagram">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#diagram"></use>
        </svg>
        <svg role="img" class="message">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#message"></use>
        </svg>
        <svg role="img" class="rocket">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#rocket"></use>
        </svg>
        <svg role="img" class="search">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#search"></use>
        </svg>
        <svg role="img" class="bulb second-bulb">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#bulb"></use>
        </svg>
        <svg role="img" class="bulb third-bulb">
            <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#bulb"></use>
        </svg>
    </div>
</div>
