<section class="nearest-courses front-page-section render-deferred">
    <div class="container">
        <h2><?php ett( 'Ближайшие курсы' ); ?></h2>
        <div class="splide nearest-courses-list">
            <div class="splide__track">
                <ul class="splide__list">
					<?php foreach ( get_nearest_courses( true ) as $course ): ?>
                        <li class="splide__slide course">
                            <div class="course-top">
                                <svg role="img" class="lines">
                                    <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#lines"></use>
                                </svg>
                                <svg role="img" class="code">
                                    <use data-href="<?= get_template_directory_uri() ?>/assets/img/svg-symbols.svg#code"></use>
                                </svg>
                                <p><?php echo $course->title; ?></p>
                            </div>
                            <div class="course-bottom">
								<?php foreach ( $course->cities as $city ): ?>
                                    <a href="<?php echo $city->link; ?>" class="course-city">
										<?php ett( $city->name, true ); ?>
                                        <span><?php echo $city->date; ?></span>
                                    </a>
								<?php endforeach; ?>
                            </div>
                            <div class="course-shadow"></div>
							<?php if ( count( $course->cities ) > 1 ): ?>
                                <div class="arrow-next"></div>
							<?php endif; ?>
                        </li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>