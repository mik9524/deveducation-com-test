<div class="mountains render-deferred">
	<?php $features = get_acf_repeater( get_the_ID(), 'features' ); ?>
    <div class="container">
        <div class="mountains-flex">
            <div class="mountains-items">
                <div class="mountains-item">
                    <div class="mountains-item-top">
                        <span class="number">87</span><span class="ancillary">%</span>
                    </div>
                    <div class="mountains-item-bottom"><?php echo $features[0]->subtitle; ?></div>
                </div>
                <div class="mountains-item">
                    <div class="mountains-item-top">
                        <?php 
                        if ( is_ukrainian_site() ) {
                            $cost = 0;
                        } else {
                            $cost = 40;
                        }
                        ?>
                        <span class="number"><?php echo $cost; ?></span><span class="ancillary">$</span>
                    </div>
                    <div class="mountains-item-bottom"><?php echo $features[1]->subtitle; ?></div>
                </div>
                <div class="mountains-item">
                    <div class="mountains-item-top">
                        <span class="number">800</span><span class="ancillary">+</span>
                    </div>
                    <div class="mountains-item-bottom"><?php echo $features[2]->subtitle; ?></div>
                </div>
                <div class="mountains-item">
                    <div class="mountains-item-top">
                        <span class="number">4</span><span class="ancillary"><?php ett( 'мес.' ) ?></span>
                    </div>
                    <div class="mountains-item-bottom"><?php echo $features[3]->subtitle; ?></div>
                </div>
            </div>
            <div class="stiven-image">
                <img src="<?= get_template_directory_uri() . '/assets/img/stiven.svg' ?>" alt="stiven leopard"
                     width="260" height="485">
            </div>
            <div class="mountains-about">
                <p class="mountains-about-top"><?php echo get_post_meta( get_the_ID(), 'mountains_about_top', true ); ?></p>
                <p class="mountains-about-bottom"><?php echo get_post_meta( get_the_ID(), 'mountains_about_bottom', true ); ?></p>
            </div>
        </div>
    </div>
</div>