<?php if ( get_post_meta(get_the_ID(), 'block_social_title', true ) ) : ?>
<section class="more-info-section render-deferred">
	<div class="container">
		<div class="more-info">
			<div class="more-info-content">
				<h2><?php echo get_post_meta(get_the_ID(), 'block_social_title', true ); ?></h2>
				<p class="more-info-text"><?php echo get_post_meta(get_the_ID(), 'block_social_text', true ); ?></p>
				<p class="more-info-bid-text"><?php echo get_post_meta(get_the_ID(), 'block_social_sub_text', true ); ?></p>

				<?php $social_block_button = get_post_meta(get_the_ID(), 'block_social_button', true ); ?>
				<?php if ( ! empty( $social_block_button['block_social_button_link'] ) ) : ?>
					<div class="more-info-button">
						<a href="<?= $social_block_button['block_social_button_link']; ?>" rel="nofollow">
							<img src="<?= $social_block_button['block_social_button_image']['url']; ?>" alt="telegram"
								 width="50"
								 height="50">
							<?= $social_block_button['block_social_button_text']; ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="more-info-image">
				<div class="img-wrapper">
					<img src="/wp-content/themes/deveducation/assets/img/stiven-horizontal.svg"
						 alt="Leopard" width="150" height="85">
				</div>
			</div>
			<div class="more-info-social">
				<p class="more-info-social-title"><?php echo get_post_meta(get_the_ID(), 'block_social_text_over_social_link', true ); ?></p>
				<ul class="more-info-social-items">
					<?php
					$necessarySocialList = [ 'instagram', 'tiktok', 'facebook', 'youtube', 'linkedin', 'vk' ];
					foreach ( get_acf_options( 'socials' ) as $social ):
						
						if ( is_russian_site() && true == preg_match('/(facebook|instagram)/', $social->link ) ) {
							continue;
						}

						$isNecessary = false;

						foreach ( $necessarySocialList as $necessarySocialItem ) {
							$isNecessary = strripos( $social->link, $necessarySocialItem );
							if ( $isNecessary ) {
								break;
							}
						}

						if ( ! $isNecessary ) {
							continue;
						} ?>
						<li>
							<a href="<?php echo $social->link; ?>" target="_blank" rel="nofollow">
								<img src="<?php echo wp_get_attachment_image_url( $social->image ); ?>"
									 alt="Social icon" width="60" height="60">
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>