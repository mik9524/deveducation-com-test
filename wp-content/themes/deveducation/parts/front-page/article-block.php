<?php $blogs = get_posts( array( 'post_type' => 'blog', 'numberposts' => 8 ) ); ?>
<?php if ( count( $blogs ) ): ?>
    <section class="articles-block render-deferred">
        <div class="container">
            <h2><?php ett( 'Блог' ); ?></h2>
            <div class="articles-list">
				<?php foreach ( $blogs as $blog ): ?>
                    <div class="article">
						<?php $link = get_permalink( $blog->ID ); ?>
						<?php $origin = get_post_meta( $blog->ID, 'origin', true ); ?>
						<?php $size = array( 0, '200' ); ?>
						<?php $thumbnail = $origin['image'] ?? get_the_post_thumbnail_url( $blog->ID, $size ); ?>
						<?php $title = preg_replace( '/"(.*)"/', '«$1»', $blog->post_title ); ?>
                        <a href="<?php echo $link; ?>">
                            <img src="<?php echo $thumbnail; ?>" alt="<?php echo $title; ?>"
                                 width="350" height="200">
                        </a>
                        <div class="article-desc">
                            <div class="article-title">
                                <a href="<?php echo $link; ?>"><?php echo $title; ?></a>
                            </div>
                            <div class="article-author">
                                <span><?php echo get_user_name( $origin['author'] ?? $blog->post_author ); ?></span>
                                <time datetime="<?php echo $blog->post_date; ?>"><?php echo date( 'd.m.Y', strtotime( $blog->post_date ) ); ?></time>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
            <div class="article-more-events">
                <a href="<?php echo get_post_type_archive_link( 'blog' ); ?>"
                   class="more-events"><?php ett( 'Больше статей' ); ?></a>
            </div>
        </div>
    </section>
<?php endif; ?>