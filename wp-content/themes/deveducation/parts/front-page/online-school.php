<section class="more-info-section render-deferred">
    <div class="container">
        <div class="more-info">
            <div class="more-info-content">
                <h2><?php echo get_post_meta(get_the_ID(), 'onlinesch_title', true ); ?></h2>
                <div class="more-info-text"><?php echo wpautop(get_post_meta(get_the_ID(), 'onlinesch_descr', true )); ?></div>
                <div class="more-info-button">
                <button type="button" class="button-home-page" data-popup="sign-course-online"><?php ett('Оставить заявку'); ?></button>
                </div>
                <div class="more-info-text"><?php echo wpautop(get_post_meta(get_the_ID(), 'onlinesch_descr-ext', true )); ?></div>
            </div>
            <div class="more-info-image">
                <div class="img-wrapper">
                    <img src="/wp-content/themes/deveducation/assets/img/stiven-horizontal.svg"
                         alt="Leopard" width="150" height="85">
                </div>
            </div>
        </div>
    </div>
</section>