<?php if ( get_post_meta( get_the_ID(), 'g_visible', true ) ): ?>
    <section class="graduates render-deferred">
        <div class="container graduates-splide">
            <div class="graduates-title"><span class="graduates-title-purple"><span
                            class="graduates-title-8">8</span><span
                            class="graduates-title-7">7</span>%</span> <?php ett( 'выпускников уже работают в IT' ); ?>
            </div>
            <div class="graduates-list-container">
                <ul class="graduates-list">
                </ul>
            </div>
        </div>
    </section>
<?php endif; ?>