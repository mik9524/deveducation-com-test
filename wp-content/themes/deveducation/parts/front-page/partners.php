<?php if ( get_post_meta( get_the_ID(), 'partners_visible', true ) ): ?>
    <div class="partners render-deferred">
        <div class="container">
            <div class="partners-title"><?php ett( 'Сотрудничаем с международными компаниями' ); ?></div>
            <div class="partners-list-container">
                <div class="partners-list">
					<?php foreach ( get_acf_repeater( get_the_ID(), 'partners' ) as $partner ): ?>
						<?php if ( ! $partner->logo ) {
							continue;
						} ?>
                        <a href="<?php echo $partner->link; ?>" target="_blank" rel="nofollow"><img
                                    src="<?php echo wp_get_attachment_image_url( $partner->logo, 'full' ); ?>"
                                    width="150" height="50" alt="Partner"></a>
					<?php endforeach; ?>
					<?php foreach ( get_acf_repeater( get_the_ID(), 'partners' ) as $partner ): ?>
						<?php if ( ! wp_get_attachment_image_url( $partner->logo, 'full' ) ) {
							continue;
						} ?>
                        <a href="<?php echo $partner->link; ?>" target="_blank" rel="nofollow"><img
                                    src="<?php echo wp_get_attachment_image_url( $partner->logo, 'full' ); ?>"
                                    width="150" height="50" alt="Partner"></a>
					<?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>