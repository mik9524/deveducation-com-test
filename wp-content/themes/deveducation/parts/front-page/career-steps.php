<section class="career-steps render-deferred">
    <div class="container">
        <h2><?php echo get_post_meta( get_the_ID(), 'step_block_title', true ); ?></h2>
        <div class="splide steps">
            <div class="splide__track">
                <div class="splide__list">
					<?php foreach ( get_acf_repeater( get_the_ID(), 'steps' ) as $number => $step ): ?>
                        <div class="step splide__slide">
                            <div class="step-blocks">
                                <div class="step-image">
                                    <img src="<?php echo wp_get_attachment_image_url( $step->image_steps ) ?>"
                                         width="120" height="120" alt="steps">
                                </div>
                                <div class="step-number"><?php ett( 'Шаг' ) ?> <span><?php echo ++ $number; ?></span>
                                </div>
                                <svg width="118" height="5" viewBox="0 0 118 5" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <rect x="0.736328" width="116.529" height="5" rx="2.5" fill="#241970"/>
                                </svg>
                            </div>
                            <div class="step-desc"><?php echo $step->text; ?></div>
                        </div>
					<?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="step-sign-course">
            <button data-popup="sign-course" class="button-sign-course-career">
				<?php ett( 'Записаться на курс' ); ?>
            </button>
        </div>
    </div>
</section>