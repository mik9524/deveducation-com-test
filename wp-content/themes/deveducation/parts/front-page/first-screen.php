<section class="first-screen video-bg">
    <video preload="none" loop muted poster="<?php echo ASSETS_URI; ?>/img/onload.jpg"
           class="fullscreen-bg__video">
        <source src="#" data-src="https://deveducation.com/wp-content/uploads/sites/2/2020/10/video-bg.mp4"
                type="video/mp4">
        <source src="<?php echo ASSETS_URI; ?>/img/header__bg-video.jpg">
    </video>
    <div class="container">
        <div class="first-screen__content">
            <div class="first-screen__info">
                <h1><?= get_post_meta( get_the_ID(), 'title', true ); ?></h1>
                <div class="first-screen__text">
					<?= get_post_meta( get_the_ID(), 'description', true ); ?>
                </div>
            </div>
            <button data-popup="sign-course">
				<?php ett( 'Записаться на курс' ); ?>
            </button>

            <div class="first-screen__cities">
                <div class="first-screen__cities-title"><?php ett( 'Выберите филиал в вашем городе' ) ?>:</div>
                <div class="first-screen__cities-list" data-list-title="<?php ett( 'Филиалы' ) ?>">
                    <a href="<?= get_site_lang_url( 5 ); ?>" class="first-screen__cities-item">
                        <span><?php ett( 'Киев' ); ?></span>
                    </a>
                    <a href="<?= get_site_lang_url( 4 ); ?>" class="first-screen__cities-item">
                        <span><?php ett( 'Днепр' ); ?></span>
                    </a>
                    <a href="<?= get_site_lang_url( 3 ); ?>" class="first-screen__cities-item">
                        <span><?php ett( 'Харьков' ); ?></span>
                    </a>
                    <a href="<?= get_site_lang_url( 6 ); ?>" class="first-screen__cities-item">
                        <span><?php ett( 'Баку' ); ?></span>
                    </a>
                    <a href="<?= get_site_lang_url( 2 ); ?>" class="first-screen__cities-item disabled">
                        <span><?php ett( 'Санкт-Петербург' ); ?></span>
                    </a>
                    <a href="<?= get_site_lang_url( 8 ); ?>" class="first-screen__cities-item disabled">
                        <span><?php ett( 'Екатеринбург' ); ?></span>
                    </a>
                    <a href="<?= get_site_lang_url( 7 ); ?>" class="first-screen__cities-item disabled">
                        <span><?php ett( 'Казань' ); ?></span>
                    </a>
                </div>
            </div>
        </div>
        <aside class="first-screen__aside">
			<?php foreach ( get_acf_repeater( get_the_ID(), 'advantages' ) as $advantage ) { ?>
                <div class="first-screen__aside-item">
                    <div class="first-screen__aside-item__image">
                        <img src="<?php echo wp_get_attachment_image_url( $advantage->image ); ?>"
                             alt="<?php echo $advantage->title; ?>" width="78" height="78">
                    </div>
                    <div class="first-screen__aside-item__title">
						<?= $advantage->title; ?>
                    </div>
                    <div class="first-screen__aside-item__text">
						<?= $advantage->description; ?>
                    </div>
                </div>
			<?php } ?>
        </aside>
    </div>
</section>