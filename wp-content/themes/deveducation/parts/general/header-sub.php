<!-- HEADER -->
<header class="<?php echo isset( $args['transparent'] ) ? 'transparent' : ''; ?>">
    <div class="container">
        <div class="header-logo">
            <a href="<?php echo get_home_lang_url(); ?>">
                <img src="<?php echo wp_get_attachment_image_url( get_theme_mod( 'custom_logo' ), 'full' ); ?>"
                     alt="Site logo" width="200" height="36">
            </a>
            <?php if ( is_site_subdomain() ): ?>
                <button class="courses-menu"><?php ett( 'Курсы' ); ?></button>
            <?php endif; ?>
            <button class="mobile-menu" aria-hidden="true" tabindex="-1"><img
                        src="<?php echo ASSETS_URI . '/img/icons/menu.svg'; ?>" alt="Burger Menu" width="18"
                        height="12"></button>
        </div>
        <input type="checkbox" id="burger-check">
        <label id="burger" for="burger-check">
            <img src="<?php echo ASSETS_URI . '/img/icons/menu.svg'; ?>" alt="Burger Menu" width="18"
                 height="12">
        </label>
        <div class="header-nav">
            <?php if ( has_nav_menu( 'primary' ) ) : ?>
                <?php echo get_menu_by_location_sub( 'primary', 'dropdown' ); ?>
            <?php endif; ?>
            <?php if ( is_site_subdomain() ): ?>
                <div class="location-selector">
                    <span><?php ett( get_bloginfo( 'blogname' ) ); ?></span>
                    <?php echo get_site_selector(); ?>
                </div>
            <?php endif; ?>
            <?php if ( function_exists( 'pll_the_languages' ) ): ?>
                <div class="lang-selector">
                    <span><?php echo pll_current_language(); ?></span>
                    <ul>
                        <?php pll_the_languages( array( 'display_names_as' => 'slug' ) ); ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
        <?php if ( is_site_subdomain() ): ?>
            <div class="header-courses">
                <ul>
                    <?php foreach (
                        get_posts( array(
                            'post_type'   => 'courses',
                            'numberposts' => - 1
                        ) ) as $course
                    ): ?>
                        <?php $excluded = array( 'python', 'js', 'fotoshop', 'php', 'react', 'veb-dizajn', 'kompyuternye-kursy' ); ?>
                        <?php if ( is_offline_school() || ( ! is_offline_school() && ! in_array( $course->post_name, $excluded ) ) ): ?>
                            <li><a href="<?php echo get_permalink( $course ); ?>"><?php echo $course->post_title; ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <button class="button button-action"
                        data-popup="sign-course"><?php ett( 'Записаться на курс' ); ?></button>
            </div>
        <?php endif; ?>
    </div>
</header>
<!-- HEADER -->