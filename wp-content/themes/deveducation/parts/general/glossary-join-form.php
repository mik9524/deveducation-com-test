<section class="join-form">
    <div class="container">
        <div class="join-form-title"><?php ett( 'Заполни форму и стань одним из успешных выпускников DevEducation!' ); ?></div>
        <form data-ajax-action="join_us" method="post">
			<?php wp_nonce_field( - 1, 'join' ); ?>
            <input class="form-input" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
            <input class="form-input phone-mask" name="phone" placeholder="<?php ett( 'Телефон' ); ?>"
                   required>
            <button class="button button-primary form-button"><?php ett( 'Отправить' ); ?></button>
            <div class="personal-conditions">
                <label>
                    <input checked required
                           type="checkbox"> <?php ett( 'Заполняя форму регистрации, я принимаю условия обработки персональных данных' ); ?>
                </label>
            </div>
        </form>
    </div>
</section>