<div class="popup-overlay" id="sign-course">
    <div class="popup">
        <div class="popup-close"></div>
        <div class="popup-title"><?php ett( 'Записаться на курс' ); ?></div>
        <div class="popup-content">
            <form data-ajax-action="sign_course">
				<?php wp_nonce_field( - 1, 'sign_course' ); ?>
                <input type="text" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
                <input class="phone-mask" type="text" name="phone" placeholder="<?php ett( 'Телефон' ); ?>" required>
                <input type="email" placeholder="Email" name="email" required>
                <div class="custom-select">
                    <?php 
                        $args = is_offline_school() ? '' : array( 'site__not_in' => array( get_main_site_id() ) );
                        $all_sites = get_sites( $args );
                        $site_name = 'Выберите город';
                        $site_id = '';
                        foreach ( $all_sites as $site ) {
                            if ( get_current_blog_id() == $site->blog_id ) {
                                $site_name = $site->blogname;
                                $site_id = $site->blog_id;
                                break;
                            }
                        }
                    ?>
					<div class="select city"><?php ett($site_name); ?></div>
                    <div class="choose-city">
						<?php foreach ( $all_sites as $site ): ?>
                            <?php if ( is_disabled_site($site->domain) ) continue; ?>
                            <div class="choose-city-list"
                                 data-value="<?php echo $site->blog_id; ?>" <?php echo get_current_blog_id() == $site->blog_id ? 'data-selected="1"' : ''; ?>><?php ett( $site->blogname ); ?></div>
						<?php endforeach; ?>
                    </div>
                    <input class="choose-city-input" value="<?php echo $site_id; ?>" type="text" name="city_id" hidden required>
                </div>
                <div class="custom-select">
                    <div class="select course"><?php ett( 'Выберите курс' ); ?></div>
                    <div class="courses-list"></div>
                    <input class="choose-course-input" value="" type="text" name="course" hidden required>
                </div>
                <button class="button button-action"><?php ett( 'Отправить' ); ?></button>
            </form>
        </div>
    </div>
</div>