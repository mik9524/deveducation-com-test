<section class="nearest-courses">
    <h2><?php ett( 'Ближайшие курсы' ); ?></h2>
    <div class="container">
        <div class="splide nearest-courses-list">
            <div class="splide__track">
                <ul class="splide__list">
                    <?php foreach ( get_nearest_courses( true ) as $course ): ?>
                        <li class="splide__slide course">
                            <div class="course-top">
                                <img src="<?php echo $course->image; ?>"
                                     alt="<?php echo $course->title; ?>"
                                     width="80">
                                <span><?php echo $course->title; ?></span>
                            </div>
                            <div class="course-bottom">
                                <?php foreach ( $course->cities as $city ): ?>
                                    <a href="<?php echo $city->link; ?>" class="course-city">
                                        <?php ett( $city->name, true ); ?>
                                        <span><?php echo $city->date; ?></span>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                            <div class="course-shadow"></div>
                            <?php if ( count( $course->cities ) > 1 ): ?>
                                <div class="arrow-next"></div>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>