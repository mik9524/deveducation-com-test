<div class="join-form">
    <h2><?php ett( 'Заполни форму и стань одним из успешных выпускников DevEducation!' ); ?></h2>
    <form data-ajax-action="join_us" method="post">
		<?php wp_nonce_field(-1, 'join'); ?>
        <input class="form-input" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
        <input class="form-input phone-mask" name="phone" placeholder="<?php ett( 'Телефон' ); ?>"
               required>
        <button class="button button-primary form-button"><?php ett( 'Отправить' ); ?></button>
    </form>
</div>