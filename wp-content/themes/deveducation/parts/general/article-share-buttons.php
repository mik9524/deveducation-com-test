<div class="share-buttons">
   <?php if ( ! is_russian_site() ) : ?>
    <a rel="nofollow" href="#" title="Поделиться в Facebook" target="_blank" class="share-fb"
       onclick="window.open('//www.facebook.com/sharer/sharer.php?u=' + window.location.href, '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0');return false"></a>
   <?php endif; ?>
    <a rel="nofollow" href="#" title="Добавить в Telegram" target="_blank" class="share-tg"
       onclick="window.open('//telegram.me/share/url?url=' + window.location.href + '&amp;text=' + document.head.title, '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0');return false"></a>
    <a rel="nofollow" href="#" title="Поделиться в ВКонтакте" target="_blank" class="share-vk"
       onclick="window.open('//vk.com/share.php?url=' + window.location.href + '&amp;title=' + document.head.title, '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0');return false"></a>
</div>