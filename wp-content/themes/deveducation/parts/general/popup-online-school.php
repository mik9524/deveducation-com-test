<div class="popup-overlay" id="sign-course-online">
    <div class="popup">
        <div class="popup-close"></div>
        <div class="popup-title"><?php ett( 'Оставить заявку' ); ?></div>
        <div class="popup-content">
            <form data-ajax-action="sign_course">
				<?php wp_nonce_field( - 1, 'sign_course_online' ); ?>
                <input type="text" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
                <input class="phone-mask" type="text" name="phone" placeholder="<?php ett( 'Телефон' ); ?>" required>
                <input type="email" placeholder="Email" name="email" required>                
                <input class="choose-city-input" value="9999" type="text" name="city_id" hidden="" required="">
                <button class="button button-action"><?php ett( 'Отправить' ); ?></button>
            </form>
        </div>
    </div>
</div>