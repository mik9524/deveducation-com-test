<!-- HEADER -->
<?php $courses_page = get_page_by_path('courses')->ID;?>
<header class="<?php echo isset($args['transparent']) ? 'transparent' : ''; ?> <?= is_page($courses_page) ? 'courses-page' : ''?>">
	<div class="container">
		<div class="header__top-line">
			<a href="<?php echo get_home_lang_url(); ?>">
				<img class="header__logo"
					 src="<?php echo wp_get_attachment_image_url(get_theme_mod('custom_logo'), 'full'); ?>"
					 alt="Site logo" width="200" height="36">
			</a>
		</div>

		<div class="header-nav">
			<?php if (has_nav_menu('primary')) : ?>
				<?php echo get_menu_by_location_origin('primary', 'dropdown'); ?>
			<?php endif; ?>

			<?php if (is_site_subdomain()): ?>
				<div class="location-selector">
					<span><?php ett(get_bloginfo('blogname')); ?></span>
					<?php echo get_site_selector(); ?>
				</div>
			<?php endif; ?>
		</div>

		<?php if (is_site_subdomain()): ?>
			<div class="header-courses">
				<ul>
					<?php foreach (
						get_posts(array(
							'post_type' => 'courses',
							'numberposts' => -1
						)) as $course
					): ?>
						<li><a href="<?php echo get_permalink($course); ?>"><?php echo $course->post_title; ?></a></li>
					<?php endforeach; ?>
				</ul>
				<button class="button button-action"
						data-popup="sign-course"><?php ett('Записаться на курс'); ?></button>
			</div>
		<?php endif; ?>

		<?php if (function_exists('pll_the_languages')): ?>
			<div class="lang-selector">
				<span class="langCurrent"><?php echo pll_current_language(); ?></span>
				<ul>
					<?php pll_the_languages(array('display_names_as' => 'slug')); ?>
				</ul>
			</div>
		<?php endif; ?>
		<button type="button" class="mobile-menu"><span class="burger"></span></button>
		<ul class="header-social-links">
			<?php
			$necessarySocialList = ['instagram', 'tiktok', 'facebook', 'youtube', 'linkedin'];
			foreach (get_acf_options( 'socials' ) as $social):

				if ( is_russian_site() && true == preg_match('/(facebook|instagram)/', $social->link ) ) {
					continue;
				}
				
				$isNecessary = false;

				foreach ($necessarySocialList as $necessarySocialItem) {
					$isNecessary = strripos($social->link, $necessarySocialItem);
					if ($isNecessary) break;
				}

				if (!$isNecessary) continue; ?>
				<li>
					<a href="<?php echo $social->link; ?>" target="_blank" rel="nofollow"><img
								src="<?php echo wp_get_attachment_image_url($social->image); ?>"
								alt="Social icon" width="27" height="27"></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</header>
<!-- HEADER -->
