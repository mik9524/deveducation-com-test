<?php

$schema = array(
	'@context'     => 'https://schema.org/',
	'@type'        => 'Organization',
	'name'         => 'DevEducation',
	'url'          => 'https://deveducation.com/',
	'logo'         => wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' )[0],
	'address'      => array(),
	'contactPoint' => array(),
	'sameAs'       => array( 'https://www.facebook.com/IT.DevEducation' )
);

if ( is_site_subdomain() ) {
	$contact                  = get_contacts_data();
	$schema['address'][]      = array(
		'@type'           => 'PostalAddress',
		'streetAddress'   => $contact->address ?? '',
		'addressLocality' => $contact->city ?? '',
		'addressCountry'  => get_country_code()
	);
	$schema['contactPoint'][] = array(
		'@type'       => 'ContactPoint',
		'telephone'   => $contact->phone ?? '',
		'contactType' => 'sales',
		'areaServed'  => array( get_country_code() ),
		'email'       => $contact->email ?? ''
	);
} else if ( $contacts = get_page_by_path( '/contacts' ) ) {
	foreach ( get_acf_repeater( $contacts->ID, 'contacts' ) as $contact ) {
		$schema['address'][]      = array(
			'@type'           => 'PostalAddress',
			'streetAddress'   => $contact->address ?? '',
			'addressLocality' => $contact->city ?? '',
			'addressCountry'  => get_country_code()
		);
		$schema['contactPoint'][] = array(
			'@type'       => 'ContactPoint',
			'telephone'   => $contact->phone ?? '',
			'contactType' => 'sales',
			'areaServed'  => array( get_country_code() ),
			'email'       => $contact->email ?? ''
		);
	}
}
?>
<?php
if( is_front_page() ): ?>
	<script type="application/ld+json"><?php echo json_encode( $schema, JSON_UNESCAPED_UNICODE ); ?></script>
<?php endif; ?>
