<?php if ( is_site_subdomain() && ! is_user_logged_in() ): ?>
	<!-- POPUP -->
	<div class="popup-overlay render-deferred" id="account-sign">
		<div class="popup">
			<div class="popup-close"></div>
			<div class="popup-title"><?php ett( 'Личный кабинет' ); ?></div>
			<div class="popup-content">
				<form data-ajax-action="auth">
					<?php wp_nonce_field( - 1, 'account' ); ?>
					<input type="email" placeholder="Email" name="email" required>
					<input type="password" placeholder="<?php ett( 'Пароль' ); ?>" name="password" required>
					<button class="button button-action"><?php ett( 'Войти' ); ?></button>
				</form>
			</div>
		</div>
	</div>
	<!-- POPUP -->
<?php endif; ?>
<!-- POPUP -->
<div class="popup-overlay render-deferred" id="email-subscription">
	<div class="popup">
		<div class="popup-close"></div>
		<div class="popup-title"><?php ett( 'Получите доступ к закрытой IT-рассылке' ); ?></div>
		<div class="popup-content">
			<ul>
				<li><?php ett( 'Авторские учебные материалы' ); ?></li>
				<li><?php ett( 'Бесплатное посещение IT-мероприятий' ); ?></li>
				<li><?php ett( 'Разбор главных событий отрасли' ); ?></li>
			</ul>
			<form action="https://cp.unisender.com/ru/subscribe?hash=6st81br1a7cpr3ncb83awg5kckn3a5w31iximhi3m361fhc3w31qo"
				  method="POST">
				<input type="email" placeholder="Email" name="email" required>
				<input type="hidden" name="charset" value="UTF-8">
				<input type="hidden" name="default_list_id" value="17276885">
				<input type="hidden" name="overwrite" value="2">
				<input type="hidden" name="is_v5" value="1">
				<button id="email-subscription-button"
						class="button button-action form-button"><?php ett( 'Подписаться' ); ?></button>
			</form>
		</div>
	</div>
</div>
<!-- POPUP -->
<!-- POPUP -->
<?php
if ( is_site_subdomain() ) {
	get_template_part( 'parts/general/popup-sign-course-sub' );
} else {
	get_template_part( 'parts/general/popup-sign-course' );
}
?>
<!-- POPUP -->
<?php 
	if ( ( is_front_page() && ! is_site_subdomain() ) || ( is_front_page() && is_offline_school() ) ) {
		get_template_part( 'parts/general/popup-online-school' );
	} 
?>
<div class="fixed-button render-deferred">
	<button data-popup="sign-course"><?php ett( 'Записаться на курс' ); ?></button>
</div>
<!-- FOOTER -->
<footer class="render-deferred">
	<div class="container">
		<?php echo get_menu_by_location_sub( 'primary', 'footer-menu' ); ?>
		<?php if ( is_site_subdomain() ): ?>
			<ul class="courses-list">
				<?php foreach ( get_posts( array( 'post_type' => 'courses', 'numberposts' => '10' ) ) as $post ): ?>
				<?php $excluded = array( 'python', 'js', 'fotoshop', 'php', 'react', 'veb-dizajn', 'kompyuternye-kursy' ); ?>
				<?php if ( is_offline_school() || ( ! is_offline_school() && ! in_array( $post->post_name, $excluded ) ) ): ?>             
					<li><a href="<?php echo get_permalink( $post->ID ); ?>"><?php echo $post->post_title; ?></a></li>
				<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<ul class="social-links">
			<?php foreach ( get_acf_options( 'socials' ) as $social ):		
				if ( is_russian_site() && true == preg_match('/(facebook|instagram)/', $social->link ) ) {
					continue;
				}
			?>
				<li>
					<a href="<?php echo $social->link; ?>" target="_blank" rel="nofollow">
						<img src="<?php echo wp_get_attachment_image_url( $social->image ); ?>"
							alt="Social icon"
							title="Social icon"
							width="25"
							height="25"
							loading="lazy">
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
		<div class="bottom-links">
			<a href="<?php echo get_permalink( get_page_by_path( 'politika-konfidentsialnosti' ) ); ?>"><?php ett( 'Политика конфиденциальности' ); ?></a>
			<a href="<?php echo get_permalink( get_page_by_path( 'terms-and-condition' ) ); ?>"><?php ett( 'Положения и условия' ); ?></a>
		</div>
		<a href="//www.dmca.com/Protection/Status.aspx?ID=1e1de1e0-35df-49e5-9517-4b07f8dfcde2"
		   title="DMCA.com Protection Status" rel="dofollow" class="dmca-badge">
			<img width="100" height="20"
				 src="https://images.dmca.com/Badges/dmca-badge-w100-5x1-08.png?ID=1e1de1e0-35df-49e5-9517-4b07f8dfcde2"
				 alt="DMCA.com Protection Status"
				 title="DMCA.com Protection Status" />
		</a>
		<time datetime="<?php echo date( 'Y' ); ?>"><?php echo date( 'Y' ); ?></time>
	</div>
</footer>
<!-- FOOTER -->
