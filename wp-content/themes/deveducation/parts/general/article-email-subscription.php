<!-- EMAIL SUBSCRIPTION -->
<?php 
$current_language = pll_current_language();
$subscription = get_acf_options('subscription-'.$current_language, true);


if ( $subscription && $subscription->visible->value ) { ?>
<div class="mail-subscription">
    <div class="subscription-title"><?php echo $subscription->title->value; ?></div>
    <ul>
        <?php 
        if (!empty($subscription->list->item)) {
            foreach ($subscription->list->item as $item) { ?>
            <li><?php echo $item; ?></li>
        <?php } 
        }?>
    </ul>
    <form action="https://cp.unisender.com/ru/subscribe?hash=6st81br1a7cpr3ncb83awg5kckn3a5w31iximhi3m361fhc3w31qo" method="POST">
        <input type="email" placeholder="Email" name="email" required>
        <input type="hidden" name="charset" value="UTF-8">
        <input type="hidden" name="default_list_id" value="17276885">
        <input type="hidden" name="overwrite" value="2">
        <input type="hidden" name="is_v5" value="1">
        <button><?php ett( 'Подписаться' ); ?></button>
    </form>
</div>
<?php 
} ?>
<!-- EMAIL SUBSCRIPTION -->