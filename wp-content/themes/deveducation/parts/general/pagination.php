<?php 
/**
 * Pagination for archive pages
 */
$nav = get_the_posts_pagination( array( 'screen_reader_text' => ' ' ) );
?>
<div class="pagination-links">
    <?php 
    echo str_replace('<h2 class="screen-reader-text"> </h2>', '',$nav);
    echo pagination_links_all();
    ?>
</div>
