<?php get_header( null, array( 'transparent' => true ) ); ?>
    <!-- PAGE 404 -->
    <main>
        <div class="container">
            <div class="title">404</div>
            <div class="subtitle"><?php ett( 'Такой страницы нет, либо мы еще работаем над ней.' ); ?></div>
            <a href="/" class="link-home">
                <button class="btn-color btn" data-callback="onSubmit"><?php ett( 'На главную' ); ?></button>
            </a>
        </div>
    </main>
    <!-- PAGE 404 -->
<?php get_footer(); ?>