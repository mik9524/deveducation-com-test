<?php

require 'include/theme-loader.php';
require 'include/theme-functions.php';
require 'include/polylang-strings.php';
require 'include/polylang-slug.php';
require 'include/disable-default.php';

define( 'ASSETS_URI', get_stylesheet_directory_uri() . '/assets' );

$theme = new DevEducationTheme();
$theme->run();