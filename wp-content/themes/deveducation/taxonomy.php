<?php get_header(); ?>
    <!-- TAXONOMY -->
    <main>
        <div class="container">
			<?php $current = get_queried_object()->term_id; ?>
			<?php echo print_breadcrumbs(); ?>
			<?php $exists = get_glossary_exists_letters( $current ); ?>
            <div class="glossary">
                <h1><?php ett( 'Глоссарий' ); ?></h1>
                <div class="glossary-desc">
                    В глоссарии объясняется множество терминов, которые вы должны знать для успешного управления
                    проектами. Данный глоссарий включает в себя более
                    50 терминов, и поможет вам быстро найти нужный термин и понять его значение.
                </div>
                <div class="glossary-categories">
					<?php foreach ( get_terms( 'glossary_cat', array( 'hide_empty' => false ) ) as $term ): ?>
                        <a href="<?php echo get_term_link( $term ); ?>"
                           class="glossary-cat<?php echo $term->term_id == $current ? ' active' : ''; ?>"><?php echo $term->name; ?></a>
					<?php endforeach; ?>
                </div>
                <div class="glossary-alphabet">
                    <div class="alphabet-left"></div>
                    <div class="alphabet-letters">
						<?php foreach ( get_cyrillic_alphabet() as $letter ): ?>
                            <div class="glossary-letter<?php echo ! in_array( $letter, $exists ) ? ' disabled' : ''; ?>"><?php echo $letter; ?></div>
						<?php endforeach; ?>
                    </div>
                    <div class="alphabet-right"></div>
                </div>
                <div class="glossary-vocabulary">
                    <div class="glossary-words">
						<?php foreach ( get_glossary( $current ) as $key => $word ): ?>
							<?php if ( $key == 0 ): ?>
								<?php $preview = $word->ID; ?>
							<?php endif; ?>
                            <a class="glossary-word"
                               href="<?php echo $word->link; ?>"
                               data-id="<?php echo $word->ID; ?>"><?php echo $word->title; ?></a>
						<?php endforeach; ?>
                    </div>
                    <div class="glossary-content">
						<?php if ( isset( $preview ) ): ?>
							<?php $preview = get_glossary_word( $preview ); ?>
						<?php endif; ?>
                        <div class="word-title"><?php echo $preview ? $preview->title : ''; ?></div>
                        <div class="word-content"><?php echo $preview ? $preview->content : ''; ?></div>
                    </div>
                </div>
            </div>
        </div>
		<?php get_template_part( 'parts/general/glossary-join-form' ); ?>
    </main>
    <!-- TAXONOMY -->
<?php get_footer(); ?>