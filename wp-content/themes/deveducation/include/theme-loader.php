<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Don't access directly
};

require_once( 'theme-actions.php' );
require_once( 'theme-filters.php' );
require_once( 'theme-ajax.php' );

final class DevEducationTheme {
	use ThemeActions;
	use ThemeFilters;
	use ThemeAjax;

	public function __construct() {
	}

	public function run() {
		$this->addActions();
		$this->addFilters();
		$this->addAjaxActions( array(
			'load_articles'     => 'loadArticles',
			'join_us'           => 'joinUs',
			'load_courses'      => 'loadCourses',
			'auth'              => 'auth',
			'check_certificate' => 'checkCertificate',
			'sign_course'       => 'signCourse',
			'get_word'          => 'getWord',
			'load_webinars'     => 'loadWebinars',
			'ask_question'      => 'askQuestion',
			'grant_chance'      => 'grantChance'
		) );
		$this->addThemeSupport( array(
			'menus'           => array(),
			'custom-logo'     => array(),
			'title-tag'       => array(),
			'post-thumbnails' => array( 'events', 'blog', 'reviews' ),
			'editor-styles'   => array(),
			'html5'           => array( 'style', 'script' )
		) );
	}

	private function addActions() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'pre_get_posts', array( $this, 'preGetPosts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'registerScripts' ) );
		add_action( 'after_setup_theme', array( $this, 'afterSetupTheme' ) );
		add_action( 'admin_menu', array( $this, 'adminMenu' ) );
		add_action( 'admin_print_scripts', array( $this, 'adminPrintScripts' ) );
		add_action( 'after_switch_theme', array( $this, 'afterSwitchTheme' ) );
		add_action( 'robots_txt', array( $this, 'robotsTxt' ), - 1 );
//		add_action( 'wp_head', array( $this, 'wpHead' ) );
		add_action( 'wp_footer', array( $this, 'wpFooter' ) );
		add_action( 'do_feed', array( $this, 'disableFeed' ), 1 );
		add_action( 'do_feed_rdf', array( $this, 'disableFeed' ), 1 );
		add_action( 'do_feed_rss', array( $this, 'disableFeed' ), 1 );
		add_action( 'do_feed_rss2', array( $this, 'disableFeed' ), 1 );
		add_action( 'do_feed_atom', array( $this, 'disableFeed' ), 1 );
		add_action( 'do_feed_rss2_comments', array( $this, 'disableFeed' ), - 1 );
		add_action( 'do_feed_atom_comments', array( $this, 'disableFeed' ), - 1 );

		if ( ! is_site_subdomain() ) { // only for origin
			add_action( 'post_submitbox_misc_actions', array( $this, 'miscActions' ) );
			add_action( 'save_post_events', array( $this, 'savePost' ), 10, 3 );
			add_action( 'save_post_blog', array( $this, 'savePost' ), 10, 3 );
			add_action( 'save_post_webinars', array( $this, 'savePostWebinars' ), 10, 3 );
			add_action( 'saved_glossary_cat', array( $this, 'savedGlossaryPost' ), 10, 3 );

			if ( class_exists( 'News_Feed_Catcher' ) ) {
				add_action( 'news_feed_catcher_on_added', array( $this, 'newsFeedCatcherOnAdded' ), 10, 2 );
			}
		}

		if ( class_exists( 'ACF' ) ) {
			add_action( 'acf/init', array( $this, 'acfInit' ) );
		}
	}

	private function addFilters() {
		add_filter( 'upload_mimes', array( $this, 'uploadMemesFilter' ) );
		add_filter( 'the_content', array( $this, 'theContent' ), 20 );
//		add_filter( 'wpseo_json_ld_output', '__return_false' );
		add_filter( 'disable_wpseo_json_ld_search', '__return_true' );
		add_filter( 'autoptimize_filter_imgopt_lazyload_cssoutput', '__return_empty_string' );
		add_filter( 'script_loader_tag', array( $this, 'scriptLoaderTag' ), 10, 2 );
		add_filter( 'wpseo_schema_graph_pieces', array( $this, 'removeFromYoastSchema' ), 11, 2 );
		add_filter( 'wpseo_schema_webpage', array( $this, 'webpageYoastSchema' ) );
		add_filter( 'wpseo_canonical', array( $this, 'yoastCanonical' ), 10, 1 );
		add_filter( 'template_include', array( $this, 'coursesOldPages' ) );
		add_filter( 'user_contactmethods', array( $this, 'changeUserContactMethods' ), 5, 1 );
		add_filter( 'wpseo_schema_graph_pieces', array( $this, 'remove_pieces_from_schema' ), 11, 2 );
		add_filter( 'wpseo_schema_article', array( $this, 'yoast_schema_article' ), 10, 2 );
		add_filter( 'wpseo_schema_person', array( $this, 'yoast_schema_person' ), 10, 2 );

		if ( function_exists( 'pll_e' ) ) {
			add_filter( 'pll_rel_hreflang_attributes', array( $this, 'pllHreflang' ) );
			add_filter( 'pll_get_post_types', array( $this, 'pllGetPostTypes' ), 10, 2 );

			if ( is_author() ) {
				add_filter( 'pll_hide_archive_translation_url', '__return_true' );
			}
		}
		if ( ! is_site_subdomain() ) { // origin
			if ( class_exists( 'ACF' ) ) {
				add_filter( 'acf/prepare_field/name=site', array( $this, 'acfPrepareFieldSite' ) );
			}

			add_filter( 'post_type_link', array( $this, 'postTypeLink' ), 1, 2 );
		} else { // subdomain
			add_filter( 'display_post_states', array( $this, 'displayPostStates' ), 10, 2 );
			add_filter( 'post_row_actions', array( $this, 'postRowActions' ), 10, 2 );
			add_filter( 'page_row_actions', array( $this, 'pageRowActions' ), 10, 2 );
			add_filter( 'get_edit_post_link', array( $this, 'getEditPostLink' ), 10, 3 );
			add_filter( 'wpseo_replacements', array( $this, 'yoastReplacements' ) );

			if ( function_exists( 'pll_e' ) ) {
				add_filter( 'pll_get_post_types', array( $this, 'pllGetPostTypes' ), 10, 2 );
			}

		}

		add_filter( 'rocket_lazyload_threshold', array( $this, 'rocketLazyloadThreshold' ) );

		if ( is_offline_school() ) {
			add_filter( 'post_type_link', array( $this, 'postTypeLinkCourses' ), 1, 2 );
		}
	}

	private function addThemeSupport( $features = array() ) {
		foreach ( $features as $feature => $options ) {
			add_theme_support( $feature, $options );
		}
	}

	private function addAjaxActions( $actions ) {
		foreach ( $actions as $action => $callback ) {
			add_action( "wp_ajax_$action", array( $this, $callback ) );
			add_action( "wp_ajax_nopriv_$action", array( $this, $callback ) );
		}
	}
}