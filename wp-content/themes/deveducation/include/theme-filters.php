<?php

use Yoast\WP\SEO\Generators\Schema\Breadcrumb;

trait ThemeFilters {
	public $common = array( 'templates/template-contacts.php' );

	public function uploadMemesFilter( $mimes ) {
		$mimes['svg'] = 'image/svg'; // if +xml then ALLOW_UNFILTERED_UPLOADS in wp-config

		return $mimes;
	}

	public function displayPostStates( $states, $post ) {
		if ( get_post_meta( $post->ID, 'origin', true ) ) {
			$states[] = 'Общая запись';
		} elseif ( in_array( get_page_template_slug( $post->ID ), $this->common ) && ! is_offline_school() ) {
			$states[] = 'Общая страница';
		}

		return $states;
	}

	public function postRowActions( $actions, $post ) {
		return get_post_meta( $post->ID, 'origin', true ) ? array( 'general' => '<span style="color: #000;">Запись автоматически подтягивается с основного домена</span>' ) : $actions;
	}

	public function pageRowActions( $actions, $post ) {
		return ( in_array( get_page_template_slug( $post->ID ), $this->common ) && ! is_offline_school() ) ? array( 'general' => '<span style="color: #000;">Данные автоматически подтягиваются с основного домена</span>' ) : $actions;
	}

	public function getEditPostLink( $url, $id, $context ) {
		if ( $meta = get_post_meta( $id, 'origin', true ) ) {
			return $meta['edit_url'];
		} elseif ( in_array( get_page_template_slug( $id ), $this->common ) && ! is_offline_school() ) {
			return '#';
		}

		return $url;
	}

	public function acfPrepareFieldSite( $field ) {
		foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ) {
			$field['choices'][ $site->blog_id ] = $site->domain;
		}

		return $field;
	}

	public function pllGetPostTypes( $types, $is_settings ) {
		$types['reviews']   = 'reviews';
		$types['graduates'] = 'graduates';

		return $types;
	}

	public function theContent( $content ) {
		$content     = $this->makeLowQuality( $content );
		$pattern     = '/<a[^>]*href="((?![A-z:\/]*\.?' . preg_quote( $_SERVER['HTTP_HOST'] ) . '\.?[A-z]*)[^>]*)"[^>]*>([^>]*)<\/a>/s';
		$replacement = '<a href="$1" class="replaced" rel="nofollow" target="_blank">$2</a>';
		$content     = preg_replace( $pattern, $replacement, $content );

		if ( $GLOBALS['post']->post_type == 'blog' ) {
			$additional = '<i>' . tt( 'Содержание' ) . ':</i> <ul>';

			preg_match_all( '/<h2>(.*?)<\/h2>/s', $content, $matches );

			foreach ( $matches[1] as $key => $title ) {
				$id         = 'article-block-' . $key;
				$content    = str_replace( $matches[0][ $key ], '<span id="' . $id . '"></span><h2>' . $title . '</h2>', $content );
				$additional .= '<li><a href="#' . $id . '">' . $title . '</a></li>';
			}

			$additional .= '</ul>';

			$new_content = count( $matches[1] ) ? $additional . $content : $content;

			return filter_content_titles( $new_content );

		} elseif ( $GLOBALS['post']->post_type == 'events' ) {
			return filter_content_titles( $content );
		} else {
			return $content;
		}

	}

	public function scriptLoaderTag( $tag, $handle ) {
		$handles = array(
			'splide.min' => 'defer',
			'main'       => 'defer',
			'gtm'        => 'async'
		);

		if ( isset( $handles[ $handle ] ) ) {
			return str_replace( ' src', ' ' . $handles[ $handle ] . ' src', $tag );
		}

		return $tag;
	}

	public function postTypeLink( $permalink, $post ) {
		if ( $post->post_type == 'glossary' ) {
			$terms = get_the_terms( $post, 'glossary_cat' );

			if ( ! is_wp_error( $terms ) && ! empty( $terms ) && is_object( $terms[0] ) ) {
				$term_slug = array_pop( $terms )->slug;
			} else {
				$term_slug = 'no-faqcat';
			}

			return get_home_url() . '/' . $term_slug . '/' . $post->post_name . '/';
		}

		return $permalink;
	}

	// Change slug for courses offline schools
	public function postTypeLinkCourses( $permalink, $post ) {

		if ( $post->post_type == 'courses' ) {
			$permalink = str_replace( '/' . $post->post_type . '/', '/', $permalink );
		}

		return $permalink;
	}


	public function pllHreflang( $hreflangs ) {
		global $post;

		if ( ! is_archive() && get_post_meta( $post->ID, 'origin', true ) ) {
			return array();
		}

		if ( ! isset( $hreflangs['x-default'] ) && isset( $hreflangs['ru'] ) ) {
			$hreflangs['x-default'] = $hreflangs['ru'];
		}

		return $hreflangs;
	}

	public function yoastCanonical( $canonical ) {
		if ( is_author() ) {
			$canonical = str_replace( get_site_url(), get_site_url( get_main_site_id() ), $canonical );
		}

		return is_paged() ? preg_replace( '/\/page\/\d+\//s', '/', $canonical ) : $canonical;
	}

	public function removeFromYoastSchema( $pieces, $context ): array {
		return array_filter( $pieces, function ( $piece ) {
			return ! $piece instanceof Breadcrumb;
		} );
	}

	public function webpageYoastSchema( $data ) {
		unset( $data['breadcrumb'] );

		return $data;
	}

	public function yoastReplacements( $replacements ) {

		if ( isset ( $replacements['%%sitename%%'] ) ) {
			$replacements['%%sitename%%'] .= ' | DevEducation';
		}

		return $replacements;
	}

	public function coursesOldPages( $template ) {
		global $post;

		$excluded = array( 'python', 'js', 'fotoshop', 'php', 'react', 'veb-dizajn', 'kompyuternye-kursy' );

		if ( is_site_subdomain() && ! is_offline_school() && in_array( $post->post_name, $excluded ) ) {
			if ( $new_template = locate_template( array( 'page.php' ) ) ) {
				$template = $new_template;
			}
		}

		return $template;
	}

	public function changeUserContactMethods( $contact_methods ) {
		$contact_methods['user_phone'] = __( 'Телефон', 'deveducation' );
		$contact_methods['user_photo'] = __( 'Ссылка на аватар', 'deveducation' );

		return $contact_methods;
	}

	public function remove_pieces_from_schema( $pieces, $context ) {
		return array_filter( $pieces, function ( $piece ) {
			if ( $piece instanceof \Yoast\WP\SEO\Generators\Schema\Person ||
			     $piece instanceof \Yoast\WP\SEO\Generators\Schema\Article ||
			     $piece instanceof \Yoast\WP\SEO\Generators\Schema\Author ) {
				return true;
			} else {
				return false;
			}
		} );
	}

	public function yoast_schema_article( $graph_piece, $context ) {
		$use_default = false;
		if ( has_post_thumbnail() ) {
			$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
			if ( empty( $image_src[1] ) || 1199 > $image_src[1] ) {
				$use_default = true;
			}
		} else {
			$use_default = true;
		}

		if ( $use_default ) {
			$graph_piece['image'] = $image_src[0];
		}

		$graph_piece['url'] = $context->canonical;

		$graph_piece['mainEntityOfPage']['@type'] = 'WebPage';

		return $graph_piece;
	}

	public function yoast_schema_person( $graph_piece, $context ) {
		$graph_piece['url']                       = $context->canonical;
		$graph_piece['mainEntityOfPage']['@type'] = 'WebPage';

		return $graph_piece;
	}

	public function rocketLazyloadThreshold( $threshold ) {
		return 100;
	}

	private function makeLowQuality( $content ) {
		preg_match_all( '/<img[^>]+>/', $content, $images );

		foreach ( $images[0] as $image ) {
			preg_match( '/src="(.*?)"/', $image, $path );

			$link = get_site_url() . wp_make_link_relative( $path[1] );
			$attr = array( 'alt' => get_the_title() );

			if ( $attachment_id = attachment_url_to_postid( $link ) ) {
				$content = str_replace( $image, wp_get_attachment_image( $attachment_id, 'medium', false, $attr ), $content );
			}
		}

		return $content;
	}
}
