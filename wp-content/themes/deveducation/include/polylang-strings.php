<?php

if ( function_exists( 'pll_register_string' ) ) {
	pll_register_string( 'City', 'Санкт-Петербург', 'DevEducationTheme' );
	pll_register_string( 'City', 'Днепр', 'DevEducationTheme' );
	pll_register_string( 'City', 'Баку', 'DevEducationTheme' );
	pll_register_string( 'City', 'Казань', 'DevEducationTheme' );
	pll_register_string( 'City', 'Киев', 'DevEducationTheme' );
	pll_register_string( 'City', 'Харьков', 'DevEducationTheme' );
	pll_register_string( 'City', 'Казань', 'DevEducationTheme' );
	pll_register_string( 'City', 'Екатеринбург', 'DevEducationTheme' );
	pll_register_string( 'City', 'Тель-Авив', 'DevEducationTheme' );
	pll_register_string( 'City', 'Чернигов', 'DevEducationTheme' );
	pll_register_string( 'City', 'Ивано-Франковск', 'DevEducationTheme' );
	pll_register_string( 'City', 'Херсон', 'DevEducationTheme' );
	pll_register_string( 'City', 'Хмельницкий', 'DevEducationTheme' );
	pll_register_string( 'City', 'Кропивницкий', 'DevEducationTheme' );
	pll_register_string( 'City', 'Луцк', 'DevEducationTheme' );
	pll_register_string( 'City', 'Николаев', 'DevEducationTheme' );
	pll_register_string( 'City', 'Одесса', 'DevEducationTheme' );
	pll_register_string( 'City', 'Полтава', 'DevEducationTheme' );
	pll_register_string( 'City', 'Ровно', 'DevEducationTheme' );
	pll_register_string( 'City', 'Ужгород', 'DevEducationTheme' );
	pll_register_string( 'City', 'Винница', 'DevEducationTheme' );
	pll_register_string( 'City', 'Запорожье', 'DevEducationTheme' );
	pll_register_string( 'City', 'Житомир', 'DevEducationTheme' );
	pll_register_string( 'City', 'Симферополь', 'DevEducationTheme' );
	pll_register_string( 'City', 'Черновцы', 'DevEducationTheme' );
	pll_register_string( 'City', 'Львов', 'DevEducationTheme' );
	pll_register_string( 'City', 'Сумы', 'DevEducationTheme' );
	pll_register_string( 'City', 'Черкассы', 'DevEducationTheme' );

	pll_register_string( 'College', 'Колледж скоро откроется', 'DevEducationTheme' );
	pll_register_string( 'Main office', 'Главный офис', 'DevEducationTheme' );
	pll_register_string( 'Privacy policy', 'Политика конфиденциальности', 'DevEducationTheme' );
	pll_register_string( 'Fill the form', 'Заполни форму и стань одним из успешных выпускников DevEducation!', 'DevEducationTheme' );
	pll_register_string( 'Full name', 'ФИО', 'DevEducationTheme' );
	pll_register_string( 'Phone', 'Телефон', 'DevEducationTheme' );
	pll_register_string( 'Send', 'Отправить', 'DevEducationTheme' );
	pll_register_string( 'Login form', 'DevEducation в партнерстве с RemoteMode открывает корпоративный университет!', 'DevEducationTheme' );
	pll_register_string( 'Employee Login', 'Вход для сотрудников', 'DevEducationTheme' );
	pll_register_string( 'Join us', 'Присоединяйся к DevEducation — стань востребованным специалистом и построй карьеру в IT!', 'DevEducationTheme' );
	pll_register_string( 'Join button', 'Присоединиться', 'DevEducationTheme' );
	pll_register_string( 'Left questions?', 'Остались вопросы? Задайте их нам.', 'DevEducationTheme' );
	pll_register_string( 'Ask question', 'Задать вопрос', 'DevEducationTheme' );
	pll_register_string( 'Change your life', 'Если у вас есть желание изменить свою жизнь в лучшую сторону.', 'DevEducationTheme' );
	pll_register_string( 'Learn new', 'Если вы хотите развиваться и познавать новое.', 'DevEducationTheme' );
	pll_register_string( 'New opportunities', 'Если вам необходимо ощущение покорения новых возможностей.', 'DevEducationTheme' );
	pll_register_string( 'Load more', 'Загрузить ещё', 'DevEducationTheme' );
	pll_register_string( 'Closed Subscription', 'Закрытая IT-рассылка', 'DevEducationTheme' );
	pll_register_string( 'Author materials', 'Авторские учебные материалы', 'DevEducationTheme' );
	pll_register_string( 'Free Visit', 'Бесплатное посещение IT-мероприятий', 'DevEducationTheme' );
	pll_register_string( 'Main events', 'Разбор главных событий отрасли', 'DevEducationTheme' );
	pll_register_string( 'Subscribe', 'Подписаться', 'DevEducationTheme' );
	pll_register_string( 'Our contacts', 'Наши контакты', 'DevEducationTheme' );
	pll_register_string( 'More details', 'Подробнее', 'DevEducationTheme' );
	pll_register_string( 'Our team', 'Наша команда', 'DevEducationTheme' );
	pll_register_string( 'Shortly', 'Содержание', 'DevEducationTheme' );
	pll_register_string( 'Author', 'Автор', 'DevEducationTheme' );
	pll_register_string( 'Want to be a developer?', 'Хочешь стать разработчиком?', 'DevEducationTheme' );
	pll_register_string( 'Get a grant', 'Получи грант на обучение и освой профессию мечты.', 'DevEducationTheme' );
	pll_register_string( 'Submit application ', 'Оставить заявку', 'DevEducationTheme' );
	pll_register_string( 'More articles', 'Больше статей', 'DevEducationTheme' );
	pll_register_string( 'City', 'Город', 'DevEducationTheme' );
	pll_register_string( 'Course starts', 'Начало занятий', 'DevEducationTheme' );
	pll_register_string( 'Sign up', 'Записаться', 'DevEducationTheme' );
	pll_register_string( 'Sign up for course', 'Записаться на курс', 'DevEducationTheme' );
	pll_register_string( 'Fill the the form and get the grant', 'Заполни форму и получи грант на обучение в ближайшем потоке курса!', 'DevEducationTheme' );
	pll_register_string( 'Choose a city', 'Выберите город', 'DevEducationTheme' );
	pll_register_string( 'Choose a course', 'Выберите курс', 'DevEducationTheme' );
	pll_register_string( 'Students reviews', 'Отзывы студентов', 'DevEducationTheme' );
	pll_register_string( 'No nearest courses', 'В ближайшее время курсов нет', 'DevEducationTheme' );
	pll_register_string( 'Have questions', 'Появились вопросы? Заполни форму обратной связи, мы поможем!', 'DevEducationTheme' );
	pll_register_string( 'Read', 'Читать', 'DevEducationTheme' );
	pll_register_string( 'More news', 'Больше новостей', 'DevEducationTheme' );
	pll_register_string( 'Answers for popular questions', 'ответы на популярные вопросы об обучении в DevEducation', 'DevEducationTheme' );
	pll_register_string( 'Join to DevEducation', 'Присоединиться к DevEducation', 'DevEducationTheme' );
	pll_register_string( 'Nearest courses', 'Ближайшие курсы', 'DevEducationTheme' );
	pll_register_string( 'Become a sought-after developer', 'Станьте востребованным разработчиком с DevEducation!', 'DevEducationTheme' );
	pll_register_string( 'This profession for', 'Эта профессия для тех, кто', 'DevEducationTheme' );
	pll_register_string( 'How to create a career?', 'Как начать карьеру в it?', 'DevEducationTheme' );
	pll_register_string( 'Course Program', 'Программа курса', 'DevEducationTheme' );
	pll_register_string( 'Online check', 'Онлайн проверка сертификата', 'DevEducationTheme' );
	pll_register_string( 'Check', 'Проверить', 'DevEducationTheme' );
	pll_register_string( 'Last name', 'Фамилия', 'DevEducationTheme' );
	pll_register_string( 'Sertificate number', 'Номер сертификата через пробел', 'DevEducationTheme' );
	pll_register_string( 'FAQ', 'Самые частые вопросы', 'DevEducationTheme' );
	pll_register_string( 'Graduates says', 'Что о нас говорят выпускники?', 'DevEducationTheme' );
	pll_register_string( 'Fill the form and learn more', 'Заполните форму и узнайте больше о том, как построить карьеру в IT с DevEducation!', 'DevEducationTheme' );
	pll_register_string( 'Get a profession', 'Получить профессию', 'DevEducationTheme' );
	pll_register_string( 'Biography', 'Биография', 'DevEducationTheme' );
	pll_register_string( 'Resume', 'Как будет выглядеть ваше резюме после обучения?', 'DevEducationTheme' );
	pll_register_string( 'Our partners', 'Наши партнёры', 'DevEducationTheme' );
	pll_register_string( 'News', 'Новости', 'DevEducationTheme' );
	pll_register_string( 'Memories', 'Воспоминания', 'DevEducationTheme' );
	pll_register_string( 'Account', 'Кабинет', 'DevEducationTheme' );
	pll_register_string( 'My account', 'Личный кабинет', 'DevEducationTheme' );
	pll_register_string( 'Sign in', 'Войти', 'DevEducationTheme' );
	pll_register_string( 'Password', 'Пароль', 'DevEducationTheme' );
	pll_register_string( 'Terms and conditions', 'Положения и условия', 'DevEducationTheme' );
	pll_register_string( 'Teacher', 'Преподаватель', 'DevEducationTheme' );
	pll_register_string( '404', 'Такой страницы нет, либо мы еще работаем над ней.', 'DevEducationTheme' );
	pll_register_string( 'Go to the main page', 'На главную', 'DevEducationTheme' );
	pll_register_string( 'Certificate valid', 'Сертификат действителен', 'DevEducationTheme' );
	pll_register_string( 'Certificate does not exists', 'Сертификат не найден', 'DevEducationTheme' );
//	pll_register_string( 'Glossary', 'Глоссарий', 'DevEducationTheme' );
	pll_register_string( 'Personal data', 'Заполняя форму регистрации, я принимаю условия обработки персональных данных', 'DevEducationTheme' );
//	pll_register_string( 'Webinars', 'Вебинары', 'DevEducationTheme' );
	pll_register_string( 'All', 'Все', 'DevEducationTheme' );
//	pll_register_string( 'More webinars', 'Больше вебинаров', 'DevEducationTheme' );
	pll_register_string( 'Recommendations', 'Рекомендованные', 'DevEducationTheme' );
	pll_register_string( 'While you can choose a convenient branch for you?', 'Пока вы можете выбрать удобный вам филиал', 'DevEducationTheme' );

//	pll_register_string( 'More info and content', 'Больше интересного контента в', 'DevEducationTheme' );
//	pll_register_string( 'in socials', 'наших соц сетях', 'DevEducationTheme' );
	pll_register_string( 'Get the grant', 'Получить грант', 'DevEducationTheme' );
	pll_register_string( 'Get access to private IT subscription', 'Получите доступ к закрытой IT-рассылке', 'DevEducationTheme' );
	pll_register_string( 'Courses', 'Курсы', 'DevEducationTheme' );
	pll_register_string( 'Ask your question', 'Задайте ваш вопрос', 'DevEducationTheme' );
	pll_register_string( 'Success', 'Успешно отправлено', 'DevEducationTheme' );

	// header -> menu
	pll_register_string( 'Our branches', 'Наши филиалы', 'DevEducationTheme' );

	// first-screen
	pll_register_string( 'Select a branch in your city', 'Выберите филиал в вашем городе', 'DevEducationTheme' );
	pll_register_string( 'Branches', 'Филиалы', 'DevEducationTheme' );

	//grant-chance
	pll_register_string( 'Fill out the form and find out how to get a study grant', 'Заполни форму и узнай, как получить грант на обучение', 'DevEducationTheme' );
	pll_register_string( 'By filling out the registration form, I accept ', 'Заполняя форму регистрации, я принимаю', 'DevEducationTheme' );
	pll_register_string( 'conditions for processing personal data', 'условия обработки персональных данных', 'DevEducationTheme' );

	//career-steps
	pll_register_string( 'Step', 'Шаг', 'DevEducationTheme' );


	// Graduates
	pll_register_string( 'graduates are already working in IT', 'выпускников уже работают в IT', 'DevEducationTheme' );
	pll_register_string( 'Work with internationals company', 'Сотрудничаем с международными компаниями', 'DevEducationTheme' );

	//Register form front page
	pll_register_string( 'Fill out the form and find out how to get a study grant', 'Заполни форму и узнай, как получить грант на обучение', 'DevEducationTheme' );

	//mountains
	pll_register_string( 'mo.', 'мес.', 'DevEducationTheme' );

	// bottom-screen
	pll_register_string( 'Not sure which profession is right for you?', 'Не уверен какая профессия подходит именно тебе?', 'DevEducationTheme' );
	pll_register_string( 'We will help with the choice', 'Мы поможем с выбором', 'DevEducationTheme' );
	pll_register_string( 'opening two new schools', 'открытие двух новых школ', 'DevEducationTheme' );

	// courses-page
	pll_register_string( 'Enrollment for the next courses completed', 'Набор на ближайшие курсы завершён', 'DevEducationTheme' );
	pll_register_string( 'All courses in', 'Все курсы в городе', 'DevEducationTheme' );
	pll_register_string( 'All courses', 'Все курсы', 'DevEducationTheme' );
	pll_register_string( 'Start of classes', 'Старт занятий', 'DevEducationTheme' );
	pll_register_string( 'Fill out the form and get a grant for training in the next course of the course!', 'Заполни форму и получи грант на обучение в ближайшем потоке курса!', 'DevEducationTheme' );

	pll_register_string( 'Vacancies left', 'Осталось мест', 'DevEducationTheme' );
	pll_register_string( 'All pages', 'All pages', 'DevEducationTheme' );	
	pll_register_string( 'Attention string', 'Мы вынуждены приостановить обучающую деятельность на территории России с 10 марта 2022 года', 'DevEducationTheme' );	
}