<?php

$pages = array(
	'events'    => array( 'News', 'templates/template-events.php' ),
	'blog'      => array( 'Blog', 'templates/template-blog.php' ),
	'courses'   => array( 'Courses', 'templates/template-courses.php' ),
	'contacts'  => array( 'Contacts', 'templates/template-contacts.php' ),
	'graduates' => array( 'Graduates', 'templates/template-graduates.php' ),
	'about'     => array( 'About us', 'templates/template-about.php' ),
	'faq'       => array( 'FAQ', 'templates/template-faq.php' )
);

foreach ( $pages as $slug => $page ) {
	if ( is_site_subdomain() && $slug == 'faq' ) { // faq only on origin
		continue;
	}

	if ( ! get_page_by_title( $page[0] ) ) {
		wp_insert_post( array(
			'post_title'    => $page[0],
			'post_status'   => 'publish',
			'post_type'     => 'page',
			'post_name'     => $slug,
			'page_template' => $page[1]
		) );
	}
}