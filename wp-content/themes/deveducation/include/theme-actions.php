<?php

trait ThemeActions {
	public function init() {
		require_once( __DIR__ . '/custom-post-types.php' );

		$get_post_type          = get_post_type_object( 'post' );
		$labels                 = $get_post_type->labels;
		$labels->menu_name      = 'Блог';
		$labels->name_admin_bar = 'Блог';

		if ( ! is_admin() ) {
			wp_deregister_script( 'jquery' );
		}
		
		wp_deregister_style( 'wp-block-library' );
		wp_deregister_style( 'wp-block-library-theme' );
		wp_deregister_style( 'wc-block-style' );

//		wp_register_script( 'jquery', ASSETS_URI . '/js/jquery-3.5.1.min.js', false, '3.5.1', true );
//		wp_enqueue_script( 'gtm', ASSETS_URI . '/js/gtm.js', false, '1.0.0', true );

		if ( ! is_site_subdomain() ) {
			$this->glossaryRewriteRules();
		}

		$role = get_role( 'editor' );

		$role->add_cap( 'list_users' );
		$role->add_cap( 'edit_users' );
		$role->add_cap( 'delete_users' );
		$role->add_cap( 'create_users' );
		$role->add_cap( 'add_users' );
		$role->add_cap( 'remove_users' );
		$role->add_cap( 'manage_network_users' );
		$role->add_cap( 'manage_options' ); // for plugins
		$role->add_cap( 'wpseo_manage_options' ); // for Yoast
		$role->add_cap( 'install_plugins' );
		$role->add_cap( 'update_themes' );
		$role->add_cap( 'edit_themes' );
		$role->add_cap( 'customize' );
		$role->add_cap( 'edit_theme_options' );
		$role->add_cap( 'activate_plugins' );

		remove_action( 'wp_head', 'rest_output_link_wp_head' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	}

	public function localizeGraduates(): void {
		global $post;

		$graduates     = get_acf_repeater( $post->ID, 'graduates' );
		$all_graduates = array();

		if ( $graduates ) {
			foreach ( $graduates as $key => $graduate ) {
				$image                    = wp_get_attachment_url( $graduate->image );
				$graduates[ $key ]->image = $image;
			}

			if ( count( $all_graduates ) < 39 ) {
				do {
					$all_graduates = array_merge( $all_graduates, $graduates );
				} while ( count( $all_graduates ) < 39 );
			}

			wp_localize_script( 'home-origin', 'graduatesList', $all_graduates );
		}

	}

	public function registerScripts() {
		$partOfGeneral = is_site_subdomain() ? '-sub' : '-origin';
		$dependency    = array(
			'origin'          => array(
				'js'  => array( 'splide.min.js', 'home-origin.js', 'main-origin.js' ),
				'css' => 'home-origin.css'
			),
			'subdomain'       => array(
				'js'  => array( 'splide.min.js', 'main-sub.js' ),
				'css' => 'home-subdomain.css'
			),
			'courses'         => array(
				'js'  => array( 'splide.min.js', 'main' . $partOfGeneral . '.js' ),
				'css' => 'courses' . $partOfGeneral . '.css'
			),
			'training-center' => array(
				'js'  => array( 'main' . $partOfGeneral . '.js' ),
				'css' => 'training-center.css'
			),
			'glossary'        => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'glossary.css' ),
			'webinars'        => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'webinars.css' ),
			'graduates'       => array(
				'js'  => array( 'splide.min.js', 'main' . $partOfGeneral . '.js' ),
				'css' => 'graduates.css'
			),
			'events'          => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'articles.css' ),
			'blog'            => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'articles.css' ),
			'about'           => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'about.css' ),
			'author'          => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'author.css' ),
			'contacts'        => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'contacts.css' ),
			'inspirer'        => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'inspirer.css' ),
			'faq'             => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => 'faq.css' ),
			'404'             => array( 'js' => array( 'main' . $partOfGeneral . '.js' ), 'css' => '404.css' ),
			'general'         => array(
				'js'  => array( 'main' . $partOfGeneral . '.js' ),
				'css' => 'general' . $partOfGeneral . '.css'
			)
		);

		$type = '';
		if ( is_front_page() ) {
			$type = is_site_subdomain() ? 'subdomain' : 'origin';
		} elseif ( is_tax( 'glossary_cat' ) ) {
			$type = 'glossary';
		} elseif ( is_single() ) {
			$type = get_post_type();
		} elseif ( is_page() ) {
			$type = get_queried_object()->post_name;
			if ( ! get_page_template_slug( get_queried_object_id() ) ) {
				$type = 'general';
			}
		} elseif ( is_archive() && ! is_author() ) {
			$type = get_queried_object()->name;
		} elseif ( is_author() ) {
			$type = 'author';
		} 
		
		if ( is_page_template('templates/template-course.php') ) {
			$type = 'courses';
		}

		wp_enqueue_style( 'raleway-font', ASSETS_URI . '/css/raleway.css' );
		wp_enqueue_style( 'general-dependency', ASSETS_URI . '/css/' . $dependency['general']['css'], [ 'raleway-font' ] );

		if ( $type ) {
			wp_enqueue_style( $type . '-dependency', ASSETS_URI . '/css/' . $dependency[ $type ]['css'], [ 'general-dependency' ] );
		} else {
			$type = 'general';
		}

		foreach ( $dependency[ $type ]['js'] as $js ) {
			wp_enqueue_script( str_replace( '.js', '', $js ), ASSETS_URI . '/js/' . $js, array(), false, true );
		}

		if ( is_front_page() || is_home() ) {
			$this->localizeGraduates();
		}

//		wp_enqueue_script( 'gtm', 'https://www.googletagmanager.com/gtm.js?id=GTM-WM5BMK2', array(), false, true );
	}

	public function afterSetupTheme() {
		register_nav_menu( 'primary', 'Главное меню' );
		//remove short links
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
		remove_action( 'template_redirect', 'wp_shortlink_header', 11 );
	}

	public function afterSwitchTheme() {
		require_once( __DIR__ . '/custom-pages.php' );
	}

	public function adminMenu() {
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'edit.php' );
	}

	public function acfInit() {
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page( array(
				'page_title' => 'Общие настройки темы',
				'menu_title' => 'Настройки темы',
				'menu_slug'  => 'theme-general-settings',
				'capability' => 'edit_posts',
				'redirect'   => false
			) );
		}
	}

	public function adminPrintScripts() {
		global $pagenow;

		if ( $pagenow == 'nav-menus.php' ) {
			$script = '<script type="text/javascript">';
			$script .= "document.addEventListener('DOMContentLoaded', () => document.querySelector('.drag-instructions').innerHTML += '<p style=\"background: #eaf5ff;padding: 10px; border-radius: 3px;width: 350px;\"><strong>.hidden-header</strong> - для скрытия в шапке<br><strong>.hidden-footer</strong> - для скрытия в подвале</strong></p>')";
			$script .= '</script>';

			echo $script;
		}
	}

	public function miscActions() {

		global $post;

		if ( $post->post_type == 'blog' || $post->post_type == 'events' ) {
			$enabled = get_post_meta( $post->ID, 'sync_subdomain', true );

			echo '<div class="misc-pub-section"><label for="sync"><input name="sync_subdomain" type="checkbox" id="sync" value="1"' . ( $enabled ? ' checked' : '' ) . '>Отображать на поддоменах</label></div>';
		}
	}

	public function savePost( $id, $origin_post, $update ) {
		if ( isset( $_POST['sync_subdomain'] ) ) {
			remove_action( 'save_post_events', array( $this, 'savePost' ) );
			remove_action( 'save_post_blog', array( $this, 'savePost' ) );
			update_post_meta( $id, 'sync_subdomain', 1 );

			$permalink = get_permalink( $id );
			$thumbnail = get_the_post_thumbnail_url( $id, 'full' );
			$author    = $origin_post->post_author;
			$edit_url  = home_url() . '/wp-admin/post.php?post=' . $id . '&action=edit';
			$lang      = 'ru'; // default lang
			$title     = get_post_meta( $id, '_yoast_wpseo_title', true );
			$desc      = get_post_meta( $id, '_yoast_wpseo_metadesc', true );

			if ( function_exists( 'pll_get_post_language' ) ) {
				$lang = pll_get_post_language( $id );
			}

			foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $subdomain ) {
				switch_to_blog( $subdomain->blog_id );

				$languages = array( 'ru' );

				if ( function_exists( 'pll_languages_list' ) ) {
					$languages = pll_languages_list();
				}

				if ( ! in_array( $lang, $languages ) ) {
					restore_current_blog();
					continue; // skipping
				}

				$subdomain_post = get_page_by_title( $origin_post->post_title, 'OBJECT', $origin_post->post_type );
				$post_data      = array(
					'post_status'  => $origin_post->post_status,
					'post_type'    => $origin_post->post_type,
					'post_title'   => $origin_post->post_title,
					'post_content' => $origin_post->post_content,
					'post_name'    => $origin_post->post_name,
					'post_date'    => $origin_post->post_date,
					'post_author'  => $author,
				);
				$meta           = array(
					'post_id'   => $id,
					'post_link' => $permalink,
					'image'     => $thumbnail,
					'author'    => $author,
					'edit_url'  => $edit_url
				);

				if ( ! $subdomain_post ) {
					$post_id = wp_insert_post( $post_data );

					add_post_meta( $post_id, 'origin', $meta, true );
					add_post_meta( $post_id, '_yoast_wpseo_canonical', $permalink, true );
					add_post_meta( $post_id, '_yoast_wpseo_title', $title, true );
					add_post_meta( $post_id, '_yoast_wpseo_metadesc', $desc, true );

					if ( function_exists( 'pll_default_language' ) ) {
						pll_set_post_language( $post_id, pll_default_language() );
					}
				} else { // just update if exists
					$post_data['ID'] = $subdomain_post->ID;

					wp_update_post( $post_data );
					update_post_meta( $subdomain_post->ID, 'origin', $meta );
					update_post_meta( $subdomain_post->ID, '_yoast_wpseo_canonical', $permalink );
					update_post_meta( $subdomain_post->ID, '_yoast_wpseo_title', $title );
					update_post_meta( $subdomain_post->ID, '_yoast_wpseo_metadesc', $desc );
				}

				restore_current_blog();
			}
		} else {
			update_post_meta( $id, 'sync_subdomain', 0 );
		}
	}

	public function robotsTxt( $txt ) {
		$sitemap = get_site_url() . '/sitemap_index.xml';
		$lines   = array(
			'User-agent: *',
			'Disallow: /*?*',
			'Disallow: /cgi-bin',
			'Disallow: /wp-*',
			'Allow: */uploads',
			'Allow: */images',
			'Allow: /wp-*/*.css',
			'Allow: /wp-*/*.js',
			'Allow: /wp-*/*.jpg',
			'Allow: /wp-*/*.png',
			'Allow: /wp-*/*.svg',
			'Allow: /wp-*/*.gif',
			'Allow: /wp-*/*.woff2',
			'',
			'Sitemap: ' . $sitemap
		);

		$txt = implode( "\r\n", $lines );

		return $txt;
	}

	public function wpHead() {
		global $post;

		if ( pll_is_translated_post_type( $post->post_type ) ) {
			if ( is_archive() && ! is_author() && ! is_paged() ) {
				echo '<link rel="alternate" href="' . home_url( $post->post_type ) . '/" hreflang="x-default" />' . PHP_EOL;
			} elseif ( ! is_archive() && count( pll_get_post_translations( $post->ID ) ) > 1 ) {
				echo '<link rel="alternate" href="' . get_permalink( pll_get_post( $post->ID, 'ru' ) ) . '" hreflang="x-default" />' . PHP_EOL;
			}
		}
	}

	public function wpFooter() {
		echo '<noscript><embed src="https://www.googletagmanager.com/ns.html?id=GTM-WM5BMK2" height="0" width="0" style="display:none;visibility:hidden"></noscript>';
	}

	public function newsFeedCatcherOnAdded( $id, $data ) {
		update_post_meta( $id, 'sync_subdomain', 1 );

		$current   = get_post( $id );
		$permalink = get_permalink( $id );
		$meta      = array(
			'post_id'   => $id,
			'post_link' => $permalink,
			'image'     => get_the_post_thumbnail_url( $id, 'medium' ),
			'author'    => $current->post_author,
			'edit_url'  => home_url() . '/wp-admin/post.php?post=' . $id . '&action=edit'
		);
		$lang      = 'ru'; // default lang
		$title     = get_post_meta( $current->ID, '_yoast_wpseo_title', true );
		$desc      = get_post_meta( $current->ID, '_yoast_wpseo_metadesc', true );

		if ( function_exists( 'pll_get_post_language' ) ) {
			$lang = pll_get_post_language( $id );
		}

		foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $subdomain ) {
			switch_to_blog( $subdomain->blog_id );

			$languages = array( 'ru' );

			if ( function_exists( 'pll_languages_list' ) ) {
				$languages = pll_languages_list();
			}

			if ( ! in_array( $lang, $languages ) ) {
				restore_current_blog();
				continue; // skipping
			}

			if ( ! get_page_by_title( $current->post_title, 'OBJECT', $current->post_type ) ) {
				$post_data = array(
					'post_status'  => $current->post_status,
					'post_type'    => $current->post_type,
					'post_title'   => $current->post_title,
					'post_content' => $current->post_content,
					'post_name'    => $current->post_name,
					'post_date'    => $current->post_date,
					'post_author'  => $current->post_author,
				);
				$post_id   = wp_insert_post( $post_data );

				add_post_meta( $post_id, 'origin', $meta, true );
				add_post_meta( $post_id, '_yoast_wpseo_canonical', $permalink, true );

				if ( $title ) {
					add_post_meta( $post_id, '_yoast_wpseo_title', $title, true );
				}

				if ( $desc ) {
					add_post_meta( $post_id, '_yoast_wpseo_metadesc', $desc, true );
				}

				if ( function_exists( 'pll_default_language' ) ) {
					pll_set_post_language( $post_id, $lang );
				}
			}

			restore_current_blog();
		}
	}

	public function savedGlossaryPost( $term_id, $taxonomy, $update ) {
		flush_rewrite_rules();
	}

	private function glossaryRewriteRules() {
		$terms = get_terms( array(
			'taxonomy'   => 'glossary_cat',
			'hide_empty' => false,
		) );

		$categories = array_map( function ( $term ) {
			return $term->slug;
		}, $terms );

		if ( count( $categories ) > 0 ) {
//			add_rewrite_rule( 'glossary\/?$', 'index.php?post_type=glossary', 'top' ); // archive url
			add_rewrite_rule( '(?:' . implode( '|', $categories ) . ')\/([^\/]+)\/?$', 'index.php?post_type=glossary&name=$matches[1]', 'top' );
		}
	}

	public function savePostWebinars( $id, $post, $update ) {
		if ( $post->post_status == 'publish' && $post->post_excerpt ) {
			$youtube = wp_remote_get( $post->post_excerpt );

			preg_match( '/<meta itemprop="duration" content="PT(\d+)M(\d+)S">/s', $youtube['body'], $duration );
			preg_match( '/<link itemprop="thumbnailUrl" href="(.*?)"/s', $youtube['body'], $thumbnail );

			update_post_meta( $id, 'duration', sprintf( '%02d:%02d', $duration[1], $duration[2] ) );
			update_post_meta( $id, 'youtube_thumbnail', str_replace( 'maxresdefault', 'mqdefault', $thumbnail[1] ) );
		}
	}

	public function disableFeed() {
		wp_redirect( '/', 301 );
		exit;
	}

	public function preGetPosts( $query ) {
		if ( is_author() && $query->is_main_query() ) {
			$query->set( 'posts_per_page', 8 );
			$query->set( 'post_type', array( 'events', 'blog' ) );
		}

		if ( is_offline_school() ) {



			if ( ! $query->is_main_query() || ! isset( $query->query['page'] ) ) {
				return;
			}

			if ( 2 !== count( $query->query ) && ( 3 !== count( $query->query ) && ! isset( $query->query['lang'] ) ) ) {
				return;
			}
		
			if ( ! empty( $query->query['name'] ) ) {
				$query->set( 'post_type', array( 'post', 'page', 'courses' ) );
			} elseif ( ! empty( $query->query['pagename'] ) && false === strpos( $query->query['pagename'], '/' ) ) {
				$query->set( 'post_type', array( 'post', 'page', 'courses' ) );
				
				$query->set( 'name', $query->query['pagename'] );
			}
		}
	}
}