<?php

remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'wp_generator' );

/* Emoji */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_action( 'wp_footer', function () {
	wp_dequeue_script( 'wp-embed' );
} );

add_filter( 'xmlrpc_enabled', '__return_false' );

add_action( 'pre_ping', function ( $links ) {
	foreach ( $links as $l => $link ) {
		if ( 0 === strpos( $link, get_option( 'home' ) ) ) {
			unset( $links[ $l ] );
		}
	}
} );

add_action( 'init', function () {
	wp_deregister_script( 'heartbeat' );
} );
add_action( 'wp_enqueue_scripts', function () {
	if ( current_user_can( 'update_core' ) ) {
		return;
	}
	wp_deregister_style( 'dashicons' );
} );
add_filter( 'the_generator', '__return_empty_string' ); // remove wp version from scripts & css
add_action( 'do_feed', 'default_disable_feed' );
add_action( 'do_feed_rdf', 'default_disable_feed' );
add_action( 'do_feed_rss', 'default_disable_feed' );
add_action( 'do_feed_rss2', 'default_disable_feed' );
add_action( 'do_feed_atom', 'default_disable_feed' );
add_action( 'do_feed_rss2_comments', 'default_disable_feed' );
add_action( 'do_feed_atom_comments', 'default_disable_feed' );
add_filter( 'wp_lazy_loading_enabled', '__return_false' );
add_action( 'after_setup_theme', function () {
	add_filter( 'embed_oembed_discover', '__return_false' );
	remove_action( 'wp_head', 'rest_output_link_wp_head' );
	remove_action( 'template_redirect', 'rest_output_link_header', 11 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'rest_api_init', 'wp_oembed_register_route' );
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result' );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
} );

function default_disable_feed() {
	wp_die( __( 'No feed available, please visit the <a href="' . esc_url( home_url( '/' ) ) . '">homepage</a>!' ) );
}