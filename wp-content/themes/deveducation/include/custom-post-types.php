<?php

register_taxonomy( 'post_tag', array() ); // disable tags
register_taxonomy( 'category', array() ); // disable category

$taxonomy_default = array(
	'description'        => '',
	'public'             => true,
	'show_in_nav_menus'  => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'show_tagcloud'      => false,
	'show_in_quick_edit' => null,
	'hierarchical'       => true,
	'capabilities'       => array(),
	'meta_box_cb'        => null,
	'show_admin_column'  => true,
	'show_in_rest'       => null,
	'rest_base'          => null
);

$taxonomies = array(
	'glossary_cat' => array(
		'post_type' => array( 'glossary' ),
		'args'      => array(
			'labels'  => array(
				'name'              => 'Категории',
				'singular_name'     => 'Категории',
				'search_items'      => 'Поиск категорий',
				'all_items'         => 'Все категории',
				'view_item '        => 'Посмотреть категорию',
				'parent_item'       => 'Родительская категория',
				'parent_item_colon' => 'Родительская категория:',
				'edit_item'         => 'Ред. категорию',
				'update_item'       => 'Обновить категорию',
				'add_new_item'      => 'Добавить категорию',
				'new_item_name'     => 'Название новой категории',
				'menu_name'         => 'Категории',
			),
			'rewrite' => array(
				'slug'       => 'glossary',
				'with_front' => false,
			)
		)
	),
	'webinars_cat' => array(
		'post_type' => array( 'webinars' ),
		'args'      => array(
			'labels'  => array(
				'name'              => 'Категории',
				'singular_name'     => 'Категории',
				'search_items'      => 'Поиск категорий',
				'all_items'         => 'Все категории',
				'view_item '        => 'Посмотреть категорию',
				'parent_item'       => 'Родительская категория',
				'parent_item_colon' => 'Родительская категория:',
				'edit_item'         => 'Ред. категорию',
				'update_item'       => 'Обновить категорию',
				'add_new_item'      => 'Добавить категорию',
				'new_item_name'     => 'Название новой категории',
				'menu_name'         => 'Категории',
			),
			'rewrite' => false,
			'public'  => false,
		)
	)
);

$default = array(
	'public'             => true,
	'publicly_queryable' => null,
	'show_in_nav_menus'  => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'show_tagcloud'      => true,
	'hierarchical'       => false,
	'supports'           => array(
		'title',
		'editor',
		'author',
		'thumbnail',
		'excerpt',
		'custom-fields',
		'revisions',
		'page-attributes',
	),
	'has_archive'        => true,
	'rewrite'            => true,
	'query_var'          => true
);

$post_types = array(
	'blog'     => array(
		'menu_icon' => 'dashicons-groups',
		'labels'    => array(
			'name'               => 'Блог',
			'singular_name'      => 'Блог',
			'add_new'            => 'Добавить запись',
			'add_new_item'       => 'Добавление новоой записи',
			'edit_item'          => 'Редактирование записи',
			'new_item'           => 'Новая запись',
			'view_item'          => 'Смотреть запись',
			'search_items'       => 'Искать запись',
			'not_found'          => 'Не найдено записей',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Блог',
		),
		'supports'  => array( 'title', 'editor', 'thumbnail', 'author' ),
	),
	'events'   => array(
		'menu_icon' => 'dashicons-welcome-widgets-menus',
		'labels'    => array(
			'name'               => 'Новости',
			'singular_name'      => 'Новость',
			'add_new'            => 'Добавить новость',
			'add_new_item'       => 'Добавление новой новости',
			'edit_item'          => 'Редактирование новости',
			'new_item'           => 'Новая новость',
			'view_item'          => 'Смотреть новость',
			'search_items'       => 'Искать новость',
			'not_found'          => 'Не найдено новостей',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Новости'
		),
		'supports'  => array( 'title', 'editor', 'thumbnail', 'author' )
	),
	'courses'  => array(
		'menu_icon'   => 'dashicons-welcome-learn-more',
		'has_archive' => false,
		'labels'      => array(
			'name'               => 'Курсы',
			'singular_name'      => 'Курс',
			'add_new'            => 'Добавить курс',
			'add_new_item'       => 'Добавление нового курса',
			'edit_item'          => 'Редактирование курса',
			'new_item'           => 'Новый курс',
			'view_item'          => 'Смотреть курс',
			'search_items'       => 'Искать курс',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Курсы'
		),
	),
	'reviews'  => array(
		'menu_icon'   => 'dashicons-businessman',
		'has_archive' => false,
		'public'      => false,
		'labels'      => array(
			'name'               => 'Отзывы',
			'singular_name'      => 'Отзыв',
			'add_new'            => 'Добавить отзыв',
			'add_new_item'       => 'Добавление нового отзыва',
			'edit_item'          => 'Редактирование анкеты отзыва',
			'new_item'           => 'Новый отзыв',
			'view_item'          => 'Смотреть анкету отзыва',
			'search_items'       => 'Искать анкету отзыва',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Отзывы'
		),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
		'hierarchical' => false
	),
	'glossary' => array(
		'menu_icon'   => 'dashicons-text-page',
		'has_archive' => false,
		'public'      => true,
		'labels'      => array(
			'name'               => 'Глоссарий',
			'singular_name'      => 'Глоссарий',
			'add_new'            => 'Добавить термин',
			'add_new_item'       => 'Добавление термина в глоссарий',
			'edit_item'          => 'Редактирование термина',
			'new_item'           => 'Новый термин',
			'view_item'          => 'Посмотреть термин глоссария',
			'search_items'       => 'Искать термин',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Глоссарий'
		),
		'supports'    => array( 'title', 'editor', 'thumbnail' ),
		'rewrite'     => false,
		'taxonomies'  => array( 'glossary_cat' )
	),
	'webinars' => array(
		'menu_icon'   => 'dashicons-video-alt3',
		'has_archive' => false,
		'public'      => true,
		'labels'      => array(
			'name'               => 'Вебинары',
			'singular_name'      => 'Вебинар',
			'add_new'            => 'Добавить вебинар',
			'add_new_item'       => 'Добавление вебинара',
			'edit_item'          => 'Редактирование вебинара',
			'new_item'           => 'Новый вебинар',
			'view_item'          => 'Посмотреть вебинар',
			'search_items'       => 'Искать вебинар',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Вебинары'
		),
		'supports'    => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'rewrite'     => true,
		'taxonomies'  => array( 'webinars_cat' )
	)
);

foreach ( $taxonomies as $taxonomy => $settings ) {
	if ( is_site_subdomain() && in_array( $taxonomy, array( 'glossary_cat', 'webinars_cat' ) ) ) {
		continue;
	}

	register_taxonomy( $taxonomy, $settings['post_type'], array_merge( $taxonomy_default, $settings['args'] ) );
}

foreach ( $post_types as $post_type => $args ) {
	if ( ! is_site_subdomain() && in_array( $post_type, array( 'reviews' )) ) { // exclude from origin
		continue;
	} elseif ( is_site_subdomain() && in_array( $post_type, array( 'glossary', 'webinars' ) ) ) { // exclude from sub
		continue;
	} elseif ( is_offline_school() && in_array( $post_type, array( 'events' ) ) ) {
		continue;
	}

	register_post_type( $post_type, array_merge( $default, $args ) );
}