<?php

define( 'PLL_CURRENT_LANGUAGE', function_exists('pll_current_language') ? pll_current_language() : '' );
define( 'PRELOAD_IMAGE', "data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20style='background-color:rgba(0,0,0,0.1)'%3E%3C/svg%3E");

function get_menu_by_location_origin( $location, $class = '' ) {
	global $wp;

	$locations = get_nav_menu_locations();
	$url       = $wp->request;
	$dropdown  = strpos( $class, 'dropdown' ) !== false;

	if ( $locations && isset( $locations[ $location ] ) ) {
		$menu_items = wp_get_nav_menu_items( $locations[ $location ] );
		$menu_list  = '<nav><ul class="' . $class . '">';

		foreach ( $menu_items as $menu_item ) {
			if ( ( $dropdown && $menu_item->menu_item_parent != 0 ) || ( ! $dropdown && $menu_item->url == '#' ) ) {
				continue;
			}

			if ( ! empty( $url ) && ! is_front_page() && strpos( $menu_item->url, $url ) !== false ) {
				$menu_item->classes[] = 'current';
			}

			if ( strpos( $menu_item->url, 'faq' ) !== false ) { // multilangual FAQ link
				$menu_item->url = get_site_lang_url( get_main_site_id() ) . 'faq/';
			}

			if ( $dropdown ) {
				if ( strpos( $menu_item->url, 'courses' ) !== false ) {
					$menu_item->classes[] = 'courses-items';
				}

				$menu_list .= '<li class="' . trim( implode( ' ', $menu_item->classes ) ) . '"><a href="' . $menu_item->url . '">' . $menu_item->title . '</a>';
				$sub_items = '';

				$sub_items_courses = '';

				if ( strpos( $menu_item->url, 'courses' ) !== false ) {
					$message           = tt( 'В ближайшее время курсов нет' );
					$our_courses       = tt( 'Наши филиалы' );
					$sub_items_courses .= '<li class="courses-item-first"><b>' . $our_courses . ':</b></li>';
					foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ) {
						$sub_items_courses .= '<li class="courses-item has-arrow">';
						switch_to_blog( $site->blog_id );

						$sub_items_courses .= '<a href="' . get_site_lang_url( $site->blog_id ) . 'courses/" '. ( is_disabled_site(get_site_lang_url( $site->blog_id )) ? 'class="disabled"' : '' ) .'>' . tt( $site->blogname, true ) . '</a><div class="dropdown-arrow"></div>';
						$nearest           = get_nearest_courses();
						if ( count( $nearest ) ) {
							$sub_items_courses .= '<ul class="sub-menu">';
							foreach ( $nearest as $course ) {
								$sub_items_courses .= '<li><a href="' . get_home_url() . '/courses/' . $course->post_name . '/">' . $course->post_title . '</a></li>';
							}
							$sub_items_courses .= '</ul>';
						} else {
							$sub_items_courses .= '<ul class="sub-menu">';
							$sub_items_courses .= '<li><span>' . $message . '</span></li>';
							$sub_items_courses .= '</ul>';
						}
						$sub_items_courses .= '</li>';
						restore_current_blog();
					}

					$sub_items = $sub_items_courses;
				}

				foreach ( $menu_items as $sub_item ) {
					if ( $sub_item->menu_item_parent != $menu_item->ID ) {
						continue;
					}

					$sub_items .= '<li class="' . trim( implode( ' ', $sub_item->classes ) ) . '" ><a target="' . ( $sub_item->target ?: '_self' ) . '" href = "' . $sub_item->url . '" > ' . $sub_item->title . '</a></li>';
				}

				if ( ! empty( $sub_items ) ) {
					$menu_list .= '<div class="dropdown-arrow"></div><ul class="dropdown-menu">' . $sub_items . '</ul>';
				}

				$menu_list .= '</li>';
			} else {
				$menu_list .= '<li class="' . trim( implode( ' ', $menu_item->classes ) ) . '" ><a href = "' . $menu_item->url . '"> ' . $menu_item->title . '</a></li>';
			}
		}

		return $menu_list . '</ul></nav> ';
	}

	return false;
}

function get_menu_by_location_sub( $location, $class = '' ) {
	global $wp;

	$locations = get_nav_menu_locations();
	$url       = $wp->request;
	$dropdown  = strpos( $class, 'dropdown' ) !== false;

	if ( $locations && isset( $locations[ $location ] ) ) {
		$menu_items = wp_get_nav_menu_items( $locations[ $location ] );
		$menu_list  = '<nav><ul class="' . $class . '">';

		foreach ( $menu_items as $menu_item ) {
			if ( ( $dropdown && $menu_item->menu_item_parent != 0 ) || ( ! $dropdown && $menu_item->url == '#' ) ) {
				continue;
			}

			if ( ! empty( $url ) && ! is_front_page() && strpos( $menu_item->url, $url ) !== false ) {
				$menu_item->classes[] = 'current';
			}

			if ( strpos( $menu_item->url, 'faq' ) !== false ) { // multilangual FAQ link
				$menu_item->url = get_site_lang_url( get_main_site_id() ) . 'faq/';
			}

			if ( $dropdown ) {
				$menu_list .= '<li class="' . trim( implode( ' ', $menu_item->classes ) ) . '"><a href="' . $menu_item->url . '">' . $menu_item->title . '</a>';
				$sub_items = '';

				foreach ( $menu_items as $sub_item ) {
					if ( $sub_item->menu_item_parent != $menu_item->ID ) {
						continue;
					}

					$sub_items .= '<li class="' . trim( implode( ' ', $sub_item->classes ) ) . '" ><a target="' . ( $sub_item->target ?: '_self' ) . '" href = "' . $sub_item->url . '" > ' . $sub_item->title . '</a></li>';
				}

				if ( ! empty( $sub_items ) ) {
					$menu_list .= '<div class="dropdown-arrow"></div><ul class="dropdown-menu">' . $sub_items . '</ul>';
				}

				$menu_list .= '</li>';
			} else {
				$menu_list .= '<li class="' . trim( implode( ' ', $menu_item->classes ) ) . '" ><a href = "' . $menu_item->url . '"> ' . $menu_item->title . '</a></li>';
			}
		}

		return $menu_list . '</ul></nav> ';
	}

	return false;
}

function is_site_subdomain() {
	if (is_offline_school()) {
		return true;
	} else {
		return get_current_blog_id() != get_main_site_id();
	}
}

function get_acf_repeater( $id, $slug ) {
	global $wpdb;

	$results  = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE post_id = %d and meta_key LIKE %s", $id, $slug . '_%' ) );
	$repeater = array();

	foreach ( $results as $meta ) {
		preg_match( "/$slug\_(\d+)_(\w+)/s", $meta->meta_key, $field );

		$repeater[ $field[1] ][ $field[2] ] = $meta->meta_value;
	}

	return json_decode( json_encode( $repeater ) );
}

function get_acf_options( $slug, $group = false, $single = false ) {
	global $wpdb;

	$slug    = 'options_' . $slug;
	if (!$single) {
		$results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->options WHERE option_name LIKE %s", $slug . '_%' ) );
	} else {
		$results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->options WHERE option_name LIKE %s", $slug ) );
	}
	
	$options = array();

	foreach ( $results as $option ) {
		if ($group) {
			if ( preg_match( "/$slug\_([^_]+)$/s", $option->option_name, $field ) ) {
				$options[$field[1]]['value'] = $option->option_value;
			}
			if ( preg_match( "/$slug\_(\w+)_(\d+)_(\w+)/s", $option->option_name, $field_repeater ) ) {
				$options[$field_repeater[1]][$field_repeater[3]][] = $option->option_value;
			}
		} elseif ($single) {
			$options[0] = $option->option_value;
		} else {
			preg_match( "/$slug\_(\d+)_(\w+)/s", $option->option_name, $field );

			$options[ $field[1] ][ $field[2] ] = $option->option_value;
		}
	}

	return json_decode( json_encode( $options ) );
}

function ett( $string, $origin = false ) { // echo theme translate
	if ( $origin ) {
		switch_to_blog( get_main_site_id() );

		function_exists( 'pll_e' ) ? pll_e( $string ) : _e( $string );

		restore_current_blog();
	} else {
		function_exists( 'pll_e' ) ? pll_e( $string ) : _e( $string );
	}
}

function tt( $string, $origin = false ) { // return theme translate
	if ( $origin ) {
		switch_to_blog( get_main_site_id() );

		$translated = function_exists( 'pll__' ) ? pll__( $string ) : __( $string );

		restore_current_blog();

		return $translated;
	}

	return function_exists( 'pll__' ) ? pll__( $string ) : __( $string );
}

function get_user_name( $id ) {
	$author = get_userdata( $id );

	return implode( ' ', array( $author->first_name, $author->last_name ) );
}

function get_reviews( $show, $all = false ) {
	$meta_query = array(
		array( 'key' => 'show_in', 'value' => $show, 'compare' => 'LIKE' )
	);

	if ( is_site_subdomain() ) {
		foreach ( get_posts( array(
				'post_type'   => 'reviews',
				'post_status' => 'publish',
				'numberposts' => - 1,
				'meta_query'  => $meta_query
			) ) as $graduate
		) {
			yield $graduate;
		}
	} else {
		$lang = pll_current_language();

		if ( $all ) {
			foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $subdomain ) {
				switch_to_blog( $subdomain->blog_id );

				foreach ( get_posts_by_lang( $lang, 'reviews', $meta_query ) as $graduate ) {
					yield $graduate;
				}

				restore_current_blog();
			}
		} else {
			$sites = $lang === 'az' ? array( 6 ) : ( $lang === 'uk' ? array( 3, 4, 5 ) : array( 2, 3, 4, 5, 6 ) );

			switch_to_blog( $sites[ array_rand( $sites ) ] );

			foreach ( get_posts_by_lang( $lang, 'reviews', $meta_query ) as $graduate ) {
				yield $graduate;
			}

			restore_current_blog();
		}
	}
}

function get_posts_by_lang( $lang, $type, $meta_query = null ) {
	/*$slug = "get_posts_by_lang_{$lang}_{$type}";

	if ( $results = get_transient( $slug ) ) {
		return $results;
	}*/

	$args = array(
		'post_type' => $type,
		'tax_query' => array(
			array(
				'taxonomy' => 'language',
				'field'    => 'slug',
				'terms'    => $lang,
			),
		)
	);

	if ( $meta_query ) {
		$args['meta_query'] = $meta_query;
	}

	$my_posts = new WP_Query();
//	$results  = $my_posts->query( $args );

//	set_transient( $slug, $results, MINUTE_IN_SECONDS );

	return $my_posts->query( $args );
}

function get_acf_group( $id, $slug ) {
	global $wpdb;

	$results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE post_id = %d and meta_key LIKE %s", $id, $slug . '_%' ) );
	$data    = array();

	foreach ( $results as $meta ) {
		$field = str_replace( "{$slug}_", '', $meta->meta_key );

		if ( ! preg_match( '/_\d+_/s', $field ) ) { // standard
			if ( isset($data[ $field ]) ) {
				continue;
			}

			$data[ $field ] = $meta->meta_value;
		} elseif ( preg_match( '/^(\D+)_(\d+)_(\D+)$/m', $field, $repeater ) ) { // one level
			if ( isset( $data[ $repeater[1] ] ) &&
				 ! is_string( $data[ $repeater[1] ] ) &&
				 isset( $data[ $repeater[1] ][ $repeater[2] ][ $repeater[3] ] ) &&
				 is_array( $data[ $repeater[1] ][ $repeater[2] ][ $repeater[3] ] )
				) {
				continue;
			}

			if ( ! isset( $data[ $repeater[1] ] ) || is_string( $data[ $repeater[1] ] ) ) {
				$data[ $repeater[1] ] = array();
			}

			$data[ $repeater[1] ][ $repeater[2] ][ $repeater[3] ] = $meta->meta_value;
		} elseif ( preg_match( '/^(\D+)_(\d+)_(\D+)_(\d+)_(\D+)$/m', $field, $repeater ) ) {
			if ( is_string( $data[ $repeater[1] ][ $repeater[2] ][ $repeater[3] ] ) ) {
				$data[ $repeater[1] ][ $repeater[2] ][ $repeater[3] ] = array();
			}

			$data[ $repeater[1] ][ $repeater[2] ][ $repeater[3] ][ $repeater[4] ][ $repeater[5] ] = $meta->meta_value;
		}
	}

	return json_decode( json_encode( $data, JSON_UNESCAPED_UNICODE ) );
}

if ( is_site_subdomain() ) { // only for subdomains
	function get_contacts_data( $all = false ) { // get contacts info from origin
		$meta_query = array(
			array( 'key' => 'contacts', 'compare_key' => 'LIKE', 'compare' => 'EXISTS' )
		);

		switch_to_blog( get_main_site_id() );

		$pages = get_posts_by_lang( pll_current_language(), 'page', $meta_query );

		if ( $pages ) {
			$results = get_acf_repeater( reset( $pages )->ID, 'contacts' );
		}

		restore_current_blog();

		if ( ! $all ) {
			$results = array_filter( $results, function ( $match ) {
				return $match->site == get_current_blog_id();
			} );
		}

		return ! empty( $results ) ? ( $all ? $results : reset( $results ) ) : false;
	}
}

function get_home_lang_url($id = false) {
	
	if ( is_front_page() && is_site_subdomain() && !$id ) {
		$url = get_site_url( get_main_site_id() ) . '/';

		if ( is_offline_school() ) {
			$subdomain_start = strpos($url, '//') + 2;
			$subdomain_end = strpos($url, '.');
			$subdomain_name = substr($url, $subdomain_start, $subdomain_end - $subdomain_start );
			$url = str_replace($subdomain_name.'.','', $url);
		}
	} else {
		$url = function_exists( 'pll_home_url' ) ? pll_home_url() : home_url();
	}

	return $url;
}

function get_site_lang_url( $id ) {
	switch_to_blog( $id );

	$url = get_home_lang_url($id);

	restore_current_blog();

	return $url;
}

function get_site_selector() {
	global $wp, $wp_query;

	$section  = is_front_page() ? '' : ( $wp_query->queried_object->name ?? $wp_query->queried_object->post_name );
	$selector = '<ul>';

	$args = is_offline_school() ? 
		array( 'site__not_in' => array(get_current_blog_id()) ) :
		array( 'site__not_in' => array(get_main_site_id(),get_current_blog_id()) ) ;


	$subdomains = get_sites( $args );

	$current_language = pll_current_language();

	foreach ( $subdomains as $subdomain ) {

		$link = $subdomain->siteurl;

		if ( $current_language == 'en' && $current_language != pll_default_language() ) {
			$link .= '/'.$current_language;
		} elseif ( $current_language == 'uk' && ( in_array( $subdomain->blog_id, array(3,4,5) ) || is_offline_school() ) ) {
			$link .= '/'.$current_language;
		}

		if ( get_post_type() == 'page' ) {
			$link .= '/' . ( ! empty( $section ) ? $section . '/' : '' );
		} else {
			$link .= '/';
		}
		$selector .= '<li><a href="' . $link . '" '. ( is_disabled_site($link) ? 'class="disabled"' : '' ) .'>' . tt( $subdomain->blogname ) . '</a></li>';
	}

	return $selector . '</ul>';
}


function get_nearest_courses( $groupped = false ) {
	$args = array(
		'post_type'   => 'courses',
		'meta_key'    => 'courses_nearest',
		'meta_value'  => 1,
		'numberposts' => - 1
	);

	if ( $groupped ) {
		$result = array();

		foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $subdomain ) {
			switch_to_blog( $subdomain->blog_id );

			foreach ( get_posts( $args ) as $course ) {
				$alternative = get_post_meta( $course->ID, 'courses_alternative_date', true );
				$vacancy = 0;

				if ( ! empty( $alternative ) ) {
					$date = $alternative;
				} else {
					$date = convert_acf_date( get_post_meta( $course->ID, 'courses_date', true ) );
					$vacancy = get_post_meta( $course->ID, 'courses_vacancy', true );
				}

				$result[ $course->post_name ]['title']    = $course->post_title;
				$result[ $course->post_name ]['image']    = wp_get_attachment_image_url( get_post_meta( $course->ID, 'courses_icon_white', true ), 'full' );
				$result[ $course->post_name ]['cities'][] = array(
					'name'    => $subdomain->blogname,
					'date'    => $date,
					'link'    => get_site_url() . '/courses/' . $course->post_name . '/',
					'vacancy' => $vacancy,
				);
			}

			restore_current_blog();
		}

		return json_decode( json_encode( $result, JSON_UNESCAPED_UNICODE ) );
	}

	return get_posts( $args );
}

function print_breadcrumbs() {
	$path        = array();
	$breadcrumbs = '<div class="breadcrumbs">';
	$current     = 0;

	if ( ! is_home() && ! is_front_page() ) {
		$path[ get_bloginfo( 'name' ) ] = get_home_lang_url();

		if ( is_tax( 'glossary_cat' ) ) {
			if ( $page = get_page_by_path( '/glossary/' ) ) {
				$path[ $page->post_title ]              = get_permalink( $page );
				$path[ single_term_title( '', false ) ] = null;
			}
		}

		if ( is_page() ) {
			$path[ get_the_title() ] = get_the_permalink();
		} elseif ( is_single() ) {
			if ( $section = get_post_type_archive_link( get_post_type() ) ) {
				$archive                               = get_post_type_object( get_post_type() );
				$path [ tt( $archive->labels->name ) ] = $section;
			} elseif ( get_post_type() == 'courses' ) {
				$path[ tt( 'Курсы' ) ] = get_home_lang_url() . 'courses/';
			}

			$path[ get_the_title() ] = get_the_permalink();

		} elseif( is_author() )  {

			$author = get_the_author();
			$author_url = get_author_posts_url(get_the_author_meta('ID'));
			$path[tt( $author )] = $author_url;

		} elseif ( is_archive() ) {

			$post_type = get_post_type();

			$path[ tt( post_type_archive_title( '', false ) ) ] = get_post_type_archive_link($post_type);
		}
	}
	unset($path['']);

	$json_ld = '<script type="application/ld+json">{ "@context": "https://schema.org", "@type": "BreadcrumbList", "itemListElement": [';	

	foreach ( $path as $title => $url ) {
		$current ++;
		$json_ld .= '{ "@type": "ListItem", "position": '.$current.', "item": { "@id": "' . $url . '", "name": "' . $title . '" } }' . ( $current < count($path) ? ',': '') ;

		if ( $current != count( $path ) ) {
			$breadcrumbs .= '<span><a href="' . $url . '"><span>' . $title . '</span></a></span>' . '<span class="delimiter">|</span>';
		} else {
			$breadcrumbs .= '<span class="breadcrumb-last"><span>' . $title . '</span></span>';
		}
	}

	$json_ld .= '] } </script> ';


	echo $breadcrumbs . '</div>' .$json_ld;
}

function get_header_ajax() {
	if ( ! isset( $_GET['ajax'] ) ) {
		get_header();
	} else {
		ob_start();
	}
}

function get_footer_ajax() {
	if ( ! isset( $_GET['ajax'] ) ) {
		get_footer();
	} else {
		$html = ob_get_clean();

		while ( ob_get_level() ) {
			ob_end_clean();
		}

		wp_send_json( array(
			'title'     => get_the_title(),
			'canonical' => get_post_meta( get_the_ID(), '_yoast_wpseo_canonical', true ) ?: wp_get_canonical_url(),
			'html'      => $html
		), JSON_UNESCAPED_UNICODE );
	}
}

function get_cyrillic_alphabet() {
	$alphabet = array();

	foreach ( range( chr( 0xC0 ), chr( 0xDF ) ) as $letter ) {
		$letter     = mb_convert_encoding( $letter, 'UTF-8', 'windows-1251' );
		$alphabet[] = $letter;

		if ( $letter == 'Е' ) {
			$alphabet[] = 'Ё';
		}
	}

	return $alphabet;
}

function get_glossary_word( $id ) {
	global $wpdb;

	$prepared = $wpdb->prepare( "SELECT post_title as title, post_content as content from $wpdb->posts WHERE ID = %d", $id );

	return $wpdb->get_row( $prepared );

}

function get_glossary_exists_letters( $category = null ) {
	global $wpdb;

	if ( $category ) {
		$query = "SELECT GROUP_CONCAT(DISTINCT(LEFT(post_title, 1))) FROM $wpdb->posts p JOIN $wpdb->term_relationships t ON t.term_taxonomy_id = $category AND t.object_id = p.ID WHERE post_type = 'glossary' and post_status = 'publish'";
	} else {
		$query = "SELECT GROUP_CONCAT(DISTINCT(LEFT(post_title, 1))) FROM $wpdb->posts WHERE post_type = 'glossary' and post_status = 'publish'";
	}

	if ( $enabled = $wpdb->get_var( $query ) ) {
		return explode( ',', $enabled );
	} else {
		return array();
	}
}

function get_glossary( $category = null ) {
	$args = array(
		'post_type'   => 'glossary',
		'order'       => 'asc',
		'orderby'     => 'post_title',
		'post_status' => 'publish',
		'numberposts' => - 1,
	);

	if ( $category ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'glossary_cat',
				'field'    => 'id',
				'terms'    => $category
			)
		);
	}

	foreach ( get_posts( $args ) as $post ) {
		$results[] = array( 'ID' => $post->ID, 'title' => $post->post_title, 'link' => get_permalink( $post->ID ) );
	}

	return json_decode( json_encode( $results, JSON_UNESCAPED_UNICODE ) );
}

function limit_text( $text, $limit = 75 ) {
	return trim( mb_substr( $text, 0, mb_strrpos( mb_substr( $text, 0, $limit ), ' ' ) ) ) . '...';
}

function convert_acf_date( $date ) {
	$date = preg_replace( '/(\d{4})(\d{2})(\d{2})/s', '$3.$2.$1', $date );

	return date_i18n( 'j F Y', strtotime( $date ) );
}

function get_country_code(): string {
	$current = get_current_blog_id();

	return in_array( $current, [ 2, 7, 8 ] ) ? 'RU' : ( $current === 6 ? 'AZ' : 'UA' );
}

function get_image_size( string $url, int $width = 0, int $height = 0 ): array {
	$url        = str_replace( 'https://' . $_SERVER['HTTP_HOST'] . '/', ABSPATH, $url );
	$dimensions = array( 'width' => $width, 'height' => $height );

	if ( preg_match( '/^.*?\.svg$/s', $url ) ) {
		$content = file_get_contents( $url );

		preg_match( '/width="(\d+)"/', $content, $svgwidth );
		preg_match( '/height="(\d+)"/', $content, $svgheight );

		$dimensions['width']  = $svgwidth[1];
		$dimensions['height'] = $svgheight[1];
	} elseif ( $size = getimagesize( $url ) ) {
		$dimensions['width']  = $size[0];
		$dimensions['height'] = $size[1];
	}

	return $dimensions;
}

function get_posts_by_blog_id($blog_id = 1, $language_id = 6, $post_type = 'events', $limit = 8) {

	$wpdb_ = new wpdb( DB_USER, DB_PASSWORD, DB_NAME, DB_HOST);
	if ( $blog_id == 1 ) {
		$db_prefix = 'wp_';
	} else {
		$db_prefix = 'wp_'.$blog_id.'_';
	}

	$query = '
		SELECT 
			%prefix%posts.ID,
			%prefix%posts.post_title,
			%prefix%posts.post_date,
			%prefix%posts.post_author,
			%prefix%posts.guid,
			%prefix%posts.post_name,
			wp_users.display_name,
			wp_users.user_nicename,
			(SELECT meta_value
				FROM %prefix%postmeta wp
				WHERE wp.post_id = %prefix%postmeta.meta_value AND wp.meta_key = "_wp_attached_file") AS image_link
		FROM 
			%prefix%posts
		LEFT JOIN 
			wp_users ON wp_users.ID = %prefix%posts.post_author
		LEFT JOIN 
			%prefix%postmeta ON %prefix%postmeta.post_id = %prefix%posts.ID
		LEFT JOIN 
			%prefix%term_relationships ON %prefix%term_relationships.object_id = %prefix%posts.ID
		WHERE
			%prefix%term_relationships.term_taxonomy_id = %language_id%
			AND %prefix%posts.post_type = "%post_type%" 
			AND %prefix%posts.post_status = "publish"
			AND %prefix%postmeta.post_id = %prefix%posts.ID 
			AND %prefix%postmeta.meta_key = "_thumbnail_id"
		ORDER BY
			%prefix%posts.post_date DESC
		LIMIT 
			%limit%';

	$query = str_replace(array('%prefix%', '%post_type%', '%limit%', '%language_id%', ), array($db_prefix, $post_type, $limit, $language_id ), $query );
	
	$result = $wpdb_->get_results($query);

	if ( empty ($result) ) {
		return false;
	}

	return $result;
}

function pagination_links_all() {
	$args = array(
		'show_all'  => true,
		'prev_next' => false,
	);

	$result = '';

	$links = paginate_links( $args );

	if ( ! empty( $links ) ) {
		$result = sprintf('<div class="pagination-links-all"><input type="checkbox" name="pagination"><span class="pagination-links-button">%s</span><div class="pagination-links-list">%s</div></div>', tt( 'All pages',  ), $links) ;
	}

	return $result;
}

function is_disabled_site($url) {
	if ( true == preg_match('/(spb|ekb|kazan)/', $url ) ) {
		return true;
	} else {
		return false;
	}
}

function filter_content_titles($text) {
	
	$text_matches = preg_match_all('/<h(2|3|4)(?:[^>])*>(.*)<\/h(?:2|3|4)>/', $text, $matches );
	if ( !empty($matches[2]) ) {
	  $i = 0;
	  foreach ($matches[0] as $match) {
		  $text = str_replace($match, '<h' . $matches[1][$i] . '>' . strip_tags($matches[2][$i]) . '</h' . $matches[1][$i] . '>', $text);
		  $i++;
	  }
  }
  return $text;
}

function is_russian_site() {
	if ( true == preg_match('/(spb|ekb|kazan)/', get_site_url() ) ) {
		return true;
	} else {
		return false;
	}
}

function is_ukrainian_site() {
	if ( true == preg_match('/(kharkiv|dnipro|kyiv)/', get_site_url() ) ||
		 is_offline_school() ) {
		return true;
	} else {
		return false;
	}
}