<?php

trait ThemeAjax {

	public $cities_array =  array(
		2 => 'Питер',
		3 => 'Харьков',
		4 => 'Днепр',
		5 => 'Киев',
		6 => 'Баку',
		7 => 'Казань',
		8 => 'Екатеринбург',
		1 => 'Симферополь',
		10 => 'Чернигов',
		11 => 'Ивано-Франковск',
		12 => 'Херсон',
		13 => 'Хмельницкий',
		14 => 'Кропивницкий',
		15 => 'Луцк',
		16 => 'Николаев',
		17 => 'Одесса',
		18 => 'Полтава',
		19 => 'Ровно',
		20 => 'Ужгород',
		21 => 'Винница',
		22 => 'Запорожье',
		23 => 'Житомир',
	);
	
	public function joinUs() {
		if ( wp_verify_nonce( $_POST['join'] ) ) {
			$name   = filter_input( INPUT_POST, 'full_name', FILTER_SANITIZE_STRING );
			$phone  = filter_input( INPUT_POST, 'phone', FILTER_SANITIZE_STRING );
			$cities = $this->cities_array;			
			$city   = $cities[ get_current_blog_id() ] ?? '';

			if ($city == '') {
				$city_input  = filter_input( INPUT_POST, 'city', FILTER_SANITIZE_STRING );
				$city_key = array_search($city_input,$cities, true);
				if ($city_key !== false) {
					$city = $city_input;
				}
			}

			if ( is_disabled_site( get_site_url() ) ) {
				wp_send_json_error( array( 'message' => 'Incorrect inputs' ) );
			}
			
			if ( $name && $phone ) {
				$name     = explode( ' ', $name );
				$fields   = array(
					'NAME'         => $name[0],
					'LAST_NAME'    => $name[1],
					'STATUS_ID'    => 'NEW',
					'SOURCE_ID'    => 'WEB',
					'PHONE'        => array(
						array(
							'VALUE' => $phone
						)
					),
					'ADDRESS_CITY' => $city
				);
				$fields   = $this->generateUtmBitrix( $fields, $_POST['_wp_http_referer'] );
				$response = $this->sendToBitrix( 'crm.lead.add', array( 'fields' => $fields ) );

				if ( ! is_wp_error( $response ) ) {
					wp_send_json_success( $response );
				} else {
					wp_send_json_error( $response->get_error_message() );
				}
			}
		} else {
			wp_send_json_error( array( 'message' => 'Incorrect nonce' ) );
		}
	}

	public function loadArticles() {
		$types = array( 'events', 'blog', 'all' );

		if ( isset( $_POST['type'] ) && in_array( $_POST['type'], $types ) ) {
			$query = array(
				'post_type'   => $_POST['type'] == 'all' ? array( 'events', 'blog' ) : $_POST['type'],
				'numberposts' => isset( $_POST['author'] ) ? 8 : 9,
				'offset'      => intval( $_POST['offset'] ),
				'lang'        => strpos( wp_get_referer(), '/uk/' ) !== false ? 'uk' : 'ru'
			);

			if ( isset( $_POST['author'] ) ) {
				$query['author'] = filter_input( INPUT_POST, 'author', FILTER_SANITIZE_NUMBER_INT );
			}

			$data = array();

			foreach ( get_posts( $query ) as $post ) {
				if ( $origin = get_post_meta( $post->ID, 'origin', true ) ) {
					$image = $origin['image'];
				} else {
					$image = get_the_post_thumbnail_url( $post->ID, array( 0, '240' ) );
				}
				$data[] = array(
					'link'        => get_post_permalink( $post->ID ),
					'image'       => $image,
					'title'       => $post->post_title,
					'author_link' => '/author/' . get_user_meta( $origin['author'] ?? $post->post_author, 'nickname', true ) . '/',
					'author'      => get_user_name( $origin['author'] ?? $post->post_author ),
					'date'        => date( 'd.m.Y', strtotime( $post->post_date ) )
				);
			}

			wp_send_json_success( $data );
		} else {
			wp_send_json_error();
		}
	}

	public function loadCourses() {
		if ( isset( $_POST['city'] ) ) {
			if ( $site = filter_input( INPUT_POST, 'city', FILTER_SANITIZE_NUMBER_INT ) ) {
				$data = array();

				switch_to_blog( $site );

				foreach ( get_posts( array( 'post_type' => 'courses', 'numberposts' => 10 ) ) as $post ) {
					$excluded = array( 'python', 'js', 'fotoshop', 'php', 'react', 'veb-dizajn', 'kompyuternye-kursy' );

					if ( in_array( $post->post_name, $excluded ) ) {
						continue;
					}

					$data[] = $post->post_title;
				}

				restore_current_blog();
				wp_send_json_success( $data );
			}
		}
	}

	public function auth() {
		$email    = filter_input( INPUT_POST, 'email', FILTER_VALIDATE_EMAIL );
		$password = filter_input( INPUT_POST, 'password', FILTER_SANITIZE_STRING );

		if ( $email && $password ) {
			if ( $user = wp_authenticate( $email, $password ) ) {
				if ( ! is_wp_error( $user ) ) {
					wp_set_auth_cookie( $user->ID );
					wp_send_json_success( array( 'redirect_to' => get_home_lang_url() . 'lichnyj-kabinet/' ) );
				} else {
					wp_send_json_error( array( 'message' => 'Credentials are invalid.' ) );
				}
			}
		} else {
			wp_send_json_error();
		}
	}

	public function checkCertificate() {
		if ( wp_verify_nonce( $_POST['certificate'] ) ) {
			$certificate_number = filter_input( INPUT_POST, 'certificate_number', FILTER_SANITIZE_STRING );
			$last_name          = filter_input( INPUT_POST, 'last_name', FILTER_SANITIZE_STRING );

			try {
				$pdo         = new PDO( "mysql:host=mariadb-certs;dbname=wordpress;charset=utf8", 'wordpress', 'bJ13hAyfHfJkmVawHjeURmVa' );
				$certificate = explode( ' ', $certificate_number );
				$prepared    = $pdo->prepare( 'SELECT * FROM data WHERE series = ? AND number = ? AND full_name_student LIKE ?' );

				$prepared->execute( array( $certificate[0], $certificate[1], "%$last_name%" ) );

				if ( $result = $prepared->fetch() ) {
					wp_send_json_success( array(
						'message' => tt( 'Сертификат действителен' ),
						'color'   => 'green'
					) );
				} else {
					wp_send_json_error( array( 'message' => tt( 'Сертификат не найден' ), 'color' => 'red' ) );
				}
			} catch ( PDOException $e ) {
				wp_send_json_error( $e );
			}
		} else {
			wp_send_json_error();
		}
	}

	public function signCourse() {
		if ( wp_verify_nonce( $_POST['grant'] ) || 
			 wp_verify_nonce( $_POST['sign_course'] ) ||
			 wp_verify_nonce( $_POST['faq'] ) ||
			 wp_verify_nonce( $_POST['sign_course_online']
			)) {
			$name   = filter_input( INPUT_POST, 'full_name', FILTER_SANITIZE_STRING );
			$city_post   = filter_input( INPUT_POST, 'city_id', FILTER_SANITIZE_NUMBER_INT );
			$email  = filter_input( INPUT_POST, 'email', FILTER_VALIDATE_EMAIL );
			$phone  = filter_input( INPUT_POST, 'phone', FILTER_SANITIZE_STRING );
			$course = filter_input( INPUT_POST, 'course', FILTER_SANITIZE_STRING );

			if ( is_disabled_site( get_site_url() ) ) {
				wp_send_json_error( array( 'message' => 'Incorrect inputs' ) );
			}

			if ( $name && $city_post && $email && $phone ) {
				$cities = $this->cities_array;

				$city = $cities[ $city_post ];
				
				$name     = explode( ' ', $name );
				$fields   = array(
					'NAME'                 => $name[0],
					'LAST_NAME'            => $name[1],
					'PHONE'                => array(
						array(
							'VALUE' => $phone
						)
					),
					'EMAIL'                => array(
						array(
							'VALUE' => $email
						)
					),
					'STATUS_ID'            => 'NEW',
					'SOURCE_ID'            => 'WEB',
					'ADDRESS_CITY'         => $city,
					'UF_CRM_1563966219246' => $course
				);
				
				if ( $city_post == '9999' ) {
					$fields['COMMENTS'] = 'Online school form';
				}
				$fields   = $this->generateUtmBitrix( $fields, $_POST['_wp_http_referer'] );
				$response = $this->sendToBitrix( 'crm.lead.add', array( 'fields' => $fields ) );

				if ( ! is_wp_error( $response ) ) {
					wp_send_json_success( $response );
				} else {
					wp_send_json_error( $response->get_error_message() );
				}
			} else {
				wp_send_json_error( array( 'message' => 'Incorrect inputs' ) );
			}
		} else {
			wp_send_json_error( array( 'message' => 'Incorrect nonce' ) );
		}
	}

	public function askQuestion() {
		if ( wp_verify_nonce( $_POST['ask_question'] ) ) {
			$name     = filter_input( INPUT_POST, 'full_name', FILTER_SANITIZE_STRING );
			$phone    = filter_input( INPUT_POST, 'phone', FILTER_SANITIZE_STRING );
			$question = filter_input( INPUT_POST, 'question', FILTER_SANITIZE_STRING );

			if ( is_disabled_site( get_site_url() ) ) {
				wp_send_json_error( array( 'message' => 'Incorrect inputs' ) );
			}

			if ( $name && $phone && $question ) {
				$cities = $this->cities_array;

				$city     = $cities[ get_current_blog_id() ] ?? '';
				$name     = explode( ' ', $name );
				$fields   = array(
					'NAME'         => $name[0],
					'LAST_NAME'    => $name[1],
					'PHONE'        => array(
						array(
							'VALUE' => $phone
						)
					),
					'STATUS_ID'    => 'NEW',
					'SOURCE_ID'    => 'WEB',
					'ADDRESS_CITY' => $city,
					'COMMENTS'     => $question
				);
				$fields   = $this->generateUtmBitrix( $fields, $_POST['_wp_http_referer'] );
				$response = $this->sendToBitrix( 'crm.lead.add', array( 'fields' => $fields ) );

				if ( ! is_wp_error( $response ) ) {
					wp_send_json_success( $response );
				} else {
					wp_send_json_error( $response->get_error_message() );
				}
			}
		} else {
			wp_send_json_error( array( 'message' => 'Incorrect nonce' ) );
		}
	}

	public function grantChance() {
		if ( wp_verify_nonce( $_POST['grant_chance'] ) ) {
			$name  = filter_input( INPUT_POST, 'full_name', FILTER_SANITIZE_STRING );
			$phone = filter_input( INPUT_POST, 'phone', FILTER_SANITIZE_STRING );
			$email = filter_input( INPUT_POST, 'email', FILTER_VALIDATE_EMAIL );

			if ( is_disabled_site( get_site_url() ) ) {
				wp_send_json_error( array( 'message' => 'Incorrect inputs' ) );
			}

			if ( ! is_site_subdomain() ) {
				$city = filter_input( INPUT_POST, 'city_id', FILTER_SANITIZE_STRING );
			} else {
				$city = get_current_blog_id();
			}

			if ( $name && $phone && $email ) {
				$name     = explode( ' ', $name );
				$cities = $this->cities_array;

				$city     = $cities[ $city ] ?? '';
				$fields   = array(
					'NAME'         => $name[0],
					'LAST_NAME'    => $name[1],
					'PHONE'        => array(
						array(
							'VALUE' => $phone
						)
					),
					'EMAIL'        => array(
						array(
							'VALUE' => $email
						)
					),
					'ADDRESS_CITY' => $city,
					'STATUS_ID'    => 'NEW',
					'SOURCE_ID'    => 'WEB',
				);
				$fields   = $this->generateUtmBitrix( $fields, $_POST['_wp_http_referer'] );
				$response = $this->sendToBitrix( 'crm.lead.add', array( 'fields' => $fields ) );

				if ( ! is_wp_error( $response ) ) {
					wp_send_json_success( $response );
				} else {
					wp_send_json_error( $response->get_error_message() );
				}
			}
		} else {
			wp_send_json_error( array( 'message' => 'Incorrect nonce' ) );
		}
	}

	private function sendToBitrix( $method, $data = array() ) {
		return wp_remote_post( BITRIX_HOOK_URL . $method, array(
			'body' => $data
		) );		
	}

	private function generateUtmBitrix( $fields, $referrer ) {
		if ( strpos( $referrer, 'utm' ) !== false ) {
			$utm = array(
				'UF_CRM_1572015048' => 'utm_source',
				'UF_CRM_1572015072' => 'utm_campaign',
				'UF_CRM_1572015096' => 'utm_term',
				'UF_CRM_1572015120' => 'utm_medium',
				'UF_CRM_1572015141' => 'utm_content',
				'utm_addata'        => 'sky',
				'UF_CRM_1587019168' => 'budget_id',
//				'UF_CRM_1571926582' => 'traffic_manager',
//				'UF_CRM_1571926636' => 'offer'
			);

			parse_str( parse_url( $_POST['_wp_http_referer'], PHP_URL_QUERY ), $get );

			foreach ( $get as $key => $value ) {
				if ( $field = array_search( $key, $utm ) ) {
					$fields[ $field ] = $value;
				}
			}
		}

		if ( ! isset( $fields['UF_CRM_1572015048'] ) ) {
			$fields['UF_CRM_1572015048'] = 'organic__website';
		}

		$fields['UF_CRM_1565183168578'] = home_url() . $referrer;

		return $fields;
	}

	public function getWord() {
		if ( isset( $_POST['id'] ) ) {
			$word = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT );

			wp_send_json_success( get_glossary_word( $word ) );
		} else {
			wp_send_json_error( array( 'message' => 'Incorrect word ID' ) );
		}
	}

	public function loadWebinars() {
		$query = array(
			'post_type'   => 'webinars',
			'numberposts' => 12,
			'offset'      => intval( $_POST['offset'] ),
		);

		$data = array();

		foreach ( get_posts( $query ) as $post ) {
			$data[] = array(
				'link'     => get_post_permalink( $post->ID ),
				'image'    => get_post_meta( $post->ID, 'youtube_thumbnail', true ),
				'title'    => $post->post_title,
				'desc'     => limit_text( $post->post_content ),
				'category' => wp_get_object_terms( $post->ID, 'webinars_cat' )[0]->name,
				'duration' => get_post_meta( $post->ID, 'duration', true )
			);
		}

		wp_send_json_success( array(
			'webinars' => $data,
			'total'    => intval( wp_count_posts( 'webinars' )->publish )
		) );
	}
}