<?php get_header(); ?>
	<!-- MAIN -->
	<main>
		<div class="container-fluid articles-bg">
			<div class="container">
				<?php print_breadcrumbs(); ?>
				<!-- BLOG -->
				<div class="articles">
					<h1><?php ett( post_type_archive_title( '', false ) ); ?></h1>
					<div class="articles-list">
						<?php if ( have_posts() ): ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php $link = get_permalink( $post->ID ); ?>
								<figure class="article">
									<?php $origin = get_post_meta( $post->ID, 'origin', true ); ?>
									<?php $thumbnail = $origin['image'] ?? get_the_post_thumbnail_url( $post->ID, 'medium' ); ?>
									<a class="article-link" href="<?php echo $link; ?>">
										<div class="article-preloader"></div>
										<img src="<?php echo $thumbnail; ?>" alt="<?php echo $post->post_title; ?>"
											 width="382" height="240">
									</a>
									<figcaption>
										<a href="<?php echo $link; ?>"><?php echo $post->post_title; ?></a>
										<div class="author">
											<span><?php echo get_user_name( $origin['author'] ?? $post->post_author ); ?></span>
											<time datetime="<?php echo $post->post_date; ?>"><?php echo date( 'd.m.Y', strtotime( $post->post_date ) ); ?></time>
										</div>
									</figcaption>
								</figure>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
				<?php get_template_part( 'parts/general/pagination' ); ?>
				<?php get_template_part( 'parts/general/article', 'email-subscription' ); ?>
				<!-- BLOG -->
			</div>
		</div>
	</main>
	<!-- MAIN -->
<?php get_footer(); ?>