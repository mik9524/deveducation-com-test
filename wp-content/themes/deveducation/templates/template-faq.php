<?php
/**
 * Template Name: FAQ Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- MAIN -->
    <main>
        <div class="container">
			<?php print_breadcrumbs(); ?>
            <!-- FAQ -->
            <div class="faq" itemscope itemtype="https://schema.org/FAQPage">
                <h1><?php echo get_the_title(); ?></h1>
				<?php foreach ( get_acf_repeater( get_the_ID(), 'questions' ) as $number => $question ): ?>
                    <div class="question" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                        <div class="question-title" itemprop="name">
							<?php echo $number + 1; ?>. <?php echo $question->title; ?>
                        </div>
                        <div class="question-answer" itemscope itemprop="acceptedAnswer"
                             itemtype="https://schema.org/Answer">
                            <div itemprop="text">
								<?php echo nl2br( $question->answer ); ?>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
            <!-- FAQ -->
        </div>
        <section class="faq-form">
            <div class="container">
                <h2><?php ett( 'Заполни форму и получи грант на обучение в ближайшем потоке курса!' ); ?></h2>
                <form data-ajax-action="sign_course">
					<?php wp_nonce_field( - 1, 'faq' ); ?>
                    <input name="full_name" type="text" placeholder="<?php ett( 'ФИО' ); ?>" required>
                    <div class="custom-select">
                        <select class="choose-city" name="city_id" required>
                            <option selected disabled value=""><?php ett( 'Выберите город' ); ?></option>
							<?php foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ): ?>
                                <?php if ( is_disabled_site($site->domain) ) continue; ?>
                                <option value="<?php echo $site->blog_id; ?>"><?php ett( $site->blogname ); ?></option>
							<?php endforeach; ?>
                        </select>
                    </div>
                    <input type="email" name="email" placeholder="Email" required>
                    <input class="phone-mask" name="phone" type="text" placeholder="<?php ett( 'Телефон' ); ?>"
                           required>
                    <div class="custom-select">
                        <select name="course" class="courses-list" disabled required>
                            <option selected disabled value=""><?php ett( 'Выберите курс' ); ?></option>
                        </select>
                    </div>
                    <button><?php ett( 'Отправить' ); ?></button>
                </form>
            </div>
        </section>
    </main>
    <!-- MAIN -->
<?php get_footer(); ?>