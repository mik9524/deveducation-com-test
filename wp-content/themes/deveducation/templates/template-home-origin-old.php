<?php
/**
 * Template Name: Home Origin
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */

get_header( null, array( 'transparent' => true ) );
?>
<!-- MAIN -->
<main>
    <div class="home-page">
        <div class="home-page-info">
            <div class="container">
                <h1><?php echo get_post_meta( get_the_ID(), 'title', true ); ?></h1>
				<?php echo get_post_meta( get_the_ID(), 'description', true ); ?>
            </div>
        </div>
        <div class="home-page-useful">
            <div class="container">
				<?php foreach ( get_acf_repeater( get_the_ID(), 'advantages' ) as $advantage ): ?>
                    <div class="home-page-useful-item">
                        <div class="useful-image">
                            <img src="<?php echo wp_get_attachment_image_url( $advantage->image ); ?>"
                                 alt="<?php echo $advantage->title; ?>" width="63" height="63">
                        </div>
                        <div class="useful-text">
                            <div class="useful-title"><?php echo $advantage->title; ?></div>
                            <div class="useful-desc"><?php echo $advantage->description; ?></div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
        <!-- LOGIN FORM BLOCK -->
        <div class="login-block">
            <div class="container">
                <div class="login-info">
                    <span><?php ett( 'DevEducation в партнерстве с RemoteMode открывает корпоративный университет!' ); ?></span>
                    <a id="login-link" href="#" target="_blank"><?php ett( 'Вход для сотрудников' ); ?></a>
                </div>
            </div>
        </div>
        <!-- LOGIN FORM BLOCK -->
        <div class="home-page-map">
            <h2><?php ett( 'Выбери свой город на карте' ); ?>:</h2>
            <div class="preload-map" style="display: none;">
                <div class="progress-line"></div>
            </div>
            <div class="map-cities" style="display: none;">
                <img class="map-img" src="<?php echo ASSETS_URI . '/img/map.png'; ?>" alt="Map"
                     width="929" height="690">
                <a href="<?php echo get_site_lang_url( 2 ); ?>" class="map-pointer pointer-left spb">
					<?php ett( 'Санкт-Петербург' ); ?>
                    <img src="<?php echo ASSETS_URI . '/img/icons/ru.svg'; ?>"
                         alt="<?php ett( 'Санкт-Петербург' ); ?>" width="14" height="10">
                </a>
                <a href="<?php echo get_site_lang_url( 5 ); ?>" class="map-pointer pointer-right kyiv">
					<?php ett( 'Киев' ); ?>
                    <img src="<?php echo ASSETS_URI . '/img/icons/ua.svg'; ?>"
                         alt="<?php ett( 'Киев' ); ?>" width="14" height="10">
                </a>
                <a href="<?php echo get_site_lang_url( 4 ); ?>" class="map-pointer pointer-right dnepr">
					<?php ett( 'Днепр' ); ?>
                    <img src="<?php echo ASSETS_URI . '/img/icons/ua.svg'; ?>"
                         alt="<?php ett( 'Днепр' ); ?>" width="14" height="10">
                </a>
                <a href="<?php echo get_site_lang_url( 3 ); ?>" class="map-pointer pointer-left kharkiv">
					<?php ett( 'Харьков' ); ?>
                    <img src="<?php echo ASSETS_URI . '/img/icons/ua.svg'; ?>"
                         alt="<?php ett( 'Харьков' ); ?>" width="14" height="10">
                </a>
                <a href="<?php echo get_site_lang_url( 6 ); ?>" class="map-pointer pointer-left baku">
					<?php ett( 'Баку' ); ?>
                    <img src="<?php echo ASSETS_URI . '/img/icons/az.svg'; ?>"
                         alt="<?php ett( 'Баку' ); ?>" width="14" height="10">
                </a>
                <div class="map-pointer pointer-left israel">
					<?php ett( 'Тель-Авив' ); ?>
                    <img src="<?php echo ASSETS_URI . '/img/icons/isr.svg'; ?>"
                         alt="<?php ett( 'Тель-Авив' ); ?>" width="14" height="10">
                    <div class="pointer-sub"><?php ett( 'Главный офис' ); ?></div>
                </div>
            </div>
        </div>
    </div>
    <section class="nearest-courses">
        <h2><?php ett( 'Ближайшие курсы' ); ?></h2>
        <div class="container">
            <div class="splide nearest-courses-list">
                <div class="splide__track">
                    <ul class="splide__list">
						<?php foreach ( get_nearest_courses( true ) as $course ): ?>
                            <li class="splide__slide course">
                                <div class="course-top">
                                    <img src="<?php echo $course->image; ?>"
                                         alt="<?php echo $course->title; ?>"
                                         width="80">
                                    <span><?php echo $course->title; ?></span>
                                </div>
                                <div class="course-bottom">
									<?php foreach ( $course->cities as $city ): ?>
                                        <a href="<?php echo $city->link; ?>" class="course-city">
											<?php ett( $city->name, true ); ?>
                                            <span><?php echo $city->date; ?></span>
                                        </a>
									<?php endforeach; ?>
                                </div>
                                <div class="course-shadow"></div>
								<?php if ( count( $course->cities ) > 1 ): ?>
                                    <div class="arrow-next"></div>
								<?php endif; ?>
                            </li>
						<?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="grant-chance">
        <h2><?php ett( 'Заполни форму и узнай свои шансы на получение гранта' ); ?></h2>
        <div class="container">
            <form data-ajax-action="grant_chance">
				<?php wp_nonce_field( - 1, 'grant_chance' ); ?>
                <input type="text" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
                <input class="phone-mask" name="phone" type="text" placeholder="<?php ett( 'Телефон' ); ?>"
                       required>
                <div class="custom-select">
                    <select class="choose-city" name="city_id" required>
                        <option<?php echo ! is_site_subdomain() ? ' selected' : ''; ?> disabled
                                                                                       value=""><?php ett( 'Выберите город' ); ?></option>
						<?php foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ): ?>
                            <option value="<?php echo $site->blog_id; ?>"<?php echo get_current_blog_id() == $site->blog_id ? ' selected' : ''; ?>><?php ett( $site->blogname ); ?></option>
						<?php endforeach; ?>
                    </select>
                </div>
                <input type="email" name="email" placeholder="Email" required>
                <button><?php ett( 'Получить грант' ); ?></button>
            </form>
            <div class="form-result"><?php ett( 'Успешно отправлено' ); ?></div>
        </div>
    </section>
    <section class="mountains <?php echo pll_current_language(); ?>" style="">
        <h2>Deveducation</h2>
		<?php $features = get_acf_repeater( get_the_ID(), 'features' ); ?>
        <div class="container">
            <div class="mountain-row">
                <div class="mountain-text">
                    <div class="mountain-text-title"><?php echo $features[0]->title; ?></div>
                    <div class="mountain-text-desc"><?php echo $features[0]->subtitle; ?></div>
                </div>
                <div class="mountain-text">
                    <div class="mountain-text-title"><?php echo $features[1]->title; ?></div>
                    <div class="mountain-text-desc"><?php echo $features[1]->subtitle; ?></div>
                </div>
            </div>
            <div class="mountain-row">
                <div class="mountain-text">
                    <div class="mountain-text-title"><?php echo $features[2]->title; ?></div>
                    <div class="mountain-text-desc"><?php echo $features[2]->subtitle; ?></div>
                </div>
                <div class="mountain-text">
                    <div class="mountain-text-title"><?php echo $features[3]->title; ?></div>
                    <div class="mountain-text-desc"><?php echo $features[3]->subtitle; ?></div>
                </div>
            </div>
            <div class="mountain-about">
                <div class="mountain-about-top"><?php echo get_post_meta( get_the_ID(), 'mountains_about_top', true ); ?></div>
                <div class="mountain-about-desc"><?php echo get_post_meta( get_the_ID(), 'mountains_about_bottom', true ); ?></div>
            </div>
        </div>
    </section>
    <section class="career-steps">
        <h2><?php echo get_post_meta( get_the_ID(), 'step_block_title', true ); ?></h2>
        <div class="container">
            <div class="steps">
				<?php foreach ( get_acf_repeater( get_the_ID(), 'steps' ) as $number => $step ): ?>
                    <div class="step">
                        <div class="step-number"><span><?php echo ++ $number; ?></span></div>
                        <div class="step-desc"><?php echo $step->text; ?></div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </section>
    <section class="faq-block">
        <div class="faq-block-text">
            <h3>FAQ:</h3>
            <span><?php ett( 'ответы на популярные вопросы об обучении в DevEducation' ); ?></span>
            <a href="<?php echo get_site_lang_url( get_main_site_id() ); ?>faq/"
               target="_blank"><?php ett( 'Читать' ); ?></a>
        </div>
    </section>
	<?php $events = get_posts( array( 'post_type' => 'events', 'numberposts' => 8 ) ); ?>
	<?php if ( count( $events ) ): ?>
        <div class="articles-block">
            <div class="container">
                <div class="articles-block-title"><?php ett( 'Новости' ); ?></div>
                <div class="articles-list">
					<?php foreach ( $events as $event ): ?>
                        <div class="article">
							<?php $link = get_permalink( $event->ID ); ?>
							<?php $origin = get_post_meta( $event->ID, 'origin', true ); ?>
							<?php $size = array( 0, '200' ); ?>
							<?php $thumbnail = $origin['image'] ?? get_the_post_thumbnail_url( $event->ID, $size ); ?>
							<?php $title = preg_replace( '/"(.*)"/', '«$1»', $event->post_title ); ?>
                            <a href="<?php echo $link; ?>">
                                <img src="<?php echo $thumbnail; ?>" alt="<?php echo $title; ?>"
                                     height="200">
                            </a>
                            <div class="article-desc">
                                <div class="article-title">
                                    <a href="<?php echo $link; ?>"><?php echo $title; ?></a>
                                </div>
                                <div class="article-author">
                                    <a href="/author/<?php echo get_user_meta( $origin['author'] ?? $event->post_author, 'nickname', true ); ?>/"><?php echo get_user_name( $origin['author'] ?? $event->post_author ); ?></a>
                                    <time datetime="<?php echo $event->post_date; ?>"><?php echo date( 'd.m.Y', strtotime( $event->post_date ) ); ?></time>
                                </div>
                            </div>
                        </div>
					<?php endforeach; ?>
                </div>
                <a href="<?php echo get_post_type_archive_link( 'events' ); ?>"
                   class="more-events"><?php ett( 'Больше новостей' ); ?></a>
            </div>
        </div>
	<?php endif; ?>
	<?php if ( get_post_meta( get_the_ID(), 'partners_visible', true ) ): ?>
        <div class="partners">
            <div class="container">
                <div class="partners-title"><?php ett( 'Наши партнёры' ); ?></div>
                <div class="partners-list">
					<?php foreach ( get_acf_repeater( get_the_ID(), 'partners' ) as $partner ): ?>
                        <a href="<?php echo $partner->link; ?>" target="_blank" rel="nofollow"><img
                                    src="<?php echo wp_get_attachment_image_url( $partner->logo, 'full' ); ?>"
                                    height="50" alt="Partner"></a>
					<?php endforeach; ?>
                </div>
            </div>
        </div>
	<?php endif; ?>
</main>
<!-- MAIN -->
<?php get_footer(); ?>
