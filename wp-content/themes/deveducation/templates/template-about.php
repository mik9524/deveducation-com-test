<?php
/**
 * Template Name: About Us Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */

$url_language = PLL_CURRENT_LANGUAGE == 'ru' || PLL_CURRENT_LANGUAGE == 'uk' ? '' : PLL_CURRENT_LANGUAGE.'/';
$url_language_uk = PLL_CURRENT_LANGUAGE == 'ru' ? '' : PLL_CURRENT_LANGUAGE.'/';
?>
<?php get_header(); ?>
    <!-- MAIN -->
    <main>
        <div class="container">
			<?php print_breadcrumbs(); ?>
            <!-- ABOUT -->
            <div class="about<?php echo is_site_subdomain() ? ' subdomain' : ' origin'; ?>">
				<?php if ( pll_current_language() == 'ru' ): ?>
                    <noscript>
                        <div class="about-sub">
                            <a href="/graduates/"><?php ett( 'Выпускники' ); ?></a>
                            <?php if ( ! is_offline_school() ) : ?>
                            <a href="/events/"><?php ett( 'Новости' ); ?></a>
                            <?php endif; ?>
                        </div>
                    </noscript>
				<?php endif; ?>
                <div class="about-description">
                    <h1><?php echo get_post_meta( get_the_ID(), 'title', true ); ?></h1>
					<?php echo get_post_meta( get_the_ID(), 'description', true ); ?>
                    <div class="get-more">
                        <span><?php echo get_post_meta( get_the_ID(), 'get_more', true ); ?></span>
                        <a href="<?php echo get_home_lang_url() . 'courses/'; ?>"><?php echo get_post_meta( get_the_ID(), 'button', true ); ?></a>
                    </div>
                    <div class="advantages">
						<?php foreach ( get_acf_repeater( get_the_ID(), 'advantages' ) as $advantage ): ?>
                            <div class="advantage">
                                <div class="advantage-image">
									<?php $link = wp_get_attachment_image_url( $advantage->image ); ?>
									<?php $sizes = get_image_size( $link ); ?>
                                    <img src="<?php echo $link; ?>"
                                         alt="Advantage" 
                                         title="Advantage"
                                         width="<?php echo $sizes['width']; ?>"
                                         height="<?php echo $sizes['height']; ?>">
                                </div>
                                <div class="advantage-text">
                                    <div class="advantage-title"><?php echo $advantage->title; ?></div>
                                    <div class="advantage-desc"><?php echo $advantage->description; ?></div>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
                <div class="about-map">
                    <div class="map-cities">
                        <img class="map-img" src="<?php echo ASSETS_URI . '/img/short-map.svg'; ?>" width="650" height="659" alt="Map" title="Map">
                        <a href="<?php echo get_site_url( 2 ) != '' ? get_site_lang_url( 2 ) : 'https://spb.deveducation.com/'.$url_language; ?>"
                           class="map-pointer pointer-left spb disabled"><?php ett( 'Санкт-Петербург' ); ?></a>
                        <a href="<?php echo get_site_url( 5 ) != '' ? get_site_lang_url( 5 ) : 'https://kyiv.deveducation.com/'.$url_language_uk; ?>"
                           class="map-pointer pointer-right kyiv"><?php ett( 'Киев' ); ?></a>
                        <a href="<?php echo get_site_url( 4 ) != '' ? get_site_lang_url( 4 ) : 'https://dnipro.deveducation.com/'.$url_language_uk; ?>"
                           class="map-pointer pointer-right dnipro"><?php ett( 'Днепр' ); ?></a>
                        <a href="<?php echo get_site_url( 3 ) != '' ? get_site_lang_url( 3 ) : 'https://kharkiv.deveducation.com/'.$url_language_uk; ?>"
                           class="map-pointer pointer-left kharkiv"><?php ett( 'Харьков' ); ?></a>
                        <a href="<?php echo get_site_url( 6 ) != '' ? get_site_lang_url( 6 ) : 'https://baku.deveducation.com/'.$url_language; ?>"
                           class="map-pointer pointer-left baku"><?php ett( 'Баку' ); ?></a>
                        <a href="<?php echo get_site_url( 7 ) != '' ? get_site_lang_url( 7 ) : 'https://kazan.deveducation.com/'.$url_language; ?>"
                           class="map-pointer pointer-left kazan disabled"><?php ett( 'Казань' ); ?></a>
                        <a href="<?php echo get_site_lang_url( 8 ) != '' ? get_site_lang_url( 8 ) : 'https://ekb.deveducation.com/'.$url_language; ?>"
                           class="map-pointer pointer-left ekb disabled"><?php ett( 'Екатеринбург' ); ?></a>
                        <span class="map-pointer pointer-left israel"><?php ett( 'Тель-Авив' ); ?></span>
                    </div>
                </div>
                <div class="about-info">
					<?php $info = get_acf_group( get_the_ID(), 'info' ); ?>
                    <div class="info-title"><?php echo $info->title; ?></div>
                    <div class="info-desc"><?php echo $info->subtitle; ?></div>
                </div>
            </div>
            <!-- ABOUT -->
        </div>
		<?php if ( is_site_subdomain() && get_post_meta( get_the_ID(), 'about_college', true ) ): ?>
			<?php $url = wp_get_attachment_image_url( get_post_meta( get_the_ID(), 'city_image', true ), 'full' ); ?>
            <div class="city-info" data-no-lazy="1" style="background-image: url('<?php echo $url; ?>');">
                <div class="container">
                    <div class="about-college">
						<?php echo get_post_meta( get_the_ID(), 'about_college', true ); ?>
                    </div>
                </div>
            </div>
		<?php endif; ?>
        <div class="statistics-list">
            <div class="container">
				<?php foreach ( get_acf_repeater( get_the_ID(), 'statistics' ) as $stats ): ?>
                    <div class="statistics">
                        <div class="statistics-number"><?php echo $stats->number; ?></div>
                        <div class="statistics-text"><?php echo $stats->text; ?></div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
		<?php $url = wp_get_attachment_image_url( get_post_meta( get_the_ID(), 'inspire_image', true ), 'full' ); ?>
        <div class="inspire" data-no-lazy="1" style="background-image: url('<?php echo $url; ?>');">
            <div class="container">
                <div class="inspire-text">
                    <div class="inspire-title">
						<?php echo get_post_meta( get_the_ID(), 'inspire_title', true ); ?>
                    </div>
                    <div class="inspire-description">
						<?php echo get_post_meta( get_the_ID(), 'inspire_description', true ); ?>
                    </div>
                    <a href="<?php echo get_home_lang_url(); ?>inspirer/"><?php ett( 'Подробнее' ); ?></a>
                </div>
            </div>
        </div>
		<?php $team = get_acf_repeater( get_the_ID(), 'team' ); ?>
        <section class="team">
            <div class="container">
                <h3><?php ett( 'Наша команда' ); ?></h3>
                <div class="top-list">
					<?php foreach (
						array_filter( (array) $team, function ( $m ) {
							return $m->visibility == 'up';
						} ) as $member
					): ?>
                        <div class="teammate">
                            <div class="teammate-image">
                                <?php $image = wp_get_attachment_image_src($member->image, 'thumbnail'); ?>
                                <img src="<?php echo $image[0]; ?>"
                                    data-wdsrcset="<?php echo $image[0]; ?>"
                                    width="<?php echo $image[1]; ?>"
                                    height="<?php echo $image[2]; ?>"
                                    alt="<?php echo $member->full_name; ?>"
                                    title="<?php echo $member->full_name; ?>"/>
                            </div>
                            <div class="teammate-text">
                                <div class="teammate-name"><?php echo $member->full_name; ?></div>
                                <div class="teammate-position"><?php echo $member->position; ?></div>
                                <div class="teammate-desc"><?php echo $member->description; ?></div>
                            </div>
                        </div>
					<?php endforeach; ?>
                </div>
            </div>
            <div class="container-fluid">
                <div class="container">
                    <div class="bottom-list">
						<?php foreach (
							array_filter( (array) $team, function ( $m ) {
								return $m->visibility == 'down';
							} ) as $member
						): ?>
                            <div class="teammate">
								<?php $image = wp_get_attachment_image_src($member->image, 'thumbnail'); ?>
                                <img src="<?php echo  $image[0]; ?>"
                                    data-wdsrcset="<?php echo $image[0]; ?>"
                                    width="<?php echo $image[1]; ?>"
                                    height="<?php echo $image[2]; ?>"
                                    alt="<?php echo $member->full_name; ?>"
                                    title="<?php echo $member->full_name; ?>"/>
                                <div class="teammate-text">
                                    <div class="teammate-name"><?php echo $member->full_name; ?></div>
                                    <div class="teammate-position"><?php echo $member->position; ?></div>
                                    <div class="teammate-desc"><?php echo $member->description; ?></div>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div>
					<?php get_template_part( 'parts/general/join-form' ); ?>
                </div>
            </div>
        </section>
    </main>
    <!-- MAIN -->
<?php get_footer(); ?>