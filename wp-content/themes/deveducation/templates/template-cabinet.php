<?php
/**
 * Template Name: Cabinet Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php
if ( ! is_user_logged_in() ) {
	wp_redirect( '/' );
}

get_header();
?>
    <!-- MAIN -->
    <main>
        <div class="container">
            <embed style="margin: 50px 0; height: calc(100vh); width: 100%;" id="iframe"
                   src="https://itvdn.com/ru/externalvideo?course=dev-education&customer=74f7470f-e233-4b9b-8ea0-d6db6223ec6b&client=vladimirvyg@123software.ru"
                   frameborder="0" mozallowfullscreen webkitallowfullscreen allowfullscreen>
        </div>
    </main>
    <!-- MAIN -->
<?php get_footer(); ?>