<?php
/**
 * Template Name: Glossary Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- PAGE -->
    <main>
        <div class="container">
			<?php echo print_breadcrumbs(); ?>
			<?php $current = 'А'; ?>
			<?php $exists = get_glossary_exists_letters(); ?>
            <div class="glossary">
                <h1><?php ett( 'Глоссарий' ); ?></h1>
                <div class="glossary-desc">
                    В глоссарии объясняется множество терминов, которые вы должны знать для успешного управления
                    проектами. Данный глоссарий включает в себя более
                    50 терминов, и поможет вам быстро найти нужный термин и понять его значение.
                </div>
                <div class="glossary-categories">
					<?php foreach ( get_terms( 'glossary_cat', array( 'hide_empty' => false ) ) as $term ): ?>
                        <a href="<?php echo get_term_link( $term ); ?>"
                           class="glossary-cat"><?php echo $term->name; ?></a>
					<?php endforeach; ?>
                </div>
                <div class="glossary-alphabet">
                    <div class="alphabet-left"></div>
                    <div class="alphabet-letters">
						<?php foreach ( get_cyrillic_alphabet() as $letter ): ?>
                            <div class="glossary-letter<?php echo $current == $letter ? ' active' : ''; ?><?php echo ! in_array( $letter, $exists ) ? ' disabled' : ''; ?>"><?php echo $letter; ?></div>
						<?php endforeach; ?>
                    </div>
                    <div class="alphabet-right"></div>
                </div>
                <div class="glossary-vocabulary">
                    <div class="glossary-words">
						<?php foreach ( get_glossary() as $key => $word ): ?>
							<?php if ( $key == 0 ): ?>
								<?php $preview = $word->ID; ?>
							<?php endif; ?>
                            <a class="glossary-word" href="<?php echo $word->link; ?>"
                               data-id="<?php echo $word->ID; ?>"><?php echo $word->title; ?></a>
						<?php endforeach; ?>
                    </div>
                    <div class="glossary-content">
						<?php if ( isset( $preview ) ): ?>
							<?php $preview = get_glossary_word( $preview ); ?>
						<?php endif; ?>
                        <div class="word-title"><?php echo $preview ? $preview->title : ''; ?></div>
                        <div class="word-content"><?php echo $preview ? $preview->content : ''; ?></div>
                    </div>
                </div>
            </div>
        </div>
		<?php get_template_part( 'parts/general/glossary-join-form' ); ?>
    </main>
    <!-- PAGE -->
<?php get_footer(); ?>