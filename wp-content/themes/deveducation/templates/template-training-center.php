<?php
/**
 * Template Name: Training Center
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- PAGE -->
    <main>
        <div class="container">
			<?php echo print_breadcrumbs(); ?>
            <div class="training-center">
                <h1><?php the_title(); ?></h1>
                <div class="training-center-content">
                    <a href="/webinars/" class="training-center-block webinars">
                        <h3><?php ett( 'Вебинары' ); ?></h3>
                        <p><?php ett( 'Вы найдете множество интересных вебинаров, с помощью которых сможете развить свои soft и hard
                            skills' ); ?></p>
                    </a>
                    <a href="/blog/" class="training-center-block blog">
                        <h3><?php ett( 'Блог' ); ?></h3>
                        <p><?php ett( 'Интересные и полезные статьи по программированию и разработке для IT-специалистов' ); ?></p>
                    </a>
                    <a href="/glossary/" class="training-center-block glossary">
                        <h3><?php ett( 'Глоссарий' ); ?></h3>
                        <p><?php ett( 'Данный глоссарий включает в себя более 50 терминов, и поможет вам быстро найти нужный термин и понять его значение.' ); ?></p>
                    </a>
                </div>
            </div>
        </div>
		<?php get_template_part( 'parts/general/glossary-join-form' ); ?>
    </main>
    <!-- PAGE -->
<?php get_footer(); ?>