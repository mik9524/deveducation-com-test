<?php
/**
 * Template Name: Courses Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- MAIN -->
    <main>
        <div class="container">
			<?php print_breadcrumbs(); ?>
			<?php if ( is_site_subdomain() ): ?>
                <div class="our-courses">
                    <h1><?php the_title(); ?></h1>
                    <div class="our-courses-list">
						<?php $courses = get_posts( array( 'post_type' => 'courses', 'numberposts' => - 1 ) ); ?>
						<?php foreach ( $courses as $course ): ?>
							<?php $bg = wp_get_attachment_image_url( get_post_meta( $course->ID, 'courses_background', true ) ); ?>
							<?php $icon = wp_get_attachment_image_url( get_post_meta( $course->ID, 'courses_icon', true ) ); ?>
                            <a href="<?php echo get_permalink( $course->ID ); ?>"
                               style="background-image: url(<?php echo $icon; ?>), url(<?php echo $bg; ?>)">
                                <div class="course-title"><?php echo $course->post_title; ?></div>
                                <div class="course-desc"><?php echo $course->post_content; ?></div>
                            </a>
						<?php endforeach; ?>
                    </div>
                </div>
			<?php else: ?>
                <!-- COURSES -->
                <div class="nearest-courses">
                    <h1><?php echo get_the_title(); ?></h1>
                    <div class="nearest-courses-list">
						<?php $message = tt( 'В ближайшее время курсов нет' ); ?>
						<?php foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ): ?>
							<?php switch_to_blog( $site->blog_id ); ?>

							<?php $bg = wp_get_attachment_image_url( get_option( 'options_background' ) ); ?>
							<?php $icon = wp_get_attachment_image_url( get_option( 'options_icon' ) ); ?>
                            <div data-site-id="<?php echo get_current_blog_id(); ?>">
                                <a href="<?php echo get_site_lang_url( $site->blog_id ); ?>courses/"
                                   class="nearest-courses-city"
                                   style="background-image: url(<?php echo $icon; ?>), url(<?php echo $bg; ?>)">
									<?php ett( $site->blogname, true ); ?>
                                </a>
								<?php $nearest = get_nearest_courses(); ?>
								<?php if ( count( $nearest ) ): ?>
									<?php foreach ( $nearest as $course ): ?>
										<?php $alternative = get_post_meta( $course->ID, 'courses_alternative_date', true ); ?>
                                        <a href="<?php echo get_home_url(); ?>/courses/<?php echo $course->post_name; ?>/"
                                           class="nearest-course">
                                            <div class="course-title"><?php echo $course->post_title; ?></div>
											<?php if ( ! empty( $alternative ) ): ?>
                                                <div class="course-time"><?php echo $alternative; ?></div>
											<?php else: ?>
                                                <div class="course-time"><?php echo convert_acf_date( get_post_meta( $course->ID, 'courses_date', true ) ); ?></div>
											<?php endif; ?>
                                        </a>
									<?php endforeach; ?>
								<?php else: ?>
                                    <span><?php echo $message; ?></span>
								<?php endif; ?>
                            </div>
							<?php restore_current_blog(); ?>
						<?php endforeach; ?>
                    </div>
                </div>
                <!-- COURSES -->
			<?php endif; ?>
        </div>
        <section class="courses-form">
            <div class="container">
                <h2><?php ett( 'Заполни форму и получи грант на обучение в ближайшем потоке курса!' ); ?></h2>
                <form data-ajax-action="sign_course">
					<?php wp_nonce_field( - 1, 'grant' ); ?>
                    <input type="text" name="full_name" placeholder="<?php ett( 'ФИО' ); ?>" required>
                    <div class="custom-select">
                        <select class="choose-city" name="city_id" required>
                            <option <?php echo ! is_site_subdomain() ? ' selected' : ''; ?> disabled
                                                                                            value=""><?php ett( 'Выберите город' ); ?></option>
							<?php foreach ( get_sites( array( 'site__not_in' => array( get_main_site_id() ) ) ) as $site ): ?>
                                <option value="<?php echo $site->blog_id; ?>"<?php echo get_current_blog_id() == $site->blog_id ? ' selected' : ''; ?>><?php ett( $site->blogname ); ?></option>
							<?php endforeach; ?>
                        </select>
                    </div>
                    <input type="email" name="email" placeholder="Email" required>
                    <input class="phone-mask" type="text" name="phone" placeholder="<?php ett( 'Телефон' ); ?>"
                           required>
                    <div class="custom-select">
                        <select class="courses-list" name="course" required>
                            <option selected disabled value=""><?php ett( 'Выберите курс' ); ?></option>
							<?php foreach (
								get_posts( array(
									'post_type'   => 'courses',
									'numberposts' => - 1
								) ) as $course
							): ?>
                                <option value="<?php echo $course->post_title; ?>"><?php echo $course->post_title; ?></option>
							<?php endforeach; ?>
                        </select>
                    </div>
                    <button><?php ett( 'Отправить' ); ?></button>
                </form>
            </div>
        </section>
        <div class="reviews">
            <div class="container">
                <div class="reviews-title"><?php ett( 'Отзывы студентов' ); ?></div>
                <div class="splide reviews-list">
                    <div class="splide__track">
                        <ul class="splide__list">
							<?php foreach ( get_reviews( 'reviews' ) as $review ): ?>
                                <li class="splide__slide review">
									<?php $size = array( 0, 200 ); ?>
                                    <div class="review-image"
                                         data-link="<?php echo get_post_meta( $review->ID, 'video', true ); ?>"><img
                                                src="<?php echo get_the_post_thumbnail_url( $review, $size ); ?>"
                                                height="200"
                                                alt="<?php echo $review->post_title; ?>"></div>
                                    <div class="review-name"><?php echo $review->post_title; ?></div>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<?php if ( is_site_subdomain() ): ?>
            <section class="have-questions courses-questions">
                <div class="container">
                    <h2><?php ett( 'Онлайн проверка сертификата' ); ?></h2>
                    <button class="button button-action"
                            data-popup="check-certificate"><?php ett( 'Проверить' ); ?></button>
                </div>
            </section>
		<?php else: ?>
            <section class="have-questions courses-questions">
                <div class="container">
                    <h2><?php ett( 'Остались вопросы? Задайте их нам.' ); ?></h2>
                    <span><?php ett( 'Станьте востребованным разработчиком с DevEducation!' ); ?></span>
                    <button class="button button-action"
                            data-popup="ask-question"><?php ett( 'Задать вопрос' ); ?></button>
                </div>
            </section>
		<?php endif; ?>
    </main>
    <!-- MAIN -->
    <!-- POPUP -->
    <div class="popup-overlay" id="ask-question">
        <div class="popup">
            <div class="popup-close"></div>
            <div class="popup-title"><?php ett( 'Задать вопрос' ); ?></div>
            <div class="popup-content">
                <form data-ajax-action="ask_question">
					<?php wp_nonce_field( - 1, 'ask_question' ); ?>
                    <input type="text" placeholder="<?php ett( 'ФИО' ); ?>" name="full_name" required>
                    <input class="phone-mask" type="text" placeholder="<?php ett( 'Телефон' ); ?>" name="phone"
                           required>
                    <input type="text" placeholder="<?php ett( 'Задайте ваш вопрос' ); ?>" name="question" required>
                    <div class="popup-result"></div>
                    <button class="button button-action"><?php ett( 'Отправить' ); ?></button>
                </form>
            </div>
        </div>
    </div>
    <!-- POPUP -->
    <!-- POPUP -->
    <div class="popup-overlay" id="check-certificate">
        <div class="popup">
            <div class="popup-close"></div>
            <div class="popup-title"><?php ett( 'Онлайн проверка сертификата' ); ?></div>
            <div class="popup-content">
                <form data-ajax-action="check_certificate">
					<?php wp_nonce_field( - 1, 'certificate' ); ?>
                    <input type="text" name="certificate_number"
                           placeholder="<?php ett( 'Номер сертификата через пробел' ); ?>" required>
                    <input type="text" name="last_name" placeholder="<?php ett( 'Фамилия' ); ?>" required>
                    <div class="certificate-status"></div>
                    <button class="button button-action"><?php ett( 'Проверить' ); ?></button>
                </form>
            </div>
        </div>
    </div>
    <!-- POPUP -->
<?php get_footer(); ?>