<?php
/**
 * Template Name: Contacts Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- MAIN -->
    <main>
		<?php if ( is_site_subdomain() && ! is_offline_school() ): ?>
            <div class="container">
				<?php print_breadcrumbs(); ?>
            </div>
			<?php $contact = get_contacts_data(); ?>
            <div class="container-fluid contacts-subdomain">
                <embed data-src="<?php echo $contact->google_embeded; ?>" frameborder="0" style="width: 100%"
                       height="450">
                <div class="container">
                    <div class="contact-info">
                        <div class="city-title"><h1><?php ett( 'Наши контакты' ); ?></h1></div>
                        <div class="city-address"><?php echo $contact->address; ?></div>
                        <div class="city-phone"><?php echo str_replace( PHP_EOL, '<br>', $contact->phone ); ?></div>
                        <div class="city-email"><?php echo $contact->email; ?></div>
                    </div>
                </div>
            </div>
		<?php else: ?>
            <div class="container contacts-origin">
				<?php print_breadcrumbs(); ?>
                <!-- CONTACTS -->
                <div class="contacts">
                    <h1><?php echo get_the_title(); ?></h1>
					<?php $contacts = get_acf_repeater( get_the_ID(), 'contacts' ); ?>
                   
                    <?php 
                    $i = 0; 
                    $style = '';
                    foreach ( $contacts as $key => $contact ): ?>
                        <input type="radio" name="select-city" id="select-city_<?php echo $i; ?>" class="select-city-input " <?php echo $i == 0 ? 'checked' : ''; ?>>
                        <label for="select-city_<?php echo $i; ?>" class="select-city-label"><?php echo $contact->city; ?></label>
                    <?php
                        $style .= '#select-city_'.$i.':checked ~ .contact-info .selected-city_'.$i.' {display:flex}';
                        $i++; 
                        endforeach;
                    ?>
                    
                    <div class="contact-info" data-style="<?php echo $style; ?>">
					<?php $i = 0; foreach ( $contacts as $contact ): ?>
                        <div class="selected-city_<?php echo $i; ?> contact-block" >
                            <div class="contact-info">
                                <div class="city-title"><?php echo $contact->city; ?></div>
                                <div class="city-address"><?php echo $contact->address; ?></div>
                                <div class="city-phone"><?php echo str_replace( PHP_EOL, '<br>', $contact->phone ); ?></div>
                                <div class="city-email"><?php echo $contact->email; ?></div>
                            </div>
                            <div class="contact-map">
                                <embed data-src="<?php echo $contact->google_embeded; ?>" frameborder="0"
                                      >
                            </div>
                        </div>
					<?php $i++; endforeach; ?>
                    </div>
                </div>
                <!-- CONTACTS -->
            </div>
		<?php endif; ?>
        <section class="have-questions">
            <div class="container">
                <h2><?php ett( 'Остались вопросы? Задайте их нам.' ); ?></h2>
                <div class="causes">
                    <div class="cause">
						<?php ett( 'Если у вас есть желание изменить свою жизнь в лучшую сторону.' ); ?>
                    </div>
                    <div class="cause">
						<?php ett( 'Если вы хотите развиваться и познавать новое.' ); ?>
                    </div>
                    <div class="cause">
						<?php ett( 'Если вам необходимо ощущение покорения новых возможностей.' ); ?>
                    </div>
                </div>
                <button class="button button-action form-button" data-popup="ask-question"><?php ett( 'Задать вопрос' ); ?></button>
            </div>
        </section>
    </main>
    <!-- MAIN -->
    <!-- POPUP -->
    <div class="popup-overlay" id="ask-question">
        <div class="popup">
            <div class="popup-close"></div>
            <div class="popup-title"><?php ett( 'Задать вопрос' ); ?></div>
            <div class="popup-content">
                <form data-ajax-action="ask_question">
					<?php wp_nonce_field( - 1, 'ask_question' ); ?>
                    <input type="text" placeholder="<?php ett( 'ФИО' ); ?>" name="full_name" required>
                    <input class="phone-mask" type="text" placeholder="<?php ett( 'Телефон' ); ?>" name="phone"
                           required>
                    <input type="text" placeholder="<?php ett( 'Задайте ваш вопрос' ); ?>" name="question" required>
                    <div class="popup-result"></div>
                    <button class="button button-action form-button"><?php ett( 'Отправить' ); ?></button>
                </form>
            </div>
        </div>
    </div>
    <!-- POPUP -->
<?php get_footer(); ?>