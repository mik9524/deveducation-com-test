<?php
/**
 * Template Name: Graduates Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- MAIN -->
    <main>
        <div class="container">
			<?php print_breadcrumbs(); ?>
        </div>
        <div class="graduates">
            <div class="container">
                <h1><?php echo get_the_title(); ?></h1>
                <div class="graduates-list splide" <?php echo is_offline_school() ? 'data-count="3"' : '';?>>
                    <div class="splide__track">
                        <ul class="splide__list">
							<?php foreach ( get_reviews( 'graduates' ) as $graduate ): ?>
                                <li class="splide__slide">
                                    <figure data-link="<?php echo get_post_meta( $graduate->ID, 'video', true ); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url( $graduate->ID, array(
											0,
											200
										) ); ?>" width="405" height="230" alt="<?php echo $graduate->post_title; ?>">
                                        <figcaption>
                                            <div class="graduate-name"><?php echo $graduate->post_title; ?></div>
                                            <div class="graduate-position"><?php echo get_post_meta( $graduate->ID, 'position', true ); ?></div>
                                            <div class="graduate-desc"><?php echo $graduate->post_content; ?></div>
                                        </figcaption>
                                    </figure>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </div>
                </div>
				<?php get_template_part( 'parts/general/join-form' ); ?>
            </div>
        </div>
    </main>
    <!-- MAIN -->
<?php get_footer(); ?>