<?php
/**
 * Template Name: Home Origin
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */

get_header( null, array( 'transparent' => true ) );
$nearest = get_nearest_courses();
$events  = get_posts( array( 'post_type' => 'events', 'numberposts' => 8 ) );
?>
<!-- MAIN -->
<main>
	<?php get_template_part( 'parts/front-page/first-screen' ) ?>
	<?php get_template_part( 'parts/front-page/neares-courses' ) ?>
	<?php get_template_part( 'parts/front-page/grant-chance' ) ?>
	<?php get_template_part( 'parts/front-page/mountains' ) ?>
	<?php get_template_part( 'parts/front-page/career-steps' ) ?>
	<?php get_template_part( 'parts/front-page/graduates' ) ?>
	<?php get_template_part( 'parts/front-page/article-block' ) ?>
	<?php get_template_part( 'parts/front-page/online-school' ) ?>
	<?php get_template_part( 'parts/front-page/partners' ) ?>
	<?php get_template_part( 'parts/front-page/bottom-screen' ) ?>
</main>
<!-- MAIN -->
<?php get_footer(); ?>
