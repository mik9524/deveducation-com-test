<?php
/**
 * Template Name: Home Subdomain
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */

 // offline school class
if ( is_offline_school() ) {
	$offline_class = 'offline';
} else {
	$offline_class = '';
}

// current language
$current_language = pll_current_language() == 'ru' ? '' : pll_current_language().'/';

get_header( null, array( 'transparent' => true ) ); ?>
	<!-- MAIN -->
	<main>
		<section class="first-screen video-bg">
			<video preload="auto" loop="" muted="" poster="<?php echo ASSETS_URI; ?>/img/onload.jpg"
				   class="fullscreen-bg__video">
				<source src="#" data-src="<?php echo wp_get_attachment_url( get_post_meta( get_the_ID(), 'video', true ) ); ?>"
						type="video/mp4">
				<source src="<?php echo ASSETS_URI; ?>/img/header__bg-video.jpg">
			</video>
			<div class="container">
				<div class="first-screen__content <?php echo $offline_class; ?>">
					<div class="first-screen__info">
						<?php if ( get_post_meta( get_the_ID(), 'has_courses_title', true ) ): ?>
							<h2><?php echo get_post_meta( get_the_ID(), 'title', true ); ?></h2>
						<?php else: ?>
							<h1><?php echo get_post_meta( get_the_ID(), 'title', true ); ?></h1>
						<?php endif; ?>
						<div class="first-screen__text"><?php echo get_post_meta( get_the_ID(), 'subtitle', true ); ?></div>
					</div>
					<?php if ( is_offline_school() ) : ?>
					<div class="offline-schools-sign">
						<div class="first-screen__text title"><?php echo get_post_meta( get_the_ID(), 'off_school_sign_course_dscr', true );?></div>
						<div class="list">
							<div class="first-screen__cities">
								<div class="first-screen__cities-title"><?php ett( 'Пока вы можете выбрать удобный вам филиал' ) ?>:</div>
								<div class="first-screen__cities-list" data-list-title="<?php ett( 'Филиалы' ) ?>">
									<a href="https://kyiv.deveducation.com/<?php echo $current_language; ?>" class="first-screen__cities-item">
										<span><?php ett( 'Киев' ); ?></span>
									</a>
									<a href="https://dnipro.deveducation.com/<?php echo $current_language; ?>" class="first-screen__cities-item">
										<span><?php ett( 'Днепр' ); ?></span>
									</a>
									<a href="https://kharkiv.deveducation.com/<?php echo $current_language; ?>" class="first-screen__cities-item">
										<span><?php ett( 'Харьков' ); ?></span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<?php else : ?>
					<?php $nearest = get_nearest_courses(); ?>
					<button<?php echo ! count( $nearest ) ? ' disabled' : ''; ?>
							data-popup="sign-course"><?php ett( 'Записаться на курс' ); ?></button>
					<div class="upcoming-courses">
						<div class="block-title"><?php ett( 'Ближайшие курсы' ); ?></div>
						<?php if ( ! count( $nearest ) ): ?>
							<span><?php ett( 'В ближайшее время курсов нет' ); ?></span>
						<?php endif; ?>
						<div class="courses">
							<?php foreach ( $nearest as $course ): ?>
								<div class="course">
									<a href="<?php echo get_permalink( $course->ID ); ?>">
										<?php 
										$alternative = get_post_meta( $course->ID, 'courses_alternative_date', true );
										$vacancy = get_post_meta( $course->ID, 'courses_vacancy', true );
										?>
										<div class="course-title"><?php echo $course->post_title; ?></div>
										<?php if ( empty( $alternative ) ): ?>
											<div class="time"><?php echo convert_acf_date( get_post_meta( $course->ID, 'courses_date', true ) ); ?></div>
											<?php 
											if ($vacancy != '') : ?>
												<div class="vacancy"><span class="vacancy-title"><?php ett('Осталось мест'); ?> </span><span class="vacancy-count"><?php echo $vacancy;?></span></div>
											<?php endif; ?>
										<?php else: ?>
											<div class="time"><?php echo $alternative; ?></div>
										<?php endif; ?>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<aside class="first-screen__aside">
					<?php foreach ( get_acf_repeater( get_the_ID(), 'advantages' ) as $advantage ) { ?>
						<div class="first-screen__aside-item">
							<div class="first-screen__aside-item__image">
								<img src="<?php echo wp_get_attachment_image_url( $advantage->image ); ?>"
									alt="<?php echo $advantage->title; ?>" width="78" height="78">
							</div>
							<div class="first-screen__aside-item__title">
								<?php echo $advantage->title; ?>
							</div>
							<div class="first-screen__aside-item__text">
								<?php echo $advantage->subtitle; ?>
							</div>
						</div>
					<?php } ?>
				</aside>
			</div>
		</section>
		<?php if ($login_back = get_post_meta( get_the_ID(), 'login_background', true)) : 
			$login_img_url = wp_get_attachment_image_url($login_back, 'full');
		?>
		<section class="login-form render-deferred" data-no-lazy="1" style="background-image:url('<?php echo $login_img_url; ?>')">
			<div class="login-form-text">
				<span><?php echo get_post_meta( get_the_ID(), 'login_title', true); ?></span>
				<a id="login-link" href="#" target="_blank" rel="nofollow"><?php ett( 'Вход для сотрудников' ); ?></a>
			</div>
		</section>
		<?php endif; ?>

		<?php get_template_part( 'parts/front-page/grant-chance' ) ?>
		<?php get_template_part( 'parts/front-page/mountains' ) ?>
		<?php get_template_part( 'parts/front-page/career-steps' ) ?>
		
		<div class="reviews render-deferred">
			<div class="container">
				<div class="reviews-title"><?php ett( 'Отзывы студентов' ); ?></div>
				<div class="splide reviews-list">
					<div class="splide__track">
						<ul class="splide__list">
							<?php foreach ( get_reviews( 'reviews' ) as $review ): ?>
								<li class="splide__slide review">
									<?php $size = array( 0, 200 ); ?>
									<?php $thumbnail = get_the_post_thumbnail_url( $review, $size ); ?>
									<?php $sizes = get_image_size( $thumbnail ); ?>
									<div class="review-image"
										 data-link="<?php echo get_post_meta( $review->ID, 'video', true ); ?>">
										<img src="<?php echo $thumbnail; ?>" alt="<?php echo $review->post_title; ?>"
											 width="<?php echo $sizes['width']; ?>"
											 height="<?php echo $sizes['height']; ?>">
									</div>
									<div class="review-name"><?php echo $review->post_title; ?></div>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php if ( is_offline_school() ) { 
			get_template_part( 'parts/front-page/online-school' );
		} ?>
		<div class="partners render-deferred">
			<div class="container">
				<div class="partners-title"><?php ett( 'Наши партнёры' ); ?></div>
				<div class="partners-list-container">
					<div class="partners-list">
						<?php foreach ( get_acf_repeater( get_the_ID(), 'partners' ) as $partner ): ?>
							<?php $logo = wp_get_attachment_image_url( $partner->logo, 'full' ); ?>
							<?php $sizes = get_image_size( $logo, 150, 150 ); ?>
							<a href="<?php echo $partner->link; ?>" target="_blank" rel="nofollow"><img
										src="<?php echo $logo; ?>"
										width="<?php echo $sizes['width']; ?>"
										height="<?php echo $sizes['height']; ?>"
										alt="Partner"></a>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ($faq_back = get_post_meta( get_the_ID(), 'faq_background', true)) {
			$faq_img_url = wp_get_attachment_image_url($faq_back, 'full');
		}
		?>
		<section class="faq-block render-deferred" data-no-lazy="1" style="background-image:url('<?php echo $faq_img_url; ?>')">
			<div class="faq-block-text">
				<h3>FAQ:</h3>
				<span><?php echo get_post_meta( get_the_ID(), 'faq_title', true); ?></span>
				<a href="<?php echo str_replace('crimea.', '', get_site_lang_url( get_main_site_id() ) ); ?>faq/"
				   target="_blank"><?php ett( 'Читать' ); ?></a>
			</div>
		</section>
		<?php 


		if ( is_offline_school() ) {
			// Get events from main domain
			if ( PLL_CURRENT_LANGUAGE == 'ru' ) {
				$pll_lang_id = 6;
				$lang_name_events = 'events/';
			} elseif ( PLL_CURRENT_LANGUAGE == "uk" ) {
				$pll_lang_id = 3;
				$lang_name_events = 'uk/events/';
			} else {
				$pll_lang_id = 0;
			}
			
		
			$events = $pll_lang_id != 0 ? get_posts_by_blog_id(1, $pll_lang_id) : array();
			$domain_url = 'https://deveducation.com/';
			$img_link_prefix = $domain_url.'wp-content/uploads/';
			$custom_events = true;

		} else {
			$events = get_posts( array( 'post_type' => 'events', 'numberposts' => 8 ) );
		} ?>
		<?php if ( pll_current_language() !== 'en' && $events == true &&  count( $events ) ): ?>
			<div class="articles-block render-deferred">
				<div class="container">
					<div class="articles-block-title"><?php ett( 'Новости' ); ?></div>
					<div class="articles-list">
						<?php foreach ( $events as $event ): ?>
							<div class="article">
								<?php 
								if ( isset($custom_events) ) {
									$link = $domain_url.$lang_name_events.$event->post_name.'/';
									$thumbnail = $img_link_prefix.$event->image_link;
									$sizes = [ 'width'=> 300, 'height'=> 200 ];
									$title = preg_replace( '/"(.*)"/', '«$1»', $event->post_title );
									$author_name = $event->display_name;
									$author_nickname = $event->user_nicename;

								} else {
									$link = get_permalink( $event->ID ); 
									$origin = get_post_meta( $event->ID, 'origin', true ); 
									$size = array( 0, '200' );
									$thumbnail = $origin['image'] ?? get_the_post_thumbnail_url( $event->ID, $size ); 
									$sizes = get_image_size( $thumbnail, 360, 220 );
									$title = preg_replace( '/"(.*)"/', '«$1»', $event->post_title );
									$author_name = get_user_name( $origin['author'] ?? $event->post_author );
									$author_nickname = get_user_meta( $origin['author'] ?? $event->post_author, 'nickname', true );
								}
								?>
								<a href="<?php echo $link; ?>" <?php echo is_offline_school() ? 'rel="nofollow"' : ''; ?>>
									<img src="<?php echo $thumbnail; ?>"
										alt="<?php echo $title; ?>"
										width="<?php echo $sizes['width']; ?>"
										height="<?php echo $sizes['height']; ?>"
										data-wdsrcset="<?php echo $thumbnail; ?>">
								</a>
								<div class="article-desc">
									<div class="article-title">
										<a href="<?php echo $link; ?>" <?php echo is_offline_school() ? 'rel="nofollow"' : ''; ?>><?php echo $title; ?></a>
									</div>
									<div class="article-author">
										<span><?php echo $author_name; ?></span>
										<time datetime="<?php echo $event->post_date; ?>"><?php echo date( 'd.m.Y', strtotime( $event->post_date ) ); ?></time>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<a href="<?php echo is_offline_school() ? 'https://kyiv.deveducation.com/'.$current_language.'events/' :  get_post_type_archive_link( 'events' ); ?>"
					class="more-events" <?php echo is_offline_school() ? 'rel="nofollow"' : ''; ?>><?php ett( 'Больше новостей' ); ?></a>
					<?php $title = get_post_meta( get_the_ID(), 'has_courses_title', true ); ?>
					<?php if ( ! empty( $title ) ): ?>
						<div class="has-courses">
							<h1><?php echo $title; ?></h1>
							<div class="has-courses-content">
								<?php echo get_post_meta( get_the_ID(), 'has_courses_description', true ); ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	</main>
<!-- MAIN -->
<?php get_footer(); ?>