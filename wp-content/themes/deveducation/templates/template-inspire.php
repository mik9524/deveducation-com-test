<?php
/**
 * Template Name: Inspire Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- MAIN -->
    <main>
        <div class="container">
			<?php print_breadcrumbs(); ?>
        </div>
        <!-- INSPIRE -->
		<?php $inspirer = get_acf_group( get_the_ID(), 'inspire' ); ?>
        <div class="inspirer">
            <div class="container">
                <div class="inspirer-top">
                    <div class="inspirer-name">
                        <div><?php echo $inspirer->title; ?></div>
                        <h1><?php echo $inspirer->name; ?></h1>
                    </div>
                    <div class="inspirer-image">
                        <img src="<?php echo wp_get_attachment_image_url( $inspirer->preview, 'full' ); ?>" width="600"
                             height="318" alt="Inspirer">
                    </div>
                </div>
                <div class="inspirer-biography">
                    <div class="inspirer-biography-title"><?php ett( 'Биография' ); ?></div>
                    <div class="inspirer-biography-blocks">
                        <div class="inspirer-biography-left">
							<?php $half = intval( count( $inspirer->biography ) / 2 ); ?>
                            <ul>
								<?php for ( $i = 0; $i <= $half; $i ++ ): ?>
                                    <li><?php echo $inspirer->biography[ $i ]->text; ?></li>
								<?php endfor; ?>
                            </ul>
                        </div>
                        <div class="inspirer-biography-right">
                            <ul>
								<?php for ( $i = $half + 1; $i < count( $inspirer->biography ); $i ++ ): ?>
                                    <li><?php echo $inspirer->biography[ $i ]->text; ?></li>
								<?php endfor; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="inspirer-memories">
            <div class="container">
                <div class="inspirer-memories-title"><?php ett( 'Воспоминания' ); ?></div>
				<?php foreach ( $inspirer->memories as $item ): ?>
                    <div class="inspirer-memories-block">
                        <div class="inspirer-memories-content"><?php echo $item->text; ?></div>
                        <div class="inspirer-memories-author"><?php echo $item->author; ?></div>
                        <div class="inspirer-memories-position"><?php echo $item->position; ?></div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
        <!-- INSPIRE -->
    </main>
    <!-- MAIN -->
<?php get_footer(); ?>