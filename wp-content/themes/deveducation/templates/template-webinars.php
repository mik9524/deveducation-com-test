<?php
/**
 * Template Name: Webinars Page
 *
 * @package WizardsDev
 * @subpackage DevEducation
 * @since Dev Education 1.0
 */
?>
<?php get_header(); ?>
    <!-- MAIN -->
    <main>
        <div class="container">
			<?php print_breadcrumbs(); ?>
            <!-- WEBINARS -->
            <div class="webinars">
                <h1><?php ett( 'Вебинары' ); ?></h1>
                <div class="webinars-categories">
                    <div class="webinar-category"><?php ett( 'Все' ); ?></div>
					<?php foreach ( get_terms( array( 'taxonomy' => 'webinars_cat' ) ) as $term ): ?>
                        <div class="webinar-category"
                             data-id="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></div>
					<?php endforeach; ?>
                </div>
                <div class="webinars-list">
					<?php $webinars = get_posts( array( 'post_type' => 'webinars', 'numberposts' => 12 ) ); ?>
					<?php foreach ( $webinars as $webinar ): ?>
                        <figure>
                            <a href="<?php echo get_permalink( $webinar->ID ); ?>">
                                <div class="webinar-preview">
                                    <img src="<?php echo get_post_meta( $webinar->ID, 'youtube_thumbnail', true ); ?>"
                                         alt="<?php echo $webinar->post_title; ?>" width="415" height="200">
                                    <div class="webinar-duration">
										<?php echo get_post_meta( $webinar->ID, 'duration', true ); ?>
                                    </div>
                                </div>
                            </a>
                            <figcaption>
                                <div class="webinar-title"><?php echo $webinar->post_title; ?></div>
                                <div class="webinar-desc"><?php echo limit_text( $webinar->post_content ); ?></div>
                                <div class="webinar-cat"><?php echo wp_get_object_terms( $webinar->ID, 'webinars_cat' )[0]->name; ?></div>
                            </figcaption>
                        </figure>
					<?php endforeach; ?>
                </div>
            </div>
			<?php if ( wp_count_posts( 'webinars' )->publish > 12 ): ?>
                <div class="load-more-webinars"><?php ett( 'Больше вебинаров' ); ?></div>
			<?php endif; ?>
            <!-- WEBINARS -->
        </div>
		<?php get_template_part( 'parts/general/glossary-join-form' ); ?>
    </main>
    <!-- MAIN -->
<?php get_footer(); ?>