<?php get_header(); ?>
    <!-- PAGE -->
    <main>
        <div class="container">
            <?php print_breadcrumbs(); ?>
            <div class="general-page">
                <h1><?php echo get_the_title(); ?></h1>
                <div class="content"><?php echo wpautop(get_the_content()); ?></div>
            </div>
        </div>
    </main>
    <!-- PAGE -->
<?php get_footer(); ?>