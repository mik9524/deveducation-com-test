<?php
/* Plugin Name: Autoptimie Preloader
 * Description: Allow you to preload generated bundle by Autoptimize
 * Author: Rauf Kerimov
 * Version: 1.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_filter( 'redis_cache_content', function ( $content ) {
	if ( preg_match( '/<script[^>]+src="([^>]+autoptimize_[A-z0-9]+\.js)[^<]+<\/script>/s', $content, $js ) ) {
		$preloader = '<link rel="preload" as="script" href="' . $js[1] . '">';
		$content   = str_replace( '<head>', '<head>' . $preloader, $content );
		$content   = str_replace( $js[0], '', $content );
		$content   = str_replace( '</footer>', get_dynamic_loader( $js[1] ) . '</footer>', $content );
	}

	if ( preg_match( '/<link[^>]+href="([^>]+autoptimize_[A-z0-9]+\.css)[^>]+\/>/s', $content, $css ) ) {
		$preloader = '<link rel="preload" as="style" href="' . $css[1] . '">';
		$content   = str_replace( '<head>', '<head>' . $preloader, $content );
	}

	return $content;
} );

function get_dynamic_loader( $js ) {
	$script = '<script>let loaded=false;["click","ontouchstart","mousemove","scroll"].forEach(a=>{document.addEventListener(a,()=>{let a=document.createElement("script");a.src="' . $js . '",a.async=!1;if(!loaded){document.body.append(a);loaded=true;}},{once:!0})});</script>';

	return $script;
}