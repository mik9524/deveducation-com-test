<?php
/* Plugin Name: Lightweight Page Cache with Redis
 * Description: Allow you to cache pages to get high Time to First Byte
 * Author: Rauf Kerimov
 * Version: 1.3.6th
 * Supports: Autoptimize, AMP for WP
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'DISABLE_WP_CRON' ) ) {
	define( 'DISABLE_WP_CRON', true );
}

header( 'Cache-Control: private, no-transform, must-revalidate, max-age=5' );

if ( defined( 'REDIS_ENABLE' ) && REDIS_ENABLE && class_exists( 'Redis' ) ) {
	try {
		$redis = new Redis();
		$redis->connect( REDIS_HOST, REDIS_PORT );
	} catch ( RedisException $e ) {
		return;
	}

	if ( $redis->isConnected() ) {
		if ( is_cacheable() && $key = get_redis_key() ) {
			if ( $redis->exists( $key ) ) {
				$cache   = $redis->get( $key );
				$pattern = '/<!-- ID: (\d+) -->/s';

				if ( preg_match( $pattern, $cache, $post ) && ! headers_sent() ) {
					if ( $time = get_the_modified_date( 'r', $post[1] ) ) {
						$expires = $redis->ttl( $key );

						header( 'Last-Modified: ' . str_replace( '+0000', 'GMT', $time ) );
						header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + $expires ) . ' GMT' );

						if ( isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) && strtotime( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) >= strtotime( $time ) ) {
							$protocol = ( $_SERVER['SERVER_PROTOCOL'] ?? 'HTTP/1.1' );

							header( $protocol . ' 304 Not Modified' );
						}

					}
				}

				do_action( 'redis_before_send' );
				echo preg_replace( $pattern, '', apply_filters( 'redis_cache_content', $cache ) );
				exit;
			}

			add_action( 'init', function () use ( $redis, $key ) {
				ob_start( function ( $buffer ) use ( $redis, $key ) {
					if ( ! is_404() && ! is_feed() && ! is_search() && did_action( 'template_redirect' )
					     && ! empty( $buffer ) && $buffer != strip_tags( $buffer ) ) {
						$expires = is_archive() ? REDIS_EXPIRE_TIME_ARCHIVE : REDIS_EXPIRE_TIME;
						$comment = '<!-- ' . PHP_EOL;
						$comment .= ' * Time: ' . date( 'Y-m-d H:i:s' ) . PHP_EOL;
						$comment .= ' * Expires: ' . date( 'Y-m-d H:i:s', time() + $expires ) . PHP_EOL;
						$comment .= ' * Cached by Lightweight Page Cache v1.3.4th' . PHP_EOL;
						$comment .= '-->';

						if ( ! ( function_exists( 'amp_is_request' ) && amp_is_request() ) ) {
							$buffer = str_replace( '</footer>', "<script>document.addEventListener('scroll', e => fetch('/wp-cron.php'), {once: true});</script></footer>", $buffer );
						}

						$redis->set( $key, '<!-- ID: ' . get_the_ID() . ' -->' . PHP_EOL . $buffer . PHP_EOL . $comment, [
							'nx',
							'ex' => $expires
						] );
					}

					return $buffer;
				} );
			}, - 1 );
		}

		add_action( 'admin_bar_menu', function ( $bar ) {
			$bar->add_menu( array(
				'id'    => 'menu_id',
				'title' => '<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzBwdCIgaGVpZ2h0PSIzMHB0IiB2aWV3Qm94PSIwIDAgMzAgMzAiIHZlcnNpb249IjEuMSI+CjxnIGlkPSJzdXJmYWNlMSI+CjxwYXRoIHN0eWxlPSIgc3Ryb2tlOm5vbmU7ZmlsbC1ydWxlOm5vbnplcm87ZmlsbDpyZ2IoMTAwJSwxMDAlLDEwMCUpO2ZpbGwtb3BhY2l0eToxOyIgZD0iTSAxNS42Mjg5MDYgMTAuNDQxNDA2IEMgMTUuMTIxMDk0IDYuMTIxMDk0IDEyLjYyODkwNiAyLjg1MTU2MiA5LjkyOTY4OCAyLjg1MTU2MiBDIDkuODA4NTk0IDIuODUxNTYyIDkuNjYwMTU2IDIuODUxNTYyIDkuNTM5MDYyIDIuODc4OTA2IEMgNy4zMjAzMTIgMy4xNDg0MzggNS42OTkyMTkgNS42MDkzNzUgNS4zNzEwOTQgOC44NTE1NjIgTCA4LjgyMDMxMiA3Ljg5MDYyNSBDIDguODIwMzEyIDcuODkwNjI1IDguODUxNTYyIDcuODkwNjI1IDguOTEwMTU2IDcuODU5Mzc1IEwgOSA3LjgyODEyNSBMIDkuMDU4NTk0IDcuODI4MTI1IEMgOS4yMzgyODEgNy44MDA3ODEgOS41MTE3MTkgNy43MzgyODEgOS44Mzk4NDQgNy42Nzk2ODggQyA5Ljg3MTA5NCA3LjY3OTY4OCA5Ljg5ODQzOCA3LjY3OTY4OCA5LjkyOTY4OCA3LjY0ODQzOCBMIDkuOTYwOTM4IDcuNjQ4NDM4IEMgOS45NjA5MzggNy42NDg0MzggOS45NjA5MzggNy42NDg0MzggOS45ODgyODEgNy42NDg0MzggQyAxMC44MjgxMjUgNy42MjEwOTQgMTEuNjQwNjI1IDkuMDMxMjUgMTEuODc4OTA2IDEwLjgyODEyNSBDIDEyLjA4OTg0NCAxMi42OTE0MDYgMTEuNTc4MTI1IDE0LjI4MTI1IDEwLjczODI4MSAxNC4zNzEwOTQgQyAxMC4wMTk1MzEgMTQuNDYwOTM4IDkuMzAwNzgxIDEzLjQ0MTQwNiA4Ljk0MTQwNiAxMS45Njg3NSBMIDYuMTQ4NDM4IDEyLjc1IEwgNi4wODk4NDQgMTIuNzUgQyA1LjkxMDE1NiAxMi43ODEyNSA1LjczMDQ2OSAxMi44MDg1OTQgNS41NTA3ODEgMTIuODA4NTk0IEMgNi4zOTA2MjUgMTYuNTMxMjUgOC42NzE4NzUgMTkuMjMwNDY5IDExLjEwMTU2MiAxOS4yMzA0NjkgQyAxMS4yMTg3NSAxOS4yMzA0NjkgMTEuMzcxMDk0IDE5LjIzMDQ2OSAxMS40ODgyODEgMTkuMTk5MjE5IEMgMTQuMzA4NTk0IDE4Ljg3MTA5NCAxNi4xNzE4NzUgMTQuOTQxNDA2IDE1LjYyODkwNiAxMC40NDE0MDYgWiBNIDE1LjYyODkwNiAxMC40NDE0MDYgIi8+CjxwYXRoIHN0eWxlPSIgc3Ryb2tlOm5vbmU7ZmlsbC1ydWxlOm5vbnplcm87ZmlsbDpyZ2IoNjYuNjY2NjY3JSw2Ni42NjY2NjclLDY2LjY2NjY2NyUpO2ZpbGwtb3BhY2l0eToxOyIgZD0iTSA5LjY5MTQwNiAxMC44MDA3ODEgQyAxMC4yODkwNjIgMTAuNjc5Njg4IDEwLjY0ODQzOCAxMC4xMDkzNzUgMTAuNTMxMjUgOS41MTE3MTkgQyAxMC40NDE0MDYgOSA5Ljk2MDkzOCA4LjY0MDYyNSA5LjQ0OTIxOSA4LjY0MDYyNSBDIDkuMzkwNjI1IDguNjQwNjI1IDkuMzAwNzgxIDguNjQwNjI1IDkuMjM4MjgxIDguNjcxODc1IEwgNS40ODgyODEgOS43MTg3NSBDIDUuNDI5Njg4IDkuNzE4NzUgNS4zMzk4NDQgOS43NSA1LjI4MTI1IDkuNzUgQyA1LjAzOTA2MiA5Ljc1IDQuODAwNzgxIDkuNjYwMTU2IDQuNTg5ODQ0IDkuNTExNzE5IEMgNC4zNTE1NjIgOS4zMDA3ODEgNC4xOTkyMTkgOSA0LjE5OTIxOSA4LjY3MTg3NSBMIDQuMTk5MjE5IDQuMDc4MTI1IEMgNC4xOTkyMTkgMy40ODA0NjkgMy43MTg3NSAzIDMuMTIxMDk0IDMgQyAyLjUxOTUzMSAzIDIuMDM5MDYyIDMuNDgwNDY5IDIuMDM5MDYyIDQuMDc4MTI1IEwgMi4wMzkwNjIgOC42NDA2MjUgQyAyLjAzOTA2MiA5LjYyODkwNiAyLjQ4ODI4MSAxMC41NTg1OTQgMy4yMzgyODEgMTEuMTYwMTU2IEMgMy44Mzk4NDQgMTEuNjQwNjI1IDQuNTU4NTk0IDExLjkxMDE1NiA1LjMwODU5NCAxMS45MTAxNTYgQyA1LjUxOTUzMSAxMS45MTAxNTYgNS43NjE3MTkgMTEuODc4OTA2IDUuOTY4NzUgMTEuODUxNTYyIFogTSA5LjY5MTQwNiAxMC44MDA3ODEgIi8+CjxwYXRoIHN0eWxlPSIgc3Ryb2tlOm5vbmU7ZmlsbC1ydWxlOm5vbnplcm87ZmlsbDpyZ2IoMTAwJSwxMDAlLDEwMCUpO2ZpbGwtb3BhY2l0eToxOyIgZD0iTSAxMy4zMjAzMTIgMTkuNzY5NTMxIEMgMTMuMjMwNDY5IDI1LjU1ODU5NCAxNC4zNzEwOTQgMjcuODA4NTk0IDE0Ljg1MTU2MiAyOC41IEwgMTYuMDc4MTI1IDI3LjIzODI4MSBMIDE3LjYwOTM3NSAyNy44NzEwOTQgTCAxNy42MDkzNzUgMjcuODM5ODQ0IEwgMTguODM5ODQ0IDI2LjU3ODEyNSBMIDIwLjMwODU5NCAyNy4xNzk2ODggTCAyMS41MzkwNjIgMjUuOTIxODc1IEwgMjMuMDExNzE5IDI2LjUxOTUzMSBMIDI0LjE0ODQzOCAyNS4zNTE1NjIgTCAyNS41ODk4NDQgMjUuNzM4MjgxIEMgMjQuNjAxNTYyIDIzLjI1IDI0LjQ0OTIxOSAxNy4zNzEwOTQgMjQuNDQ5MjE5IDE3LjM3MTA5NCBaIE0gMTMuMzIwMzEyIDE5Ljc2OTUzMSAiLz4KPHBhdGggc3R5bGU9IiBzdHJva2U6bm9uZTtmaWxsLXJ1bGU6bm9uemVybztmaWxsOnJnYigxMDAlLDEwMCUsMTAwJSk7ZmlsbC1vcGFjaXR5OjE7IiBkPSJNIDI3Ljk2MDkzOCA4Ljc4OTA2MiBDIDI3Ljg3MTA5NCA0LjE0MDYyNSAyNC44Mzk4NDQgMi4yODEyNSAyMy40ODgyODEgMS42Nzk2ODggQyAyMy4xMjg5MDYgMS41MzEyNSAyMi43MzgyODEgMS40Njg3NSAyMi4zNTE1NjIgMS41IEwgMTIuMjY5NTMxIDIuNTc4MTI1IEMgMTQuNDI5Njg4IDMuODM5ODQ0IDE2LjE0MDYyNSA2Ljc1IDE2LjU1ODU5NCAxMC4zMjAzMTIgQyAxNi44MjgxMjUgMTIuNjYwMTU2IDE2LjUzMTI1IDE0Ljk2ODc1IDE1LjY2MDE1NiAxNi43Njk1MzEgQyAxNS4zMjgxMjUgMTcuNDg4MjgxIDE0LjkxMDE1NiAxOC4wODk4NDQgMTQuNDYwOTM4IDE4LjYwMTU2MiBMIDI0LjUzOTA2MiAxNi41IEMgMjUuMDUwNzgxIDE2LjM3ODkwNiAyNS41MzEyNSAxNi4xNDA2MjUgMjUuODU5Mzc1IDE1Ljc4MTI1IEMgMjYuNzMwNDY5IDE0Ljg1MTU2MiAyOC4wNTA3ODEgMTIuODA4NTk0IDI3Ljk2MDkzOCA4Ljc4OTA2MiBaIE0gMTguNTcwMzEyIDEzLjA1MDc4MSBMIDE4LjA1ODU5NCAxMy4xNDA2MjUgQyAxNy44MjAzMTIgMTMuMTcxODc1IDE3LjYwOTM3NSAxMy4wMTk1MzEgMTcuNTc4MTI1IDEyLjc4MTI1IEMgMTcuNTUwNzgxIDEyLjUzOTA2MiAxNy42OTkyMTkgMTIuMzI4MTI1IDE3Ljk0MTQwNiAxMi4zMDA3ODEgTCAxOC40NDkyMTkgMTIuMjEwOTM4IEMgMTguNjkxNDA2IDEyLjE3OTY4OCAxOC44OTg0MzggMTIuMzI4MTI1IDE4LjkyOTY4OCAxMi41NzAzMTIgQyAxOC45NjA5MzggMTIuODA4NTk0IDE4LjgwODU5NCAxMy4wMTk1MzEgMTguNTcwMzEyIDEzLjA1MDc4MSBaIE0gMjEuMjY5NTMxIDEyLjcxODc1IEwgMjAuNzYxNzE5IDEyLjgwODU5NCBDIDIwLjUxOTUzMSAxMi44Mzk4NDQgMjAuMzA4NTk0IDEyLjY5MTQwNiAyMC4yODEyNSAxMi40NDkyMTkgQyAyMC4yNSAxMi4yMTA5MzggMjAuMzk4NDM4IDEyIDIwLjY0MDYyNSAxMS45Njg3NSBMIDIxLjE0ODQzOCAxMS44Nzg5MDYgQyAyMS4zOTA2MjUgMTEuODUxNTYyIDIxLjYwMTU2MiAxMiAyMS42Mjg5MDYgMTIuMjM4MjgxIEMgMjEuNjYwMTU2IDEyLjQ4MDQ2OSAyMS40ODA0NjkgMTIuNjkxNDA2IDIxLjI2OTUzMSAxMi43MTg3NSBaIE0gMjMuOTQxNDA2IDEyLjI2OTUzMSBMIDIzLjQyOTY4OCAxMi4zNTkzNzUgQyAyMy4xOTE0MDYgMTIuMzkwNjI1IDIyLjk4MDQ2OSAxMi4yMzgyODEgMjIuOTQ5MjE5IDEyIEMgMjIuOTIxODc1IDExLjc2MTcxOSAyMy4wNzAzMTIgMTEuNTUwNzgxIDIzLjMwODU5NCAxMS41MTk1MzEgTCAyMy44MjAzMTIgMTEuNDI5Njg4IEMgMjQuMDU4NTk0IDExLjM5ODQzOCAyNC4yNjk1MzEgMTEuNTUwNzgxIDI0LjMwMDc4MSAxMS43ODkwNjIgQyAyNC4zMjgxMjUgMTIuMDMxMjUgMjQuMTc5Njg4IDEyLjIzODI4MSAyMy45NDE0MDYgMTIuMjY5NTMxIFogTSAyNi42MDkzNzUgMTEuODUxNTYyIEwgMjYuMTAxNTYyIDExLjk0MTQwNiBDIDI1Ljg1OTM3NSAxMS45Njg3NSAyNS42NDg0MzggMTEuODIwMzEyIDI1LjYyMTA5NCAxMS41NzgxMjUgQyAyNS41ODk4NDQgMTEuMzM5ODQ0IDI1LjczODI4MSAxMS4xMjg5MDYgMjUuOTgwNDY5IDExLjEwMTU2MiBMIDI2LjQ4ODI4MSAxMS4wMTE3MTkgQyAyNi43MzA0NjkgMTAuOTgwNDY5IDI2Ljk0MTQwNiAxMS4xMjg5MDYgMjYuOTY4NzUgMTEuMzcxMDk0IEMgMjcgMTEuNjA5Mzc1IDI2Ljg1MTU2MiAxMS44MjAzMTIgMjYuNjA5Mzc1IDExLjg1MTU2MiBaIE0gMjYuNjA5Mzc1IDExLjg1MTU2MiAiLz4KPC9nPgo8L3N2Zz4K" style="width: 16px;height: 16px;vertical-align:middle; margin-right: 5px;"  alt=""/> Clear cache',
				'href'  => '/wp-admin/?lpcr_flush=true',
			) );
		}, 200 );

		add_action( 'post_updated', function ( $post_id, $value, $old ) use ( $redis ) {
			$redis->del( get_redis_key( $post_id ) );

			if ( function_exists( 'amp_get_permalink' ) ) { // has AMP
				$redis->del( get_redis_key( $post_id, true ) );
			}
		}, 20, 3 );

		if ( is_admin() && isset( $_GET['lpcr_flush'] ) ) {
			$redis->flushAll();

			add_action( 'admin_notices', function () {
				echo '<div class="notice notice-success is-dismissible"><p>Redis Cache successfully flushed</p></div>';
			} );
		}

		/* Autoptimize integrations */
		add_action( 'autoptimize_action_cachepurged', function () use ( $redis ) {
			$redis->flushAll();
		} );
		/* Autoptimize integrations */

		function clean_redis() {
			global $redis;

			$redis->flushAll();
		}

		function clean_redis_by_post( $post_id ) {
			global $redis;

			$redis->del( get_redis_key( $post_id ) );

			if ( function_exists( 'amp_get_permalink' ) ) { // has AMP
				$redis->del( get_redis_key( $post_id, true ) );
			}
		}

		function clean_redis_by_url( $url ) {
			global $redis;

			$redis->del( get_redis_key( null, false, $url ) );
		}
	}
}

function is_cacheable(): bool {
	$urls    = apply_filters( 'redis_urls_exclude', [ 'cabinet/', 'my-account/', 'myaccount/', 'my_account/' ] );
	$cookies = apply_filters( 'redis_cookies_exclude', [ 'wordpress_logged', 'mmEmail', 'mmUser', '_ASPXAUTHAPI' ] );

	foreach ( $urls as $url ) {
		if ( strpos( $_SERVER['REQUEST_URI'], $url ) !== false ) {
			return false;
		}
	}

	foreach ( $_COOKIE as $key => $value ) {
		foreach ( $cookies as $cookie ) {
			if ( strpos( $key, $cookie ) !== false ) {
				return false;
			}
		}
	}

	return ! isset( $_GET['s'] ) ? true : false;
}

function get_redis_key( $post_id = null, $amp = false, $url = '' ): string {
	if ( $post_id === null ) {
		$key = md5( $_SERVER['HTTP_HOST'] . strtok( $url ?: $_SERVER['REQUEST_URI'], '?' ) );
	} elseif ( $amp && function_exists( 'amp_get_permalink' ) ) {
		$key = md5( preg_replace( '(^https?://)', '', amp_get_permalink( $post_id ) ) );
	} else {
		$key = md5( preg_replace( '(^https?://)', '', get_permalink( $post_id ) ) );
	}

	return apply_filters( 'get_redis_key', $key );
}

/* Temporary decision */
if ( ! is_admin() ) {
	function wp_verify_nonce( $nonce, $action = - 1 ): bool {
		return true;
	}
}
/* Temporary decision */