<?php
/* Plugin Name: ARedirect - entry point for website
 * Description: Allows you to process the incorrect URL
 * Author: Rauf Kerimov
 * Version: 1.3.3
 * Supports: AMP for WP
 */

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	return true;
}

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

check_aredirect();

/**
 * @return array correct url
 */
function get_correct_url( $url ): array {
	$result   = array( 'old' => $url, 'status' => 301 );
	$decoded  = parse_url( $url );
	$statuses = array(
		'/\/home\.?(?:php|htm[A-z]?)?\/?$/' => 404,
		'/\/\*\/?/'                         => 404,
		'/^(?!.*(page)).*\/\d+\/?$/'        => 404,
		'/^\/page\/\d+\/?$/'                => 404,
	);
	$replaces = array(
		'/http[^s]/'                        => 'https:',
		'/www\./'                           => '',
		'/\/amp\/?/'                        => '/',
		'/\/feed\/?/'                       => '/',
		'/\/rss\/?/'                        => '/',
		'/\/index\.?(?:php|htm[A-z]?)?\/?/' => '/',
		'/\/{2,}/'                          => '/',
		'/:\//'                             => '://'
	);

	if ( defined( 'AREDIRECT_SUPPORTS' ) && in_array( 'www', AREDIRECT_SUPPORTS ) ) {
		unset( $replaces['/www\./'] );

		if ( strpos( $url, '://www.' ) === false ) {
			$url = str_replace( '://', '://www.', $url );
		}
	}

	if ( defined( 'AREDIRECT_SUPPORTS' ) && in_array( 'amp', AREDIRECT_SUPPORTS ) ) {
		unset( $replaces['/\/amp\/?/'] );
	}

	if ( defined( 'AREDIRECT_SUPPORTS' ) && in_array( 'feed', AREDIRECT_SUPPORTS ) ) {
		unset( $replaces['/\/feed\/?/'] );
	}

	if ( defined( 'AREDIRECT_SUPPORTS' ) && in_array( 'rss', AREDIRECT_SUPPORTS ) ) {
		unset( $replaces['/\/rss\/?/'] );
	}

	$url = preg_replace( array_keys( $replaces ), array_values( $replaces ), case_sensitive( $url ) );

	if ( strpos( $url, $decoded['scheme'] . '://' . $decoded['host'] ) !== false ) { // if protocol and host not changed
		$request = parse_url( $url, PHP_URL_PATH );

		foreach ( $statuses as $preg => $code ) { // handle for other status
			if ( preg_match( $preg, $request ) ) {
				$result['status'] = $code;
				break;
			}
		}
	}

	if ( $result['status'] === 301 ) { // skip 40x pages for adding trailing slash
		if ( is_trailing_permalink() ) {
			if ( ! preg_match( '/\.[a-z]+$/', $url )
			     && strpos( $url, '?' ) === false
			     && ! preg_match( '/\/$/s', $url )
			     && strpos( $url, '/wp-json/' ) === false && ! is_admin() ) {
				$url .= '/';
			}
		} elseif ( ! preg_match( '/\.[a-z]+\/$/', $url ) ) {
			$url = preg_replace( '/^(.*?)\/?\/+$/s', '$1', $url ); // remove trailing slashes
		}
	}

	if ( $result['old'] === $url && $result['status'] === 301 ) { // url not changed
		$result['status'] = 200;
	} elseif ( $result['status'] === 301 ) {
		$result['redirect_to'] = $url;
	}

	return $result;
}

/**
 * @param string $url link to change
 *
 * @return string
 */
function case_sensitive( string $url ): string {
	$url = explode( '?', $url );

	if ( preg_match( '/[A-Z]+/', $url[0] ) ) {
		$url[0] = mb_strtolower( $url[0] );
	}

	return implode( '?', $url );
}

/**
 * @return void
 */

function check_aredirect(): void {
	$correct = get_correct_url( get_current_url() );

	if ( strpos( $_SERVER['REQUEST_URI'], 'newbrand' ) === false && ! is_admin() && $correct['status'] !== 200 ) {
		if ( $correct['status'] === 301 && isset( $correct['redirect_to'] ) ) {
			force_redirect( $correct['redirect_to'], 301, 'ARedirect' );
			exit;
		} elseif ( $correct['status'] === 404 ) {
			add_action( 'wp', 'force_404', - 1 );
		}
	}

	add_action( 'wp', 'pagination_check', - 1 );
	do_action( 'aredirect_correct' ); // url correct, pagination checked
}

/**
 * @return string current url
 */
function get_current_url(): string {
	return $_SERVER['HTTP_X_FORWARDED_PROTO'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

function force_redirect( $url, $status, $xredirect ) {
	header( 'Location: ' . $url, true, $status );
	header( 'X-Redirect-By: ' . $xredirect );
	exit;
}

function force_404() {
	global $wp_query;

	$wp_query->set_404();

	status_header( 404 );
	nocache_headers();
	include( get_query_template( '404' ) );
	exit;
}

function is_trailing_permalink() {
	return preg_match( '/\/$/', get_option( 'permalink_structure' ) );
}


function pagination_check() {
	global $wp_query;

	if ( isset( $wp_query->query['paged'] ) ) {
		if ( is_single() || is_page() || is_singular() ) {
			force_404();
		} elseif ( in_array( $wp_query->query['paged'], array( 0, 1 ) ) ) {
			$url = preg_replace( '/\/page\/\d+\/*/', is_trailing_permalink() ? '/' : '', get_current_url() );
			force_redirect( $url, 301, 'ARedirect' );
			exit;
		}
	}
}