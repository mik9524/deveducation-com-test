<?php

add_action( 'pll_init', function () {
	remove_action( 'pre_get_posts', array( PLL_Integrations::instance()->wpseo, 'before_sitemap' ), 0 );
}, 20 );