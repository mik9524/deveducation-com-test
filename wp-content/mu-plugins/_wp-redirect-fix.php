<?php

remove_action( 'template_redirect', 'maybe_redirect_404' );
add_filter( 'redirect_canonical', function ( $redirect, $request ) {
	if ( strpos( $request, 'page' ) === false ) {
		return false;
	}

	return $redirect;
}, - 1, 2 );