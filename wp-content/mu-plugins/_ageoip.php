<?php
/* Plugin Name: GeoRedirect
 * Description: Redirects to the site section based on language
 * Author: Rauf Kerimov
 * Version: 1.0.0
 */
add_action( 'aredirect_correct', function () {
	if ( is_main_site() ) {
		$list = array(
			'/',
			'/courses/',
			'/faq/',
			'/about/',
			'/graduates/',
			'/contacts/'
		);

		$lang = get_country_lang();

		if ( ! isset( $_COOKIE['pll_language'] ) && in_array( $_SERVER['REQUEST_URI'], $list ) && $lang !== 'ru' ) {
			setcookie( 'pll_language', $lang );
			force_redirect( "/$lang$_SERVER[REQUEST_URI]", 301, 'GeoRedirect' );
		}
	}
} );

function get_country_lang() {
	$slug      = 'en';
	$countries = array( 'RU' => 'ru', 'UA' => 'uk', 'BY' => 'ru', 'AZ' => 'az' );

	if ( isset( $_SERVER['HTTP_CF_IPCOUNTRY'] ) && isset( $countries[ $_SERVER['HTTP_CF_IPCOUNTRY'] ] ) ) {
		$slug = $countries[ $_SERVER['HTTP_CF_IPCOUNTRY'] ];
	}

	return $slug;
}