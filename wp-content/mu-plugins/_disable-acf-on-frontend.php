<?php
/**
 * Удаляем плагин ACF во фронтенде
 *
 * @param array $plugins Массив всех плагинов
 *
 * @return array
 */
function disable_acf_on_front( $plugins ) {
	if ( is_admin() || wp_is_json_request() || wp_is_jsonp_request() ) {
		return $plugins;
	}

	foreach ( $plugins as $key => $plugin ) {
		if ( 'advanced-custom-fields-pro/acf.php' === $plugin ) {
			unset( $plugins[ $key ] );
		}
	}

	return $plugins;
}

add_filter( 'option_active_plugins', 'disable_acf_on_front' );