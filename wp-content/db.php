<?php

if ( is_offline_school() ) { // using another database
	$wpdb = new wpdb( DB_USER, DB_PASSWORD, DB_NAME, DB_HOST . '-mu' );
}

function is_offline_school(): bool {
	$online = array( 'spb', 'kyiv', 'kharkiv', 'dnipro', 'baku', 'kazan', 'ekb' );
	$host   = explode( '.', str_replace('www.', '', $_SERVER['HTTP_HOST'] ) );

	if ( count( $host ) > 2 && ! in_array( $host[0], $online ) ) { // subdomain
		return true;
	}

	return false;
}