const {watch, parallel, series, src, dest} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify');
const gcmq = require('gulp-group-css-media-queries');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const webpack = require('webpack-stream');
const named = require('vinyl-named');

const destination = {
    css: {
        from: 'src/css/out/*.sass',
        to: '../wp-content/themes/deveducation/assets/css',
        watch: 'src/css/**/*.sass'
    },
    js: {
        from: 'src/js/out/*.js',
        to: '../wp-content/themes/deveducation/assets/js'
    },
    fonts: {
        from: 'src/fonts/**',
        to: '../wp-content/themes/deveducation/assets/fonts',
    },
    images: {
        from: 'src/img/**',
        to: '../wp-content/themes/deveducation/assets/img',
    },
    wp: {
        from: '../wp-content/uploads/**',
        to: '../wp-content/uploads'
    }
};

function generateCSS() {
    return src(destination.css.from).pipe(sass().on('error', sass.logError)).pipe(gcmq()).pipe(postcss([autoprefixer(), cssnano()])).pipe(dest(destination.css.to));
}

function generateJS() {
    return src(destination.js.from).pipe(named()).pipe(webpack({mode: 'production', output: {filename: '[name].js'}})).pipe(uglify()).pipe(dest(destination.js.to));
    // return src(destination.js.from).pipe(concat('main.js')).pipe(webpack()).pipe(babel({presets: ['@babel/env']}))..pipe(dest(destination.js.to));
}

function copyImages() {
    return src(destination.images.from).pipe(dest(destination.images.to))
}

function copyFonts() {
    return src(destination.fonts.from).pipe(dest(destination.fonts.to));
}

function optimizeUploads() {
    return src(destination.wp.from).pipe(image()).pipe(dest(destination.wp.to));
}

exports.css = series(parallel(generateCSS, copyFonts, generateJS))
exports.build = series(parallel(generateCSS, generateJS, copyImages, copyFonts));

exports.watch = function () {
    watch(destination.css.watch, generateCSS)
    watch(destination.js.from, generateJS)
    watch(destination.images.from, copyImages)
    watch(destination.fonts.from, copyFonts)
}