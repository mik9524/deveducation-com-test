export const header = () => {
    const header = document.querySelector('header')
    const langSelector = header.querySelector('.lang-selector')
    const mobileMenuButton = header.querySelector('.mobile-menu')
    const body = document.querySelector('body')
    const headerSocialLink = document.querySelector('.header-social-links')
    const nav = header.querySelector('nav')
    const container = header.querySelector('.container')
    const coursesItems = header.querySelector('.courses-items')
    const coursesLink = header.querySelector('.courses-items a')
    const coursesCopyLink = coursesLink.cloneNode(true)
    const dropdownMenu = coursesItems.querySelector('.dropdown-menu')

    let langShow = false
    let menuShow = false
    let mobileHeader = false

    const overlay = document.createElement('div')
    overlay.classList.add('lang-overlay')

    let coursesElement = document.createElement('li')
    coursesElement.classList.add('courses-link')
    coursesElement.insertAdjacentElement('afterbegin', coursesCopyLink)

    let coursesHeader = document.createElement('li')
    coursesHeader.classList.add('courses-item-header')
    coursesHeader.insertAdjacentHTML('afterbegin', '<span class="arrow-back"></span>')
    coursesHeader.insertAdjacentHTML('beforeend', `<span class="courses-item-title">${coursesLink.textContent}</span>`)

    if (window.innerWidth <= 1366) {
        mobileHeader = true
        window.requestAnimationFrame(() => {
            nav.insertAdjacentElement('beforeend', headerSocialLink);
            dropdownMenu.insertAdjacentElement('afterbegin', coursesElement)
            dropdownMenu.insertAdjacentElement('afterbegin', coursesHeader)
            headerSocialLink.style.display = 'flex';
        });
    }

    overlay.addEventListener('click', function () {
        if (langShow) {
            langSelector.classList.remove('langShow')
            mobileMenuButton.classList.remove('opened')
            body.classList.remove('scroll-disabled')
            header.classList.remove('opened')
            overlay.remove()
            langShow = false
        }
    })

    header.addEventListener('click', (e) => {
        if (e.target.classList.contains('langCurrent') && mobileHeader) {
            if (!langShow && !menuShow) {
                langSelector.classList.add('langShow')
                mobileMenuButton.classList.add('opened')
                body.classList.add('scroll-disabled')
                header.classList.add('opened')
                langShow = true
                header.insertAdjacentElement('afterend', overlay)
            }

            if (!langShow && menuShow) {
                header.classList.remove('showMenu')
                header.classList.add('opened')
                langSelector.classList.add('langShow')
                removeOpened('.has-arrow')
                langShow = true
                menuShow = false
                header.insertAdjacentElement('afterend', overlay)
            }
        }

        if (e.target.classList.contains('mobile-menu') && mobileHeader || e.target.closest('.mobile-menu') && mobileHeader) {
            if (langShow && !menuShow) {
                langSelector.classList.remove('langShow')
                mobileMenuButton.classList.remove('opened')
                body.classList.remove('scroll-disabled')
                header.classList.remove('opened')
                langShow = false
                overlay.remove()
            } else if (!langShow && !menuShow) {
                mobileMenuButton.classList.add('opened')
                body.classList.add('scroll-disabled')
                header.classList.add('showMenu')
                menuShow = true
            } else if (!langShow && menuShow) {
                mobileMenuButton.classList.remove('opened')
                body.classList.remove('scroll-disabled')
                header.classList.remove('showMenu')
                header.classList.remove('coursesOpened')
                removeOpened('.has-arrow')
                menuShow = false
            }
        }

        if (e.target.classList.contains('has-arrow') && mobileHeader) {
            e.target.classList.toggle('opened')
        } else if (e.target.closest('.courses-item') && mobileHeader && !e.target.closest('.sub-menu')) {
            e.preventDefault()
            e.target.parentNode.classList.toggle('opened')
        }

        if (e.target.classList.contains('courses-items') && mobileHeader) {
            header.classList.add('coursesOpened')
        } else if (e.target.closest('.courses-items') && mobileHeader && !e.target.closest('.courses-link') && !e.target.closest('.sub-menu')) {
            e.preventDefault()
            header.classList.add('coursesOpened')
        }

        if (e.target.classList.contains('arrow-back') && mobileHeader) {
            header.classList.remove('coursesOpened')
        }
    })

    window.addEventListener('resize', function () {
        if (window.innerWidth <= 1366) {
            mobileHeader = true
            nav.insertAdjacentElement('beforeend', headerSocialLink)
            dropdownMenu.insertAdjacentElement('afterbegin', coursesElement)
            dropdownMenu.insertAdjacentElement('afterbegin', coursesHeader)
            headerSocialLink.style.display = 'flex';
        } else {
            window.requestAnimationFrame(() => container.insertAdjacentElement('beforeend', headerSocialLink));
            mobileMenuButton.classList.remove('opened')
            body.classList.remove('scroll-disabled')
            header.classList.remove('showMenu')
            header.classList.remove('coursesOpened')
            removeOpened('.has-arrow')
            removeElement()
            mobileHeader = false
            menuShow = false
        }
    })

    function removeOpened(itemClass, className = 'opened') {
        document.querySelectorAll(itemClass).forEach(item => {
            if (item.classList.contains(className)) {
                item.classList.remove(className)
            }
        })
    }

    function removeElement() {
        if (document.querySelector('.courses-link')) {
            document.querySelector('.courses-link').remove()
        }
        if (document.querySelector('.courses-item-header')) {
            document.querySelector('.courses-item-header').remove()
        }
    }

    /* END MOBILE MENU */

    if (document.querySelector('.first-screen__cities')) {
        const citiesList = document.querySelector('.first-screen__cities-list')
        let chooseCities = document.createElement('div')

        chooseCities.classList.add('first-screen__cities-chooseItem')

        chooseCities.textContent = citiesList.dataset.listTitle

        if (window.innerWidth <= 576) {
            window.requestAnimationFrame(() => citiesList.insertAdjacentElement('beforebegin', chooseCities))
        }

        document.querySelector('.first-screen__cities').addEventListener('click', function (e) {
            if (e.target.classList.contains('first-screen__cities-chooseItem')) {
                citiesList.classList.toggle('showList')
                e.target.classList.toggle('showList')
            }
        })

        window.addEventListener('resize', function () {
            if (window.innerWidth <= 576) {
                window.requestAnimationFrame(() => citiesList.insertAdjacentElement('beforebegin', chooseCities))
            } else {
                removeOpened('.showList', 'showList')
            }
        })
    }
}