export const header = () => {
    document.querySelector('.mobile-menu').addEventListener('click', () => {
        document.querySelector('body').classList.toggle('scroll-disabled');
        document.querySelector('header').classList.toggle('opened');
    });

    document.querySelectorAll('.header-nav .dropdown-arrow').forEach(el => {
        el.addEventListener('click', e => {
            e.target.classList.toggle('opened');
            e.target.nextElementSibling.toggleDisplay();
        });
    });

    if (document.querySelector('.courses-menu')) {
        document.querySelector('.courses-menu').addEventListener('click', () => {
            document.querySelector('.header-courses').toggleDisplay();
        });
    }
}
