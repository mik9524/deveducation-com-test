import IMask from "imask";
import {showEmbed} from "../modules/_showEmbed";

export const main = () => {
    const loginLink = 'https://corporate.deveducation.com/',
        ajaxLink = '/wp-admin/admin-ajax.php',
        thanksLink = '/lp/course-v1/thank-you-page.html';

    if (document.querySelector('.phone-mask')) {
        if (location.host.includes('spb') || location.host.includes('kazan') || location.host.includes('ekb')) {
            document.querySelectorAll('.phone-mask').forEach(el => IMask(el, {mask: '+{7} 000 000-00-00'}));
        } else if (location.host.includes('baku')) {
            document.querySelectorAll('.phone-mask').forEach(el => IMask(el, {mask: '+{9}{9}{4} 00 000-00-00'}));
        } else if (location.host.includes('kharkiv') || location.host.includes('kyiv') || location.host.includes('dnipro')) {
            document.querySelectorAll('.phone-mask').forEach(el => IMask(el, {mask: '+{3}{8}{0} 00 000-00-00'}));
        } else if (document.getElementsByTagName('body')[0].classList.contains('offline-school')) {
            document.querySelectorAll('.phone-mask').forEach(el => IMask(el, {mask: '+{3}{8}{0} 00 000-00-00'}));
        } else {
            document.querySelectorAll('.phone-mask').forEach(el => {
                el.setAttribute('maxlength', '20');
                el.addEventListener('keyup', e => {
                    e.target.value = e.target.value.replace(/[^\d]/, '');
                });
            });
        }
    }

    document.querySelectorAll('.course .arrow-next').forEach(el => {
        el.addEventListener('click', e => {
            let max = e.target.parentElement.querySelectorAll('a').length, next = e.target.getAttribute('next');

            if (next !== null) {
                if (parseInt(next) === max) {
                    e.target.parentElement.querySelector('.course-bottom').scrollLeft = 0;
                    next = 0;
                }

                e.target.parentElement.querySelectorAll('a')[next].focus();
                e.target.setAttribute('next', parseInt(next) + 1);
            } else {
                e.target.parentElement.querySelectorAll('a')[1].focus();
                e.target.setAttribute('next', 2);
            }
        });
    });

    document.querySelector('body').addEventListener('click', e => {
        if (document.querySelector('.overlay')) {
            document.querySelector('.overlay').remove();
            document.querySelector('body').style.overflow = 'initial';
            e.stopPropagation();
        }
    });

    if (document.querySelector('.has-courses h1')) {
        document.querySelector('.has-courses h1').addEventListener('click', e => {
            e.target.nextElementSibling.toggleDisplay();
            e.target.classList.toggle('open');
        });
    }

    document.querySelectorAll('.alphabet-left, .alphabet-right').forEach(el => {
        el.addEventListener('click', e => {
            let firstLetter = document.querySelector('.alphabet-letters');
            let right = firstLetter.scrollWidth - firstLetter.clientWidth;
            let step = document.querySelector('.alphabet-letters').scrollLeft;

            if (el.classList.contains('alphabet-left')) {
                if (step === 0) {
                    step = right;
                } else {
                    step -= 100;
                }
            } else {
                if (step >= right) {
                    step = 0;
                } else {
                    step += 100;
                }
            }

            firstLetter.scroll({
                left: step,
                behavior: 'smooth'
            });
        });
    });

    document.querySelectorAll('form[data-ajax-action]').forEach(el => {
        el.addEventListener('submit', e => {
            e.preventDefault();

            let action = e.target.getAttribute('data-ajax-action'),
                data = new FormData(e.target);

            data.append('action', action);

            fetch(ajaxLink, {method: 'POST', body: data}).then(res => res.json()).then(e => {
                if (e.success) {
                    if (action === 'join_us' || action === 'sign_course') {
                        location.href = thanksLink;
                    } else if (action === 'auth') {
                        location.href = e.data['redirect_to'];
                    } else if (action === 'check_certificate') {
                        document.querySelector('.certificate-status').style.color = e.data.color;
                        document.querySelector('.certificate-status').innerHTML = e.data.message;
                    } else if (action === 'ask_question') {
                        location.href = thanksLink;
                    } else if (action === 'grant_chance') {
                        location.href = thanksLink;
                    }
                } else {
                    if (action === 'check_certificate') {
                        document.querySelector('.certificate-status').innerHTML = 'Что-то пошло не так.';
                    }
                }
            });

            e.target.querySelector('button').disabled = true;

            return false;
        });
    });

    document.querySelectorAll('form[data-ajax-action] button').forEach(el => {
        el.addEventListener('click', e => {

            let currentForm = el.closest("form");
            if (currentForm.querySelectorAll("[name='city']").length > 0) {
                return;
            }

            let city = getCookie('city');
            if (city === null) {
                city = getCity();
                setCookie('city', city, 7);
            }

            let fieldCity = document.createElement("input");
            fieldCity.setAttribute("type", "hidden");
            fieldCity.setAttribute("name", "city");
            fieldCity.setAttribute("value", city);
            el.after(fieldCity);
        });
    });

    document.querySelectorAll('.review-image').forEach(el => {
        el.addEventListener('click', e => {
            e.stopPropagation();

            let video = 'https://www.youtube.com/embed/' + e.target.closest('.review-image').getAttribute('data-link').match(/v=([A-z\d-]+)/)[1],
                body = document.querySelector('body'),
                element = document.createElement('div');

            if (document.querySelector('.overlay')) {
                document.querySelector('.overlay').remove()
            }

            element.classList.add('overlay');
            element.innerHTML = `<div class="embed"><embed width="800" height="450" src="${video}"></div>`;

            body.style.overflow = 'hidden';
            window.requestAnimationFrame(() => body.insertBefore(element, body.firstChild));
        });
    });

    if (document.querySelector('#login-link')) {
        document.querySelector('#login-link').addEventListener('click', (e) => {
            e.preventDefault();
            location.href = loginLink;
        });
    }

    document.querySelectorAll('.glossary-letter:not(.disabled)').forEach(el => {
        el.addEventListener('click', e => {
            let letter = e.target.textContent;

            document.querySelectorAll('.glossary-letter').forEach(el => el.classList.remove('active'));
            e.target.classList.add('active');

            document.querySelectorAll('.glossary-word').forEach(el => {
                el.style.display = el.textContent.startsWith(letter) ? 'block' : 'none';
            });
        });
    });

    document.querySelectorAll('.popup-close').forEach(el => {
        el.addEventListener('click', e => {
            e.target.parentNode.parentNode.style.display = 'none';
        });
    });

    document.querySelectorAll('[data-popup]').forEach(el => {
        let data = el.getAttribute('data-popup');
        if (data == 'sign-course' && document.body.classList.contains('disabled-site')) {
            el.setAttribute('disabled', 'disabled');
        }
        el.addEventListener('click', e => {
            document.querySelector('#' + e.target.getAttribute('data-popup')).style.display = 'block';
        });
    });

    document.querySelectorAll('.grant-chance-button, .form-button, [data-ajax-action="sign_course"] button').forEach(el => {
        if (document.body.classList.contains('disabled-site')) {
            el.setAttribute('disabled', 'disabled');
        }
    });
    /*if ( window.innerWidth <= 768) { // what is this?
        document.querySelector('header').classList.add('sticked');
        document.querySelector('header').nextElementSibling.style.paddingTop = !document.querySelector('.video-bg, .error404, .single-courses') ? '68px' : 0;
    }*/

    document.addEventListener('scroll', () => { // custom lazy load for SVG images
        document.querySelectorAll('.alphabet-left, .alphabet-right, .share-buttons').forEach(el => el.style.display = 'flex');
        document.querySelectorAll('.render-deferred').forEach(el => el.classList.remove('render-deferred'));
        document.querySelectorAll('svg use[data-href]').forEach(el => {
            el.setAttribute('href', el.getAttribute('data-href'));
            el.removeAttribute('data-href');
        });
        document.querySelectorAll('source[data-src]').forEach(el => {
            el.setAttribute('src', el.getAttribute('data-src'));
            el.parentNode.src = el.getAttribute('data-src');
            el.removeAttribute('data-src');
            el.parentNode.play();
        });
    }, {once: true});

    document.addEventListener('scroll', () => {
        if ((document.documentElement.scrollTop || document.body.scrollTop) >= getHeight(document.querySelector('header'))) {
            document.querySelector('header').classList.add('sticked');
            document.querySelector('header').nextElementSibling.style.paddingTop = !document.querySelector('.video-bg, .error404, .single-courses') ? '68px' : 0;
        } else {
            document.querySelector('header').classList.remove('sticked');
            document.querySelector('header').nextElementSibling.style.paddingTop = 0;
        }

        if (document.querySelector('.glossary-word.active') && document.querySelector('.glossary-desc').style.display === 'none') {
            document.querySelector('.glossary-desc').style.display = 'block';
        }

        document.querySelector('.fixed-button').style.display = 'block';

        showEmbed();
    }, {passive: true});

    document.addEventListener('touchstart', () => {
        showEmbed();

        if (document.querySelector('.contacts-subdomain')) {
            document.querySelector('.contacts-subdomain').style.paddingTop = 0;
        }
    });

    document.querySelectorAll('[data-bg]').forEach(el => {
        el.addEventListener('mousemove', () => {
            if (el.getAttribute('data-bg') != "") {
                el.style.background = 'url(' + el.getAttribute('data-bg') + ')';
                el.style.backgroundSize = 'cover';
            }
        }, {once: true});
    });

    if (document.querySelector('.webinar-info')) {
        document.querySelector('.webinar-preview').addEventListener('click', e => {
            let webinar = document.querySelector('.webinar-info'), height = getWidth(webinar) / (16 / 9),
                video = 'https://www.youtube.com/embed/' + e.target.getAttribute('data-link').match(/v=([A-z\d-]+)/)[1],
                embed = document.createElement('embed');

            embed.src = video + '?autoplay=1&start=0&mute=1';
            embed.frameborder = 0;
            embed.style.width = '100%';
            embed.style.height = height + 'px';
            embed.loading = 'lazy';

            e.target.remove();
            webinar.insertBefore(embed, webinar.firstChild);
        });
    }

    document.querySelectorAll('.faq .question-title').forEach(el => {
        el.addEventListener('click', e => {
            let opened = e.target.parentNode.classList.contains('opened');

            document.querySelectorAll('.question.opened').forEach(el => {
                el.classList.remove('opened');
                el.querySelector('.question-answer').style.display = 'none';
            });

            if (!opened) {
                e.target.parentNode.classList.add('opened');
                e.target.nextElementSibling.style.display = 'block';
            }
        });
    });


    if (document.querySelector('.single-blog')) {
        document.querySelectorAll('.article a[href^="#"]').forEach(el => {
            el.addEventListener('click', e => {
                let element = e.target.getAttribute('href');

                scroll({
                    top: document.querySelector(element).getBoundingClientRect().rect.top + document.body.scrollTop - getHeight(document.querySelector('header')) - (document.querySelector('#wpadminbar').length ? 32 : 0) - 10,
                    behavior: 'smooth'
                });
            });
        });
    }

    document.querySelectorAll('.word-close-button').forEach(el => {
        el.addEventListener('click', () => {
            document.querySelectorAll('.glossary-vocabulary').forEach(el => {
                el.classList.add('hidden-content');
            });

            document.querySelectorAll('.glossary-word').forEach(el => {
                el.classList.remove('active');
            })
        });
    });


    if (document.querySelector('.load-more-webinars')) {
        document.querySelector('.load-more-webinars').addEventListener('click', e => {
            let element = e.target, data = new FormData();

            element.classList.add('loading');
            data.append('action', 'load_webinars');
            data.append('offset', document.querySelectorAll('figure').length.toString());

            fetch(ajaxLink, {method: 'POST', body: data}).then(res => res.json()).then(e => {
                element.classList.remove('loading');

                if (e.success) {
                    e.data.webinars.forEach(v => {
                        document.querySelector('.webinars-list').innerHTML += '<figure> <a href="' + v.link + '"><div class="webinar-preview"><img src="' + v.image + '" alt="' + v.title + '"><div class="webinar-duration">' + v.duration + '</div></div></a><figcaption><div class="webinar-title">' + v.title + '</div><div class="webinar-desc">' + v.desc + '</div><div class="webinar-cat">' + v.category + '</div></figcaption></figure>';
                    });

                    if (document.querySelectorAll('figure').length >= e.data.total) {
                        element.remove();
                    }
                }
            });
        });
    }

    document.querySelectorAll('.plug-title').forEach(el => {
        el.addEventListener('click', e => {
            e.target.nextElementSibling.toggleDisplay();
        });
    });

    if (location.host.indexOf('.')) { // subdomain
        let path = location.hostname.split('.'), el = document.querySelector('.' + path[0]);

        if (el) {
            el.classList.add('current');
        }
    }

    setTimeout(function () {
        if (!getCookie('email_subscription')) {
            document.querySelector('#email-subscription').style.display = 'block';
            setCookie('email_subscription', true, 2);
        }
    }, 10000);

    if (document.querySelector('.container .graduates-list.splide, .reviews-list')) {
        let c = 4;
        if (document.querySelector('.container .graduates-list.splide')) {
            let dc = document.querySelector('.container .graduates-list.splide').getAttribute('data-count');
            if (dc) {
                c = dc;
            }
        }


        new Splide('.container .graduates-list, .reviews-list', {
            perPage: c,
            perMove: 1,
            arrows: true,
            pagination: false,
            breakpoints: {
                1590: {
                    perPage: 3,
                },
                1200: {
                    perPage: 2,
                },
                768: {
                    perPage: 1,
                }
            }
        }).mount();
    }

    if (document.querySelector(' .reviews-items')) {
        new Splide('.reviews-items', {
            perPage: 4,
            perMove: 1,
            arrows: false,
            pagination: true,
            breakpoints: {
                1360: {
                    perPage: 3,
                },
                992: {
                    perPage: 2,
                },
                580: {
                    perPage: 1,
                }
            }
        }).mount();
    }

    if (document.querySelector('section.front-page-section')) {
        new Splide('.nearest-courses-list', {
            perPage: 3,
            perMove: 1,
            arrows: false,
            pagination: false,
            breakpoints: {
                1590: {
                    perPage: 3,
                },
                1200: {
                    perPage: 2,
                },
                768: {
                    perPage: 1,
                }
            }
        }).mount();
    }

    if (document.querySelector('section.career-steps')) {
        new Splide('.steps', {
            perPage: 5,
            perMove: 1,
            arrows: false,
            pagination: true,
            breakpoints: {
                1590: {
                    perPage: 4,
                },
                1366: {
                    perPage: 3,
                },
                1200: {
                    perPage: 2,
                },
                768: {
                    perPage: 1,
                }
            }
        }).mount();
    }

    if (document.querySelector('section.nearest-courses')) {
        new Splide('.nearest-courses-list', {
            perPage: 4,
            perMove: 1,
            arrows: false,
            pagination: true,
            breakpoints: {
                1200: {
                    perPage: 3,
                },
                992: {
                    perPage: 2,
                },
                576: {
                    perPage: 1,
                }
            }
        }).mount();
    }

    if (document.querySelector('.teachers-list')) {
        new Splide('.teachers-list', {
            perPage: 1,
            perMove: 1,
            arrows: false,
            pagination: false
        }).mount();
    }

    if (document.querySelector('#salary-graph')) {
        document.addEventListener('scroll', ev => {
            addChartJS();
        }, {once: true});
    }
}

function setCookie(name, value, days) {
    let expires = "";

    if (days) {
        let date = new Date();

        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

        expires = "; expires=" + date.toUTCString();
    }

    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    let nameEQ = name + "=", ca = document.cookie.split(';');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];

        while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) === 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }

    return null;
}

function getHeight(element) {
    return parseFloat(getComputedStyle(element, null).height.replace('px', ''));
}

function getWidth(element) {
    return parseFloat(getComputedStyle(element, null).width.replace('px', ''));
}

HTMLElement.prototype.toggleDisplay = function () {
    this.style.display = this.style.display === 'block' ? 'none' : 'block';
}

function addChartJS() {
    let script = document.createElement('script');

    script.onload = function () {
        Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options.tooltips,
                                _active: [sector]
                            }, chart));
                        });
                    });

                    chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1)
                            return;
                        chart.allTooltipsOnce = true;
                    }

                    chart.options.tooltips.enabled = true;
                    Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip.update();
                        tooltip.pivot();
                        tooltip.transition(easing).draw();
                    });
                    chart.options.tooltips.enabled = false;
                }
            }
        });

        const verticalLinePlugin = {
            getLinePosition: function (chart, pointIndex) {
                const meta = chart.getDatasetMeta(0);
                const data = meta.data;

                return {x: data[pointIndex]._model.x, y: data[pointIndex]._model.y};
            },
            renderVerticalLine: function (chartInstance, pointIndex) {
                const lineLeftOffset = this.getLinePosition(chartInstance, pointIndex);
                const scale = chartInstance.scales['y-axis-0'];
                const context = chartInstance.chart.ctx;

                context.beginPath();
                context.strokeStyle = '#211870';
                context.moveTo(lineLeftOffset.x, scale.bottom);
                context.lineWidth = 1;
                context.lineTo(lineLeftOffset.x, lineLeftOffset.y + 3);
                context.stroke();
            },

            afterDatasetsDraw: function (chart, easing) {
                if (chart.config.lineAtIndex) {
                    chart.config.data.datasets[0].data.forEach((e, pointIndex) => this.renderVerticalLine(chart, pointIndex));
                }
            }
        };

        Chart.plugins.register(verticalLinePlugin);

        new Chart(document.getElementById('salary-graph').getContext('2d'), {
            type: 'line',
            lineAtIndex: true,
            data: {
                labels: Object.keys(salaries),
                datasets: [{
                    pointStyle: 'rect',
                    pointBackgroundColor: '#211870',
                    pointBorderColor: '#211870',
                    borderColor: '#e64e6c',
                    borderWidth: 4,
                    data: Object.values(salaries),
                    fill: false
                }]
            },
            options: {
                showAllTooltips: true,
                tooltips: {
                    backgroundColor: '#211870',
                    titleFontSize: 0,
                    cornerRadius: 0,
                    displayColors: false,
                    caretSize: 0,
                    caretPadding: 10
                },
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });
    };

    script.src = 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js';
    script.async = true;

    document.head.appendChild(script);
}

function getCity() {
    let cities = {
        baku: 'Баку',
        kyiv: 'Киев',
        kharkiv: 'Харьков',
        dnipro: 'Днепр',
        kazan: 'Казань',
        ekb: 'Екатеринбург',
        spb: 'Питер',
    };

    let request = new XMLHttpRequest();

    request.open('GET', 'https://ipapi.co/json/', false);  // `false` makes the request synchronous
    request.send(null);

    if (request.status === 200) {
        let r = JSON.parse(request.responseText);

        if (r.city.toLowerCase() in cities) {
            return cities[r.city.toLowerCase()];
        } else if (r.city === 'St Petersburg' || r.city === 'Saint Petersburg') {
            return cities.spb;
        } else if (r.city === 'Yekaterinburg') {
            return cities.ekb;
        } else if (r.city.includes('Kazan')) {
            return cities.kazan;
        } else {
            return 'not_identified';
        }
    }

    // dmca
    (function () {
        document.addEventListener("DOMContentLoaded", function () {
            let a = document.querySelectorAll("a.dmca-badge");
            if (0 > a[0].getAttribute("href").indexOf("refurl")) for (let b, c = 0; c < a.length; c++) b = a[c], b.href = b.href + (-1 === b.href.indexOf("?") ? "?" : "&") + "refurl" + "=" + document.location
        }, !1)
    })();
}