export const showEmbed = ()=> {
    document.querySelectorAll('embed[data-src]').forEach(el => {
        let embed = document.createElement('embed');

        embed.src = el.getAttribute('data-src');
        embed.frameborder = 0;
        embed.style.height = '450px';
        embed.style.width = '100%';
        embed.loading = 'lazy';

        el.parentNode.replaceChild(embed, el);
    });
}