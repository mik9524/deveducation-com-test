
window.addEventListener('scroll', wdForceImgEvents);
document.body.addEventListener('click', wdForceImgEvents);
document.body.addEventListener('touchstart', wdForceImgEvents);

wdLazyShow = () => {
    if ( typeof window.wdLazyShowInit == 'undefined' ) {
    
        let e = document.querySelectorAll('img.wd-lazy-loading');
    
        if ( e.length ) {
    
        e.forEach(element => {
            element.src = element.getAttribute('data-wdsrcset');
            element.classList.remove('wd-lazy-loading');
            element.classList.add('wd-lazy-loaded');
        });
        }
        window.wdLazyShowInit = true;
    }
}

function wdForceImgEvents() {
  wdLazyShow();
}

var starttime;
 
requestAnimationFrame(function(timestamp){
    starttime = timestamp || new Date().getTime() 
    wdBackgroundStyleSet(timestamp, 400);
});

wdBackgroundStyleSet = (timestamp, duration) => {
  if ( typeof window.bgStyleInit == 'undefined' ) {

    var timestamp = timestamp || new Date().getTime()
    var runtime = timestamp - starttime

    if (runtime > duration){ // if duration yet
    
      let bgStyleElements = document.querySelectorAll('[data-bgstyle]');
      bgStyleElements.forEach(element => {
        let bg = element.getAttribute('data-bgstyle');
        if ( bg.length > 0) {
          element.setAttribute('style', 'background-image:url("' + bg + '")');
        }
      });
    
      window.bgStyleInit = true;

    } else {
      requestAnimationFrame(function(timestamp){ // call requestAnimationFrame again with parameters
        wdBackgroundStyleSet(timestamp, 400);
      });
    }
  }
}


