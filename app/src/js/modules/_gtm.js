import {showEmbed} from "./_showEmbed";

export const gtm = ()=> {
    let isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

    window['dataLayer'] = window['dataLayer'] || []; // loading gtm
    window['dataLayer'].push({'gtm.start': new Date().getTime(), event: 'gtm.js'}); // loading gtm

    window.onload = function () {
        window['dataLayer'].push({'event': 'wd_init_other_metrics'}); // loading other metrics
        if (isMobile) { // mobile
            document.querySelectorAll('.article-preloader ~ img').forEach(el => {
                el.previousElementSibling.remove();
                el.style.display = 'block';
            });
        } else { // other
            showEmbed();
        }
    }
}