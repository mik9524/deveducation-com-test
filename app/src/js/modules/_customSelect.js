export const customSelector = (mainBlock, optionsList, input) => {
    mainBlock.addEventListener('click', () => {
        mainBlock.parentElement.classList.toggle('open');

        if( mainBlock.parentElement.nextSibling && mainBlock.parentElement.nextSibling.nodeName !== '#text' && mainBlock.parentElement.nextSibling.classList.contains('open')) {
            mainBlock.parentElement.nextSibling.classList.remove('open')
        } else if ( typeof mainBlock.parentElement.previousSibling.classList != "undefined" && mainBlock.parentElement.previousSibling !== '#text' && mainBlock.parentElement.previousSibling.classList.contains('open')) {
            mainBlock.parentElement.previousSibling.classList.remove('open')
        }
    })
    optionsList.addEventListener('click', evn => {
        mainBlock.parentElement.classList.remove('open');
        mainBlock.innerHTML = evn.target.innerHTML;
        input.value = evn.target.dataset.value;
        input.setAttribute('value', evn.target.dataset.value);
    });
    window.addEventListener('click', event => {
        if (!event.target.classList.contains('select') && !event.target.classList.contains('choose-city-input') && !event.target.classList.contains('choose-city-input')) {
            mainBlock.parentElement.classList.remove('open');
        }
    })
}