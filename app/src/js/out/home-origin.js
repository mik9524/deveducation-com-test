import {ready} from "../modules/_ready";
import {customSelector} from "../modules/_customSelect";

let slider = false;

ready(() => {
    if (document.querySelector('.graduates-splide')) {
        if (window.innerWidth < 1366) {
            document.querySelector('.graduates-splide').classList.add('splide')
            document.querySelector('.graduates-list-container').classList.add('splide__track')
            document.querySelector('.graduates-list').classList.add('splide__list')
        }

        if (graduatesList.length >= 39) {
            let gap = 14;
            let studentWidth = 109;
            let arrows = true;
            let sliderCounter = 1;
            let listCounter = 1;

            if (window.innerWidth < 1366 && window.innerWidth > 800) {
                studentWidth = 87
                gap = 12;
            } else if (window.innerWidth <= 800 && window.innerWidth > 560) {
                studentWidth = 80;
                slider = true;
                sliderCounter = 2
                arrows = false;
            } else if (window.innerWidth <= 560) {
                gap = 10;
                studentWidth = 70;
                slider = true;
                arrows = false;
                sliderCounter = 5
            }

            const wrapper = document.querySelector('.graduates-list');
            const row = (wrapper.offsetWidth / (studentWidth + gap)).toFixed(0);
            const count = row > 13 ? 13 * 3 : row * 3;

            function addStudents() {
                if (graduatesList.length >= count) {
                    let list = document.createElement('li');
                    list.classList.add('list-' + listCounter);
                    if (slider) {
                        list.classList.add('splide__slide');
                    }
                    list.style.gridTemplateColumns = 'repeat(' + row + ', 1fr)';
                    list.style.display = 'grid!important';
                    list.style.gap = '14px';
                    for (let counter = 0; counter < count; counter++) {
                        let container = document.createElement('div');
                        let name = document.createElement('div');
                        let text = document.createElement('div');

                        name.classList.add('graduates-item-description-name');
                        text.classList.add('graduates-item-description-text');
                        name.innerText = graduatesList[0].name;
                        text.innerText = graduatesList[0].description || 'Курс такой то, город такой то'

                        if (counter === row && window.innerWidth < 1366) {
                            container.classList.add('graduates-item-big');
                            counter = counter + 3;
                            container.appendChild(name);
                            container.appendChild(text);
                        } else {
                            let image = document.createElement('img');
                            let description = document.createElement('div');

                            description.classList.add('graduates-item-description');

                            if (name.innerText === 'Валентина Бурдина') {
                                image.style.objectPosition = '100%'
                            }

                            image.alt = 'Graduates';
                            image.src = graduatesList[0].image;
                            container.appendChild(image);
                            description.appendChild(name);
                            description.appendChild(text);
                            container.appendChild(description);
                            graduatesList.splice(0, 1);
                        }

                        container.classList.add('graduates-item');
                        list.appendChild(container);
                    }

                    wrapper.appendChild(list);
                }
                if (sliderCounter !== listCounter) {
                    listCounter++;
                    addStudents();
                } else {
                    const bigItems = document.querySelectorAll('.graduates-item-big');

                    if (bigItems) {
                        document.querySelectorAll('.graduates-item').forEach(element => {
                            if (element.classList.contains('graduates-item-big')) return;

                            element.addEventListener('click', evt => {
                                bigItems.forEach(e => {
                                    e.innerHTML = element.querySelector('.graduates-item-description').innerHTML;
                                })
                            })
                        })
                    }
                }
            }

            function addListenerAddStudents() {
                addStudents();
                if (window.innerWidth < 1366) {
                    new Splide('.graduates-splide', {
                        arrows: arrows
                    }).mount();
                }
            }

            window.addEventListener('scroll', addListenerAddStudents, {once: true, passive: false})

            if (window.pageYOffset > 1000) {
                window.removeEventListener('scroll', addListenerAddStudents);
                addStudents();
                if (window.innerWidth < 1366) {
                    new Splide('.graduates-splide', {
                        arrows: arrows
                    }).mount();
                }
            }
        }
    }

    if (document.querySelector('.partners-list')) {
        let counter = 0;

        window.addEventListener('scroll', setSrc, {once: true, passive: false})
        window.addEventListener('mousemove', setSrc, {once: true, passive: false})
        window.addEventListener('touchmove', setSrc, {once: true, passive: false})

        if (window.pageYOffset > 1000) {
            setSrc();
        }

        function setSrc() {
            const partnerBlock = document.querySelector('.partners-list');
            const partners = partnerBlock.querySelectorAll('.lazyload');
            if (counter !== 0) return;
            counter++;
            partners.forEach(e => {
                e.src = e.dataset.src
            })
        }
    }
    let grantChanceSelect = document.querySelector('.grant-chance .custom-select .select')

    if (grantChanceSelect) {
        let hiddenInput = document.querySelector('.grant-chance .choose-city-input');
        let cityList = document.querySelector('.grant-chance .choose-city');
        customSelector(grantChanceSelect, cityList, hiddenInput);
    }
})