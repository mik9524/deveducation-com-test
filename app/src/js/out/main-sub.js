import {gtm} from '../modules/_gtm';
import {main} from '../global/_main';
import {header} from '../global/_header-sub';
import {ready} from '../modules/_ready';
import {customSelector} from '../modules/_customSelect';
import "../modules/_wd-lazyload";


gtm();

ready(()=> {
    main();
    header();
    const ajaxLink = '/wp-admin/admin-ajax.php';
  
    document.querySelectorAll('.choose-city-list').forEach(el => {
        el.addEventListener('click', e => {
            let list = e.target.parentNode.parentNode.parentNode.querySelector('.courses-list'), data = new FormData();

            if (list) {
                data.append('action', 'load_courses');
                data.append('city', e.target.dataset.value);

                fetch(ajaxLink, {method: 'POST', body: data}).then(res => res.json()).then(e => {
                    if (e.success) {
                        list.innerHTML = '';

                        e.data.forEach((v, k) => {
                            list.innerHTML += '<div class="courses-list-list" data-value="' + v + '">' + v + '</div>';
                        });

                        list.disabled = false;
                    }
                });
            }
        }, {passive: true});
    });
    

    let popupCitySelect = document.querySelector('#sign-course .custom-select .select.city')
    if (popupCitySelect) {
        let popupHiddenInput = document.querySelector('#sign-course .choose-city-input');
        let popupCityList = document.querySelector('#sign-course .choose-city');
        customSelector(popupCitySelect, popupCityList, popupHiddenInput);
    }

    let popupCourseSelect = document.querySelector('#sign-course .custom-select .select.course')
    if (popupCourseSelect) {
        let popupHiddenInput = document.querySelector('#sign-course .choose-course-input');
        let popupCourseList = document.querySelector('#sign-course .courses-list');
        customSelector(popupCourseSelect, popupCourseList, popupHiddenInput);
    }

    let grantChanceSelect = document.querySelector('.grant-chance .custom-select .select')

    if (grantChanceSelect) {
        let hiddenInput = document.querySelector('.grant-chance .choose-city-input');
        let cityList = document.querySelector('.grant-chance .choose-city');
        customSelector(grantChanceSelect, cityList, hiddenInput);
    }
})