import {gtm} from '../modules/_gtm'
import {main} from '../global/_main'
import {header} from '../global/_header-origin'
import {ready} from '../modules/_ready';
import {customSelector} from "../modules/_customSelect";
import "../modules/_wd-lazyload";

gtm();

ready(() => {
    main();
    header();

    const ajaxLink = '/wp-admin/admin-ajax.php';

    document.querySelectorAll('.choose-city-list').forEach(el => {
        el.addEventListener('click', e => {
            let list = e.target.parentNode.parentNode.parentNode.querySelector('.courses-list'), data = new FormData();

            if (list) {
                data.append('action', 'load_courses');
                data.append('city', e.target.dataset.value);

                fetch(ajaxLink, {method: 'POST', body: data}).then(res => res.json()).then(e => {
                    if (e.success) {
                        list.innerHTML = '';

                        e.data.forEach((v, k) => {
                            list.innerHTML += '<div class="courses-list-list" data-value="' + v + '">' + v + '</div>';
                        });

                        list.disabled = false;
                    }
                });
            }
        }, {passive: true});
    });

    let coursesPageCourseSelect = document.querySelector('.grant-chance .custom-select .select.course')
    if (coursesPageCourseSelect) {
        let coursesPageHiddenInput = document.querySelector('.grant-chance .choose-course-input');
        let coursesPageCourseList = document.querySelector('.grant-chance .courses-list');
        customSelector(coursesPageCourseSelect, coursesPageCourseList, coursesPageHiddenInput);
    }

    let coursesPageCitySelect = document.querySelector('.grant-chance .custom-select .select.city')
    if (coursesPageCitySelect) {
        let coursesPageCityHiddenInput = document.querySelector('.grant-chance .choose-city-input');
        let coursesPageCityList = document.querySelector('.grant-chance .choose-city');
        customSelector(coursesPageCitySelect, coursesPageCityList, coursesPageCityHiddenInput);
    }

    let popupCitySelect = document.querySelector('#sign-course .custom-select .select.city')
    if (popupCitySelect) {
        let popupHiddenInput = document.querySelector('#sign-course .choose-city-input');
        let popupCityList = document.querySelector('#sign-course .choose-city');
        customSelector(popupCitySelect, popupCityList, popupHiddenInput);
    }

    let popupCourseSelect = document.querySelector('#sign-course .custom-select .select.course')
    if (popupCourseSelect) {
        let popupHiddenInput = document.querySelector('#sign-course .choose-course-input');
        let popupCourseList = document.querySelector('#sign-course .courses-list');
        customSelector(popupCourseSelect, popupCourseList, popupHiddenInput);
    }

    if (document.getElementById('courses-tabs') !== null) {
        const coursesTab = document.getElementById('courses-tabs')
        const currentCity = document.createElement('div')
        currentCity.classList.add('currentCity')

        window.requestAnimationFrame(() => {
            coursesTab.insertAdjacentElement('afterbegin', currentCity)
            document.querySelector('.nearest-courses-list-courses').style.marginTop = 0;
        });

        let tab = document.querySelectorAll('.nearest-courses-list-city .city-item'),
            content = document.querySelectorAll('.nearest-courses-list-courses .course-items')

        let slideActive = false

        coursesTab.addEventListener('click', function (e) {
            if (e.target.classList.contains('city-item-name')) {
                let cityId = e.target.closest('.city-item').dataset.siteId
                let contentId = document.querySelector(`.nearest-courses-list-courses .course-items[data-courses-id='${cityId}']`)

                removeClassName(tab, 'active')
                removeClassName(content, 'active')
                removeClassName(content, 'splide__list')
                e.target.closest('.city-item').classList.add('active')
                allCoursesLink()
                contentId.classList.add('active', 'splide__list')
                currentCity.classList.remove('showed')

                if (slideActive) {
                    someSlide.destroy()
                    someSlide.mount()
                }
            }

            if (e.target.classList.contains('currentCity')) {
                e.target.classList.toggle('showed')
            }
        }, {passive: true})

        const removeClassName = (element, className) => {
            for (let i = 0; i < element.length; i++) {
                element[i].classList.remove(className)
            }
        }

        const allCoursesLink = () => {
            const allCoursesLink = coursesTab.querySelector('.nearest-courses-link a')
            let allCoursesLinkText = allCoursesLink.querySelector('.city')

            tab.forEach((item, index) => {
                if (index === 0) {
                    allCoursesLink.style.visibility = 'hidden';
                    currentCity.textContent = item.textContent
                    return;
                }

                if (item.classList.contains('active')) {
                    let draw = () => {
                        allCoursesLinkText.textContent = item.textContent
                    };

                    allCoursesLink.setAttribute('href', item.children[0].dataset.cityLink);
                    allCoursesLink.style.visibility = 'visible';

                    document.querySelector('body').addEventListener('touchstart', draw, {once: true});
                    document.querySelector('body').addEventListener('mousemove', draw, {once: true});
                    document.querySelector('body').addEventListener('click', draw, {once: true});

                    currentCity.textContent = item.textContent
                }
            })
        }

        allCoursesLink()

        document.addEventListener('mousedown', function (e) {
            if (!e.target.classList.contains('currentCity') && !e.target.closest('#courses-tabs')) {
                currentCity.classList.remove('showed')
            }
        }, {passive: true})

        let someSlide = new Splide('.splide-list-courses', {
            perPage: 1,
            perMove: 1,
            arrows: false,
            pagination: true,
            gap: 10
        })

        const listCourses = document.querySelector('.splide-list-courses')

        if (window.innerWidth <= 576) {
            someSlide.mount()
            slideActive = true
        }

        window.addEventListener('resize', function () {
            if (window.innerWidth <= 576) {
                if (!slideActive) {
                    listCourses.classList.add('splide')
                    someSlide.mount()
                    slideActive = true
                    console.log(2)
                }
            } else {
                listCourses.classList.remove('splide')
                someSlide.destroy()
                slideActive = false
            }
        });
    }
})