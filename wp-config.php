<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'devedu' );

/** MySQL database username */
define( 'DB_USER', 'devedu' );

/** MySQL database password */
define( 'DB_PASSWORD', 'p@ssw0rd' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'zyd=bUV_?Pv+G}t)sUEd(AZ3L$2Va:9gN;`Dm0{-p^[t.q?{A`=6V2~#9*ky&lQ@' );
define( 'SECURE_AUTH_KEY', '.i gDBogQS$apG^{TTV; AfV<bohP}mh,{>KzKb->1x^Fz(<AwpK9Dm38sc*o~Zp' );
define( 'LOGGED_IN_KEY', '5{N7U9,x:@(m0(!U40h%U[M5c4(e 13u=APwym|-b^C;vMM Ig[BkT_rf(dfPDgF' );
define( 'NONCE_KEY', 'qwj,0j:VSDZp9@Ub?AZG(@,G]al]G2mAh.=Kvbaw]lM$zferhxmWp.JO[E@a[kVI' );
define( 'AUTH_SALT', 'FlwL2mztroq`*+3 S-wwYn*O+]9;Sar5+NVAfC(.+S*U1-BPeGK@<xW)5~d==||&' );
define( 'SECURE_AUTH_SALT', 'v*BXYE*U3=%yhDq&zzP2ONeuU#JO2Vp1FZU@U@X)e0j@eotRV*@mpek|#L6_rYFV' );
define( 'LOGGED_IN_SALT', '<77rsRj(PG%2{*k D;LfOyAiti:-h$G.o<K^(^]MD)Hlx)2 HoYRu6u]p5n0QoGi' );
define( 'NONCE_SALT', '*O|hu0JM6z:-[g2tM% !KHhE0vl?FJPi>m<XYT`3eVIZ}X~6zTA</j[;!`1640~B' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_DISPLAY', false );
define( 'SAVEQUERIES', false );
define( 'WP_ALLOW_MULTISITE', true );
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', true );
define( 'DOMAIN_CURRENT_SITE', 'deveducation.com' );
define( 'PATH_CURRENT_SITE', '/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );
define( 'COOKIE_DOMAIN', false );

/* That's all, stop editing! Happy publishing. */

/** Custom config **/
define( 'CERTIFICATE_HOST', 'mariadb-certs' );
define( 'CERTIFICATE_DATABASE', 'wordpress' );
define( 'CERTIFICATE_USER', 'wordpress' );
define( 'CERTIFICATE_PASSWORD', 'bJ13hAyfHfJkmVawHjeURmVa' );
define( 'REDIS_ENABLE', true );
define( 'REDIS_HOST', 'redis' );
define( 'REDIS_PORT', 6379 );
define( 'REDIS_EXPIRE_TIME', 2592000 ); // 30 days
define( 'REDIS_EXPIRE_TIME_ARCHIVE', 21600 ); // 6 hours
define( 'AREDIRECT_SUPPORTS', array( 'trailing-slash' ) );

define( 'BITRIX_HOOK_URL', 'https://b24-w3p9fm.bitrix24.com/rest/1/qbcjnzsg4htrm8re/' );
/** Custom config **/

/** Absolute path to the WordPress directory. */

if ( strpos( $_SERVER['HTTP_X_FORWARDED_PROTO'], 'https' ) !== false ) {
	$_SERVER['HTTPS'] = 'on';
}

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
